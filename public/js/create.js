$("#btnTambahField").click(function () {
    var html  = '<tr><td><input type="text" name="label_input[]" class="form-control" placeholder="Masukan Nama Label Input">';
        html += '</td>' +
        '<td>' +
            '<select name="type_input[]" class="form-control select2">'+
                '<option value="integer">Angka</option>'+
                '<option value="string">Text</option>'+
                '<option value="string">File</option>'+
                '<option value="date">Tanggal</option>'+
            '</select>' +
        '</td>' +
        '<td>' +
        '<button class="btn btn-danger btn-icon btn-sm delete" type="button" >' +
        '<i class="flaticon2-trash"></i>' +
        '</button>' +
        '</td>' +
        '</tr>';
    $("#tableField tbody").append(html);
    $(".select2").select2({
        placeholder: "Pilih Data",
        allowClear: true,
        width: '100%'
    });
});

$('#tableField').on('click', '.delete', function (e) {
    $(this).closest('tr').remove();
});

$( "#periode" ).change(function() {
    var valPeriode = $('#periode').val();
    $.ajax({
            type: "GET",
            url: "{{URL('data/getPeriode')}}/"+valPeriode,
            success: function(val) {
                if(val.status=="success"){
                    $('#tgl_mulai').show();
                    $('#tgl_selesai').show();
                        var d = new Date(val['html'][0]['tgl_mulai']);var day = d.getDate();var month = d.getMonth() + 1;var year = d.getFullYear();if (day < 10) {day = "0" + day;}
                        if (month < 10) {month = "0" + month;}var date = day + "-" + month + "-" + year;
                        var e = new Date(val['html'][0]['tgl_selesai']);var day = e.getDate();var month = e.getMonth() + 1;var year = e.getFullYear();if (day < 10) {day = "0" + day;}
                        if (month < 10) {month = "0" + month;}var date2 = day + "-" + month + "-" + year;
                    $('#val_tgl_mulai').val(date);
                    $('#val_tgl_selesai').val(date2);
                    hideLoading();
                }else{
                    toastr.error(val.message);
                    $('#tgl_mulai').hide();
                    $('#tgl_selesai').hide();
                    hideLoading();
                }
            },
            error: function() {
                toastr.error("Terjadi Kesalahan , Silahkan Coba Kembali");
                hideLoading();
            }
        });
});

$('#pilihTypeForm').on('change', function (e) {
    var type = $("#pilihTypeForm").val();
        if (type == "Baru") {
            $("#tableBaru").show();
            $("#importData").hide();
        }else{
            $("#tableBaru").hide();
            $("#importData").show();
        }
});