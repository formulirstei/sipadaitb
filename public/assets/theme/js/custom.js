
$('#backButton').on('click', function(e){
    e.preventDefault();
    window.history.back();
});

$('[data-toggle="kt-tooltip"]').tooltip();

$(".select2").select2({
    placeholder: "Pilih Data",
    allowClear: true,
    width:'100%'
});


$('.summernote').summernote({
    height: 150
});

$('.summernote_1').summernote({
    height: 150
});

$(".select2-multiple").select2({
    // placeholder: "Pilih Data",
    allowClear: true,
    width:'100%'
});


getFunctionDatatable();
var rows_selected = [];
if (typeof datatableData != 'undefined') {
    datatableData.on('kt-datatable--on-layout-updated', function () {
        getFunctionDatatable();
    });
    $('[data-toggle="kt-tooltip"]').tooltip();
    search();
    selectedDelete();
    selectedApprove();
    selectedRejected();
    updateTotal();
    selection();

    $('#perbaharuiData').click(function (e) {
       datatableData.reload();
    });

}

function selection(){
    datatableData.on('kt-datatable--on-check kt-datatable--on-uncheck kt-datatable--on-layout-updated',	function(e) {
        var checkedNodes = datatableData.rows('.kt-datatable__row--active').nodes(); // get selected records
        var count = checkedNodes.length; // selected records count
        $('#kt_subheader_group_selected_rows').html(count);
        if (count > 0) {
            $('#kt_subheader_search').addClass('kt-hidden');
            $('#kt_subheader_group_actions').removeClass('kt-hidden');
        } else {
            $('#kt_subheader_search').removeClass('kt-hidden');
            $('#kt_subheader_group_actions').addClass('kt-hidden');
        }
    });
}


function search(){
    $('#kt_form_status').on('change', function() {
        datatableData.search($(this).val().toLowerCase(), '');
    });
};

function selectedDelete(){
    $('#kt_subheader_group_actions_delete_all').on('click', function() {
        var ids = datatableData.rows('.kt-datatable__row--active').nodes().find('.kt-checkbox--single > [type="checkbox"]').map(function(i, chk) {
            rows_selected.push($(chk).val());
            return $(chk).val();
        });
        if (ids.length > 0) {
            var url = $(this).data('url');
            deleteData(url, "multiple");
        } else {
            toastr.error('Pilih data yang akan di hapus terlebih dahulu.', 'Error!');
        }
    });
};

function selectedApprove(){
    $('#kt_subheader_group_actions_konfirmasi').on('click', function() {
        var ids = datatableData.rows('.kt-datatable__row--active').nodes().find('.kt-checkbox--single > [type="checkbox"]').map(function(i, chk) {
            rows_selected.push($(chk).val());
            return $(chk).val();
        });
        if (ids.length > 0) {
            var url = $(this).data('url');
            approvedData(url, "multiple");
        } else {
            toastr.error('Pilih data yang akan di konfirmasi terlebih dahulu.', 'Error!');
        }
    });
};



function selectedRejected(){
    $('#kt_subheader_group_actions_rejected').on('click', function() {
        var ids = datatableData.rows('.kt-datatable__row--active').nodes().find('.kt-checkbox--single > [type="checkbox"]').map(function(i, chk) {
            rows_selected.push($(chk).val());
            return $(chk).val();
        });
        if (ids.length > 0) {
            var url = $(this).data('url');
            rejectedData(url, "multiple");
        } else {
            toastr.error('Pilih data yang akan di tolak terlebih dahulu.', 'Error!');
        }
    });
};

function updateTotal(){
    datatableData.on('kt-datatable--on-layout-updated', function () {
        $('#kt_subheader_total').html(datatableData.getTotalRows() + ' Total');
    });
};





function approvedData(val, type) {
    var title = "";
    var data = "";
    var url = "";
    if (type == null) {
        url = $(val).data('url');
        title = "Apakah anda yakin akan menyetujui data ini?";
        data = {'id[]': $(val).data('id'), '_token': token};
    } else {
        url = val;
        title = "Apakah anda yakin akan menyetujui " + rows_selected.length + " data ?";
        data = {'id[]': rows_selected, '_token': token};
    }

    swal.fire({
        title: "Konfirmasi Persetujuan Data",
        text: title,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya, Setuju!',
        cancelButtonText: 'Tidak, Batalkan!',
        showLoaderOnConfirm: true,
        reverseButtons: true,
        allowOutsideClick: () => !Swal.isLoading()
    }).then(function(result){
        if (result.value) {
            showLoading();
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    if (data.status == "error") {
                        swal.fire("Informasi", data.message, "error");
                    } else if (data.status == "success") {
                        swal.fire("Informasi", data.message, "success");
                    } else {
                        swal.fire("Informasi", "Data Berhasil Di Setujui", "success");
                    }
                    var uri = $('meta[name=uri]').attr("content");
                    if(uri==""){
                        location.reload();
                    }else{
                        datatableData.reload();
                        if (type != null) {
                            rows_selected = [];
                        }
                    }

                    hideLoading();
                },
                error: function (data) {
                    swal.fire("Informasi", "Maaf Request Error , Silahkan Coba Kembali", "error");
                    hideLoading();
                }
            });
        }
    });
}




function rejectedData(val, type) {
    var title = "";
    var data = "";
    var url = "";
    if (type == null) {
        url = $(val).data('url');
        title = "Apakah anda yakin akan menolak data ini?";
        data = {'id[]': $(val).data('id'), '_token': token};
    } else {
        url = val;
        title = "Apakah anda yakin akan menolak " + rows_selected.length + " data ?";
        data = {'id[]': rows_selected, '_token': token};
    }

    swal.fire({
        title: "Konfirmasi Tolak Data",
        text: title,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya, Tolak!',
        cancelButtonText: 'Tidak, Batalkan!',
        showLoaderOnConfirm: true,
        reverseButtons: true,
        allowOutsideClick: () => !Swal.isLoading()
    }).then(function(result){
        if (result.value) {
            showLoading();
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    if (data.status == "error") {
                        swal.fire("Informasi", data.message, "error");
                    } else if (data.status == "success") {
                        swal.fire("Informasi", data.message, "success");
                    } else {
                        swal.fire("Informasi", "Data Berhasil Di Tolak", "success");
                    }

                    var uri = $('meta[name=uri]').attr("content");
                    if(uri==""){
                        location.reload();
                    }else{
                        datatableData.reload();
                        if (type != null) {
                            rows_selected = [];
                        }
                    }
                    hideLoading();
                },
                error: function (data) {
                    swal.fire("Informasi", "Maaf Request Error , Silahkan Coba Kembali", "error");
                    hideLoading();
                }
            });
        }
    });
}





function printData(val, type) {
    var title = "";
    var data = "";
    var url = "";
    if (type == null) {
        url = $(val).data('url');
        title = "Apakah anda yakin akan print data ini  ?";
        data = {'id[]': $(val).data('id'), '_token': token};
    } else {
        url = val;
        title = "Apakah anda yakin akan print " + rows_selected.length + " data ?";
        data = {'id[]': rows_selected, '_token': token};
    }

    swal.fire({
        title: "Konfirmasi Print Data",
        text: title,
        icon: "warning",
        showLoaderOnConfirm: true,
        closeOnConfirm: true,
        buttons: {
            cancel: {
                text: "Batal",
                value: null,
                visible: true,
                className: "",
                closeModal: true,
            },
            confirm: {
                text: "Hapus",
                value: true,
                visible: true,
                className: "btn-info",
                closeModal: false
            }
        }
    }).then((isConfirm) => {
        if(isConfirm) {
            showLoading();
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    if (data.status == "error") {
                        swal.fire("Informasi", data.message, "error");
                    } else if (data.status == "success") {
                        swal.fire("Informasi", data.message, "success");
                    } else {
                        swal.fire("Informasi", "Data Berhasil Di Hapus", "success");
                    }
                    datatableData.reload();
                    if (type != null) {
                        rows_selected = [];
                    }
                    hideLoading();
                },
                error: function (data) {
                    swal.fire("Informasi", "Maaf Request Error , Silahkan Coba Kembali", "error");
                    hideLoading();
                }
            });
        }
    });
}


function getFunctionDatatable() {
    $('[data-toggle="kt-tooltip"]').tooltip();
}


function deleteData(val, type) {
    var title = "";
    var data = "";
    var url = "";
    if (type == null) {
        url = $(val).data('url');
        title = "Apakah anda yakin akan menghapus data ini?";
        data = {'id[]': $(val).data('id'), '_token': token};
    } else {
        url = val;
        title = "Apakah anda yakin akan menghapus " + rows_selected.length + " data ?";
        data = {'id[]': rows_selected, '_token': token};
    }

    swal.fire({
        title: "Konfirmasi Hapus Data",
        text: title,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya, Hapus!',
        cancelButtonText: 'Tidak, Batalkan!',
        showLoaderOnConfirm: true,
        reverseButtons: true,
        allowOutsideClick: () => !Swal.isLoading()
    }).then(function(result){
        if (result.value) {
            showLoading();
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    if (data.status == "error") {
                        swal.fire("Informasi", data.message, "error");
                    } else if (data.status == "success") {
                        swal.fire({
                            title: "Informasi!",
                            text: data.message,
                            type: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok!",
                            confirmButtonClass: "btn btn-info"
                        });
                    } else {
                        swal.fire("Informasi", "Data Gagal Di Hapus" , "error");
                    }
                    datatableData.reload();
                    if (type != null) {
                        rows_selected = [];
                    }
                    hideLoading();
                },
                error: function (data) {
                    swal.fire("Informasi", "Maaf Request Error , Silahkan Coba Kembali", "error");
                    hideLoading();
                }
            });
        }
    });


}


function statusPendataan(val) {
    var id = $(val).data('id');
    var url =  $(val).data('url');
    var type = $(val).data('type');
    var status = $(val).data('status');

    var tittle="";
    if(type=="pendataan"){
        if(status=="confirmed"){
            title="Apakah Anda Yakin Akan Mengkonfirmasi Status Pendataan Ini ?";
        }else{
            title="Apakah Anda Yakin Akan Membatalkan Konfirmasi Status Pendataan Ini ?";
        }

    }else{
        if(status=="confirmed"){
            title="Apakah Anda Yakin Akan Mengkonfirmasi Status Pemilihan Ini ?";
        }else{
            title="Apakah Anda Yakin Akan Membatalkan Konfirmasi Status Pemilihan Ini ?";
        }
    }

    var data = {'id': id, '_token': token , 'type':type ,'status':status};

    swal.fire({
        title: tittle,
        text: title,
        type: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Ya, Konfirmasi!',
        cancelButtonText: 'Tidak, Batalkan!',
        showLoaderOnConfirm: true,
        reverseButtons: true,
        allowOutsideClick: () => !Swal.isLoading()
    }).then(function(result){
        if (result.value) {
            showLoading();
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                success: function (data) {
                    if (data.status == "error") {
                        swal.fire("Informasi", data.message, "error");
                    } else if (data.status == "success") {
                        swal.fire({
                            title: "Informasi!",
                            text: data.message,
                            type: "success",
                            buttonsStyling: false,
                            confirmButtonText: "Ok!",
                            confirmButtonClass: "btn btn-info"
                        });
                    } else {
                        swal.fire("Informasi", "Status Gagal Di Ubah" , "error");
                    }
                    hideLoading();
                    datatableData.reload();
                    if (type != null) {
                        rows_selected = [];
                    }
                },
                error: function (data) {
                    swal.fire("Informasi", "Maaf Request Error , Silahkan Coba Kembali", "error");
                    hideLoading();
                }
            });
        }
    });
}

function resetPasswordPendataan(val, type) {
    var id = $(val).data('id');
    var url =  $(val).data('url');


    Swal.fire({
        title: 'Masukan Kata Sandi Baru',
        input: 'text',
        inputAttributes: {
            autocapitalize: 'off'
        },
        showCancelButton: true,
        confirmButtonText: 'Simpan Kata Sandi',
        showLoaderOnConfirm: true,
        reverseButtons: true,
        preConfirm: (data) => {
            if (data == "") {
                Swal.showValidationMessage(
                    `Kata Sandi Belum Di Masukan`
                )
            } else if (data.length < 6) {
                Swal.showValidationMessage(
                    `Masukan Minimal 6 Karakter`
                )
            } else {
                return fetch(url, {
                    method: 'POST',
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({id:id,password: data, _token:token})
                })
                    .then(response => {
                        if (!response.ok) {
                            throw new Error(response.statusText)
                        }
                        return response.json()
                    })
                    .catch(error => {
                        Swal.showValidationMessage(
                            `Maaf Request Error , Silahkan Coba Kembali`
                        )
                    })
            }

        },
        allowOutsideClick: () => !Swal.isLoading()
    }).then((result) => {
        if (result.value) {
            var status = result.value.status;
            if (status == "success") {
                swal.fire("Informasi", result.value.message, "success");
            } else if (status == "error_validation") {
                var message = "";
                $.each(result.value.result, function (key, val) {
                    message += "<span class='kt-font-danger'>* " + val + "</span><br>";
                });
                swal.fire("Informasi", message, "error");
            } else {
                swal.fire("Informasi", result.value.message, "error");
            }
        }
    });

}
