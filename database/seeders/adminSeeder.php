<?php

namespace Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Modules\Data\Entities\MType;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

class adminSeeder extends Seeder
{
    public function run()
    {

        $mType = ([
            ['name' => 'Pilihan Ganda','type' => 'radio', 'created_by' => 1, 'updated_by' => 1 ,'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'Kotak Centang','type' => 'checkbox', 'created_by' => 1, 'updated_by' => 1 ,'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'Jawaban Singkat','type' => 'text', 'created_by' => 1, 'updated_by' => 1 ,'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'Paragraf','type' => 'textarea', 'created_by' => 1, 'updated_by' => 1 ,'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'Dropdown','type' => 'select', 'created_by' => 1, 'updated_by' => 1 ,'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'Upload File','type' => 'file', 'created_by' => 1, 'updated_by' => 1 ,'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'Tanggal','type' => 'date', 'created_by' => 1, 'updated_by' => 1 ,'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'Waktu','type' => 'timestamp', 'created_by' => 1, 'updated_by' => 1 ,'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
        ]);

        $permissionFeatureDashboard = ([
            ['name' => 'dashboard','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
        ]);

        $permissionFeatureUser = ([
            ['name' => 'user','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'tambah user','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'detail user','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'edit user','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'hapus user','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
        ]);


        $permissionFeatureRole = ([
            ['name' => 'role','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'tambah role','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'detail role','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'edit role','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'hapus role','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
        ]);


        $permissionFeaturePermission = ([
            ['name' => 'permission','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'tambah permission','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'detail permission','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'edit permission','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'hapus permission','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
        ]);


        $permissionFeaturePeriode = ([
            ['name' => 'periode','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'detail periode','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'edit periode','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'tambah periode','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'hapus periode','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
        ]);

        $permissionFeatureUnitFakultas = ([
            ['name' => 'unit-fakultas','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'detail unit-fakultas','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'edit unit-fakultas','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'tambah unit-fakultas','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'hapus unit-fakultas','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
        ]);

        $permissionFormulir = ([
            ['name' => 'formulir','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'detail formulir','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'edit formulir','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'hapus formulir','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'mapping formulir','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
        ]);

        $permissionIsianFormulir = ([
            ['name' => 'isian formulir','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'detail isian formulir','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'edit isian formulir','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'hapus isian formulir','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'data isian formulir','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
        ]);

        $permissionSuperAdmin = ([
            ['name' => 'tambah formulir is admin','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'ubah privilege','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
            ['name' => 'notifikasi','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
        ]);

        $permissionCreator = ([
            ['name' => 'tambah formulir is creator','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
        ]);

        // $permissionPengguna = ([
        //     ['name' => 'tambah formulir is creator','guard_name' => 'web', 'created_at' => '2021-05-19 00:00:00', 'updated_at' => '2021-05-19 00:00:00'],
        // ]);

        foreach($mType as $values){
            $mType = MType::create($values);
        }

        $roleSuperAdmin=Role::create(['name' => 'super admin']);
        $roleCreator=Role::create(['name' => 'creator']);
        $rolePengguna=Role::create(['name' => 'pengguna']);

        foreach($permissionFeatureDashboard as $permissionFeatureDashboards){
            $featureDashboard = Permission::create($permissionFeatureDashboards);
            $roleSuperAdmin->givePermissionTo($featureDashboard);
            $roleCreator->givePermissionTo($featureDashboard);
            $rolePengguna->givePermissionTo($featureDashboard);
        }

        foreach($permissionFeatureUser as $permissionFeatureUsers){
            $featureUser = Permission::create($permissionFeatureUsers);
            $roleSuperAdmin->givePermissionTo($featureUser);
        }

        foreach($permissionFeatureRole as $permissionFeatureRoles){
            $featureRole = Permission::create($permissionFeatureRoles);
            $roleSuperAdmin->givePermissionTo($featureRole);
        }

        foreach($permissionFeaturePermission as $permissionFeaturePermissions){
            $featurePermission = Permission::create($permissionFeaturePermissions);
            $roleSuperAdmin->givePermissionTo($featurePermission);
        }

        foreach($permissionFeaturePeriode as $permissionFeaturePeriodes){
            $featurePeriode = Permission::create($permissionFeaturePeriodes);
            $roleSuperAdmin->givePermissionTo($featurePeriode);
        }

        foreach($permissionFeatureUnitFakultas as $permissionFeatureUnitFakultass){
            $featureUnitFakultas = Permission::create($permissionFeatureUnitFakultass);
            $roleSuperAdmin->givePermissionTo($featureUnitFakultas);

        }

        foreach($permissionFormulir as $permissionFormulirs){
            $featureFormulir = Permission::create($permissionFormulirs);
            $roleSuperAdmin->givePermissionTo($featureFormulir);
            $roleCreator->givePermissionTo($featureFormulir);
        }


        foreach($permissionIsianFormulir as $permissionIsianFormulirs){
            $featureIsianFormulir = Permission::create($permissionIsianFormulirs);
            $roleSuperAdmin->givePermissionTo($featureIsianFormulir);
            $roleCreator->givePermissionTo($featureIsianFormulir);
            $rolePengguna->givePermissionTo($featureIsianFormulir);
        }


        foreach($permissionSuperAdmin as $permissionSuperAdmins){
            $superAdmin = Permission::create($permissionSuperAdmins);
            $roleSuperAdmin->givePermissionTo($superAdmin);
        }

        foreach($permissionCreator as $permissionCreators){
            $creator = Permission::create($permissionCreators);
            $roleCreator->givePermissionTo($creator);
        }

        $role = Role::whereIn('id',[$roleSuperAdmin->id])->select('id','name')->get();
        $arr = [];
        foreach($role as $key => $value){
            $arr[$key]['id'] = $value->id;
            $arr[$key]['name'] = $value->name;
            $arr[$key]['alias'] = strtolower($value->name);
        }

        $user = [
            'name'          => 'Super admin',
            'username'      => 'superadmin',
            'email'         => 'superadmin@gmail.com',
            'permission'    =>  serialize($arr),
            'password'      =>  Hash::make('12345678')
        ];
        $user = User::create($user);
        $user->assignRole($roleSuperAdmin);

        $user = User::find(1);

        $user->get_user_detail()->create([
            'type'    => null,
            'user_id'    => $user->id,
            'm_unitfakultas_id'    => null,
            'created_at'    =>  Carbon::now(),
            'created_by'    =>1,
        ]);


    }
}
