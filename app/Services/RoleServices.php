<?php
namespace App\Services;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Role;

class RoleServices
{

    public function get($request)
    {
        $dataRole = Role::orderBy('name')->get();
        return $dataRole;
    }

    public function create($request)
    {
        $data = $request->only(['name']);
        $dataRole = Role::create($data);

        return $dataRole;

    }

    public function show($request , $id)
    {
        $dataRole = Role::whereId($id)->first();
        return $dataRole;
    }

    public function update($request,$id)
    {
        $data = $request->only(['name']);
        $dataRole = Role::whereId($id)->update($data);
        return $dataRole;

    }

    public function delete($request,$id)
    {
        $dataRole = Role::whereId($id)->delete($id);
        return $dataRole;
    }



}
