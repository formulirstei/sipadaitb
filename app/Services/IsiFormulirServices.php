<?php
namespace App\Services;

use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Modules\Data\Entities\IsianFormulirForm;
use Modules\Data\Entities\MFormulir;
use Illuminate\Http\Request;
use Modules\Data\Entities\MType;


class IsiFormulirServices
{


    public function __construct(FormulirServices $FormulirServices)
    {
        $this->FormulirService = $FormulirServices;
    }

    public function get($request)
    {
        $dataFormulir = MFormulir::paginate(9);
        return $dataFormulir;
    }

    public function create($request,$id)
    {
        DB::beginTransaction();
        try {
            $id = $id;
            $master_formulir = MFormulir::find($id);
            $count = $master_formulir->m_formulir_detail->first();
            $names = $count->name;
            $table_name = $master_formulir->table_name;
            $temp_data = [];
            $i = 0;

            for ($i=0; $i < count($request->$names); $i++){
                foreach ($master_formulir->m_formulir_detail as $detail) {
                $temp_data[$i][strtolower($detail->name)]  = $request->{$detail->name}[$i];
                $temp_data[$i]['created_by']  = Auth::user()->id;
                $temp_data[$i]['updated_by']  = Auth::user()->id;
                $temp_data[$i]['created_at']  = Carbon::now();
                $temp_data[$i]['updated_at']  = Carbon::now();
                }
            }
            $insertData = DB::table($table_name)->insert($temp_data);
            DB::commit();
            return $insertData;
        }catch (\Exception $e) {
            DB::rollback();
            return back();
        }
    }

    public function createFormulirForm($request,$id)
    {
//        DB::beginTransaction();
//        try {

            $isianFormulir      = IsianFormulirForm::where('m_formulirs_id', $id)->with('get_type_detail')->get();
            $master_formulir    = MFormulir::find($id);
            $table_name         = $master_formulir->table_name;

            foreach ($isianFormulir as $key => $value){
                $array[] = [
                    'type_id'           => $value['id'],
                    'name'              => $value->get_type_detail->get_type->name,
                    'pertanyaan'        => str_replace(' ','_',strtolower($value->get_type_detail->pertanyaan)),
                ];

                $names[$key]    = str_replace(' ','_', strtolower($array[$key]['name'])).''.$array[$key]['type_id'];

                $data = [];
                for($a=0; $a < count($names); $a++){
                    $data[$array[$a]['pertanyaan']] = serialize($request->get($names[$a]));
                    $data['created_by']  = Auth::user()->id;
                    $data['updated_by']  = Auth::user()->id;
                    $data['created_at']  = Carbon::now();
                    $data['updated_at']  = Carbon::now();
                }
            }
//            dd($data);

            $insertData = DB::table($table_name)->insert($data);
            DB::commit();
            return $insertData;
//        }catch (\Exception $e) {
//            DB::rollback();
//            return back();
//        }
    }

    public function show($request , $id)
    {
        $dataFormulirServices = $this->FormulirService->show($request, $id);
        return $dataFormulirServices;
    }

    public function update($request,$id)
    {


    }

    public function delete($request,$id)
    {

    }

    public function detail($request, $id)
    {
        $dataFormulirServices = $this->FormulirService->show($request, $id);
        $table_name = $dataFormulirServices->table_name;
        $dataFormulirService = DB::table($table_name)
        ->select('*')
        ->orderBy('id', 'desc')
        ->paginate(100);

        return $dataFormulirService;
    }

    public function editData($request, $id)
    {
        $dataFormulirServices = $this->FormulirService->show($request, $id);
        $table_name = $dataFormulirServices->table_name;
        $dataFormulirService = DB::table($table_name)
        ->select('*')
        ->orderBy('id', 'desc')
        ->get();

        return $dataFormulirService;
    }

}
