<?php
namespace App\Services;

use Carbon\Carbon;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Schema;
use Modules\Data\Entities\IsianFormulirForm;
use Modules\Data\Entities\MFormulir;
use Modules\Data\Entities\FormulirDetail;
use Modules\Data\Entities\MType;
use Modules\Data\Entities\MTypeDetail;
use Modules\Data\Entities\Periode;
use Modules\Data\Entities\UnitFakultas;

class FormulirServices
{

    public function __construct(FormulirDetailServices $formulirDetailServices, FormulirDetail $formulirDetail, PeriodeServices $periodeServices)
    {
        $this->formulirDetailService = $formulirDetailServices;
        $this->periodeService = $periodeServices;
        $this->formulirDetail = $formulirDetail;
    }

    public function get($request)
    {
        $dataFormulir = MFormulir::orderBy('name')->with('m_unitfakultas');
        return $dataFormulir;
    }

    public function byCreatedBy($request)
    {
        $dataFormulir = MFormulir::where('created_by', Auth::user()->id);
        return $dataFormulir;
    }

    public function create($request)
    {
        $data =[];
        if ($request->type_formulir == "Baru") {
            # code...
            $alias = '';
            $words = explode(" ", $request->name);
            $acronym = "";

            foreach ($words as $w) {
                $acronym .= $w[0];
            }
            $alias = $acronym;

            $data = $request->only(['name','type']);
            $data['table_name'] = str_replace(' ','_', strtolower($request->name));
            $data['alias'] = strtoupper($alias);
            $data['created_by'] = Auth::user()->id;
            $data['updated_by'] = Auth::user()->id;

            $dataFormulir = MFormulir::create($data);

            $dataPeriode = Periode::find($request->periode);
            $dataFormulir->m_periode()->attach($dataPeriode);

            if (Gate::check('tambah formulir is creator')){
                $dataFormulir->m_unitfakultas()->attach(Auth::User()->get_user_detail->get_unit_fakultas->id);
            }

            $tableName = $data['table_name'];

            //hitung request dan insert formulir detail
            $fields = [];
            $dataFormulirDetailNama = $request->label_input;
            $dataFormulirDetailType = $request->type_input;
            if(!is_null($dataFormulirDetailNama)){
                for ($i=0; $i < count($dataFormulirDetailNama); $i++){
                    //array inputan
                    $fields[$i]['label_input'] = str_replace(' ','_', strtolower($dataFormulirDetailNama[$i]));
                    $fields[$i]['type_input'] = $dataFormulirDetailType[$i];
                    $fields[$i]['queque'] = $i + 1;

                    //insert ke formulir detail
                    $formulirDetail = new Request();
                    $formulirDetail['label_input'] = $dataFormulirDetailNama[$i];
                    $formulirDetail['type_input'] = $dataFormulirDetailType[$i];
                    $formulirDetail['queque'] = $i + 1;
                    $formulirDetails = $this->formulirDetailService->create($formulirDetail);
                    $dataFormulir->m_formulir_detail()->attach($formulirDetails);
                }
            }

            if (!Schema::hasTable($tableName)) {
                Schema::create($tableName, function (Blueprint $table) use ($fields) {
                    $table->increments('id');
                    if (count($fields) > 0) {
                        foreach ($fields as $field) {
                            $table->{$field['type_input']}(strtolower($field['label_input']))->nullable()->default(null);
                        }
                    }
                    $table->bigInteger('created_by')->nullable();
                    $table->bigInteger('updated_by')->nullable();
                    $table->timestamps();
                });
            }
        }else {
            # code...
            $alias = [];
            $temp = [];
            for($x=0; $x < count($request->input_nama_formulir); $x++){
                $words = explode(" ", $request->input_nama_formulir[$x]);
                $acronym = "";

                foreach ($words as $w) {
                    $acronym .= $w[0];
                }
                $alias[$x] = $acronym;

                $data['table_name'] = str_replace(' ','_', strtolower($request->input_nama_formulir[$x]));
                $data['alias'] = strtoupper($alias[$x]);
                $data['created_by'] = Auth::user()->id;
                $data['updated_by'] = Auth::user()->id;

                $tableName = $data['table_name'];
                $dataFormulir = MFormulir::create($data);

                $dataPeriode = Periode::find($request->periode);
                $dataFormulir->m_periode()->attach($dataPeriode);
                $fields =[];
                $f_details = MFormulir::find($request->select_master_formulir[$x]);
                $f_details_data = $f_details->m_formulir_detail()->get();
                $x=0;
                foreach ($f_details_data as $key => $value) {
                    # code...
                    $fields[$x] = $value->getAttributes();
                    $temp = new Request();
                    $temp['label_input'] = $fields[$x]['name'];
                    $temp['type_input'] = $fields[$x]['type'];
                    $temp['queque'] = $x + 1;
                    $formulirDetails = $this->formulirDetailService->create($temp); //Perlu ada improve pada pembuatan formulir details untuk import
                    $dataFormulir->m_formulir_detail()->attach($formulirDetails); //Perlu ada improve pada pembuatan relasi antara formulir dan formulir details untuk import
                    $x++;
                }

                if (!Schema::hasTable($tableName)) {
                    if ($request->type_formulir == "Import" && $request->pilih_import == "ya") {
                        # code...
                        DB::statement('CREATE TABLE '.$tableName.' AS SELECT * FROM '.$f_details->table_name.'');
                    } else if($request->type_formulir == "Import" && $request->pilih_import == "tidak") {
                        DB::statement('CREATE TABLE '.$tableName.' LIKE '.$f_details->table_name.'');
                    }
                }
            }
        }

        return $dataFormulir;
    }

    public function show($request , $id)
    {
        $dataFormulir = MFormulir::whereId($id)->first();
        return $dataFormulir;
    }

    public function update($request,$id)
    {
        $data = $request->only(['name']);
        $dataFormulir = MFormulir::whereId($id)->update($data);
        return $dataFormulir;

    }

    public function delete($request,$id)
    {
        MFormulir::whereId($id)->update(['deleted_by'=>Auth::user()->id, 'deleted_at'=>Carbon::now()]);
        $dataFormulir = MFormulir::whereId($id)->delete($id);
        return $dataFormulir;
    }

    public function query($request)
    {
        $namaFormulir = $request->namaFormulir;
        $namaTable = $request->namaTable;

        if(!Gate::check('formulir')){
            $dataFormulir = MFormulir::query()->where('name', 'formulir');
        }else{
            $dataFormulir = MFormulir::query();
        }

        if(!empty($namaFormulir)  && $namaFormulir!='Pilih Nama Formulir'){
            $dataFormulir = $dataFormulir->where('name',$namaFormulir);
        }

        if(!empty($namaTable) && $namaTable!='Pilih Nama Table'){
            $dataFormulir = $dataFormulir->where('table_name',$namaTable);
        }

        return $dataFormulir;
    }

    public function byNamaFormulir($request)
    {
        $dataFormulir = MFormulir::where('is_success', '1')->orderBy('id')->select('id','name','table_name')->get();
        return $dataFormulir;
    }

    public function byNamaTable($request,$table_name)
    {
        $dataFormulir = MFormulir::where('name',$table_name)->select('id','name','table_name as text')->get();
        return $dataFormulir;
    }

    public function getPeriode($request, $id)
    {
        $getDataPeriode      = Periode::whereId($id)->get();
        return $getDataPeriode;
    }

    public function getHeader($request, $id)
    {
        $dataFormulir       = MFormulir::whereId($id)->first();
        $dataFormulirDetail = $dataFormulir->m_formulir_detail()->orderBy('queque', 'ASC');

        return $dataFormulirDetail;
    }

    public function getHeaderOne($request, $id, $f_detail_id)
    {
        $dataFormulir       = MFormulir::whereId($id)->first();
        $dataFormulirDetail = $dataFormulir->m_formulir_detail()->find($f_detail_id);

        return $dataFormulirDetail;
    }

    public function createHeader($request, $id)
    {
        $dataFormulir       = MFormulir::whereId($id)->first();
        $table_name         = $dataFormulir->table_name;
        $position           = $request->tambah_setelah;
        $type               = $request->type;
        $nama_header        = $request->name;

        if ($position == '0'){
            $urutan        = 0;
        }else{
            $urutan        = $dataFormulir->m_formulir_detail()->where('name', $position)->first();
        }

        Schema::table($table_name, function($table) use ($type, $position, $nama_header)
        {
            if ($position == '0'){
                $table->{$type}(str_replace(' ','_', strtolower($nama_header)))->after('id')->nullable()->default(null);
            }else{
                $table->{$type}(str_replace(' ','_', strtolower($nama_header)))->after($position)->nullable()->default(null);
            }
        });

        if ($position == '0'){
            $position     = 0 + 1;
            $posisi_2     = 0 + 1;
        }else{
            $position     = $urutan->queque + 1;
            $posisi_2     = $urutan->queque + 1;
        }

        $update_data =  $dataFormulir->m_formulir_detail()->where('queque', '>=', $position)->orderBy('queque', 'asc')->get();

        foreach ($update_data as $value) {
            $dataFormulir->m_formulir_detail()->find($value->id)->update(['queque' => $position = $position + 1]);
        }

        $formulirDetail = new Request();
        $formulirDetail['label_input']  = $nama_header;
        $formulirDetail['type_input']   = $type;
        $formulirDetail['queque']       = $posisi_2;
        $formulirDetails                = $this->formulirDetailService->create($formulirDetail);

        $dataFormulir->m_formulir_detail()->attach($formulirDetails);

        return $formulirDetails;
    }

    public function updateHeader($request, $id, $f_detail_id)
    {
        $data['name']       = str_replace(' ', '_', strtolower($request->name));
        $data['updated_by'] = Auth::user()->id;
        $kolom_old          = str_replace(' ', '_', strtolower($request->kolom_lama));
        $kolom_new          = $data['name'];

        $dataFormulir       = MFormulir::whereId($id)->first();
        $table_name         = $dataFormulir->table_name;

        Schema::table($table_name, function($table) use ($kolom_old, $kolom_new)
        {
            $table->renameColumn($kolom_old, strtolower($kolom_new));
        });

        $dataFormulirDetail = FormulirDetail::whereId($f_detail_id)->update($data);

        return $dataFormulirDetail;
    }

    public function deleteHeader($request, $id, $f_detail_id)
    {

        $data['name']       = str_replace(' ', '_', strtolower($request->name));
        $data['deleted_by'] = Auth::user()->id;
        $kolom_name         = $data['name'];

        $dataFormulir       = MFormulir::whereId($id)->first();
        $table_name         = $dataFormulir->table_name;

        Schema::table($table_name, function($table) use ($kolom_name)
        {
            $table->dropColumn($kolom_name);
        });

        $dataFormulirDetail = FormulirDetail::whereId($f_detail_id)->update(['deleted_by'=>Auth::user()->id]);
        $dataFormulir->m_formulir_detail()->detach($dataFormulirDetail);

        $dataFormulir->delete();

        return $dataFormulirDetail;
    }

    public function mapping($request, $id)
    {

        $dataFormulir = MFormulir::whereId($id)->first();

        if ($dataFormulir->turunan_formulir != null) {
            $select_turunan = unserialize($dataFormulir->turunan_formulir);
        } else {
            $select_turunan = [];
        }

        $tampung_request = [];
        foreach($request->unit_fakultas as $c => $a ){
            $tampung_request[$c] = $a;
        }

        $marge =  array_merge($tampung_request, $select_turunan);

        $dataFormulir->update([
            'turunan_formulir' => serialize($marge)
        ]);

        $dataFormulirDetail = $dataFormulir->m_formulir_detail()->get();
        $fields = [];
        $namaUnitFakultas   = UnitFakultas::whereIn('id', $request->unit_fakultas)->get();
        $data               = $namaUnitFakultas->toArray();
        $table_name         = $dataFormulir->table_name;

        for($x=0; $x < count($request->unit_fakultas); $x++){

            $namaFormulir   = $data[$x]['shortname'].' '.$dataFormulir->name;
            $namaTable      = strtolower($data[$x]['shortname']).'_'.$table_name;

            //insert ke master formulir
            $data['name'] = $namaFormulir;
            $data['table_name'] = $namaTable;
            $data['alias'] = 'x';
            $data['type'] = $dataFormulir->type;
            $data['created_by'] = Auth::user()->id;
            $data['updated_by'] = Auth::user()->id;
            $dataFormulirId = MFormulir::create($data);

            $dataPeriode = Periode::where('id', $dataFormulir->m_periode()->first()->id)->first();
            $dataFormulirId->m_periode()->attach($dataPeriode);

            $dataUnitFakultas   = UnitFakultas::whereIn('id', [$data[$x]['id']])->get();
            $dataFormulirId->m_unitfakultas()->attach($dataUnitFakultas);

            for($a=0; $a < count($dataFormulirDetail); $a++){
                $fields[$a]['label_input'] = str_replace(' ','_', strtolower($dataFormulirDetail[$a]->name));
                $fields[$a]['type_input'] = $dataFormulirDetail[$a]->type;
                $fields[$a]['queque'] = $a + 1;

                $formulirDetail = new Request();
                $formulirDetail['label_input'] = $dataFormulirDetail[$a]->name;
                $formulirDetail['type_input'] = $dataFormulirDetail[$a]->type;
                $formulirDetail['queque'] = $a + 1;

                $formulirDetails = $this->formulirDetailService->create($formulirDetail);
                $dataFormulirId->m_formulir_detail()->attach($formulirDetails);
            }

            // create table
            if (!Schema::hasTable(strtolower($data[$x]['shortname']).'_'.$table_name)) {
                Schema::create(strtolower($data[$x]['shortname']).'_'.$table_name, function (Blueprint $table) use ($fields) {
                    $table->increments('id');
                    if (count($fields) > 0) {
                        foreach ($fields as $field) {
                            $table->{$field['type_input']}(strtolower($field['label_input']))->nullable()->default(null);
                        }
                    }
                    $table->bigInteger('created_by')->nullable();
                    $table->bigInteger('updated_by')->nullable();
                    $table->timestamps();
                });
            }

        }

        return $dataFormulir;
    }

    public function mappingForm($request, $id)
    {
        $dataFormulir = MFormulir::whereId($id)->first();
        if ($dataFormulir->turunan_formulir != null) {
            $select_turunan = unserialize($dataFormulir->turunan_formulir);
        } else {
            $select_turunan = [];
        }
        $tampung_request = [];
        foreach($request->unit_fakultas as $c => $a ){
            $tampung_request[$c] = $a;
        }
        $marge =  array_merge($tampung_request, $select_turunan);
        $dataFormulir->update([
            'turunan_formulir' => serialize($marge)
        ]);

        $namaUnitFakultas   = UnitFakultas::whereIn('id', $request->unit_fakultas)->get();
        $data               = $namaUnitFakultas->toArray();
        for($x=0; $x < count($request->unit_fakultas); $x++){
            $dataPeriode = Periode::where('id', $dataFormulir->m_periode()->first()->id)->first();
            $dataFormulir->m_periode()->attach($dataPeriode);

            $dataUnitFakultas   = UnitFakultas::whereIn('id', [$data[$x]['id']])->get();
            $dataFormulir->m_unitfakultas()->attach($dataUnitFakultas);
        }

        return $dataFormulir;
    }


    public function persetujuan($request)
    {
        $get_id_unitFakultas = UnitFakultas::where('id', Auth::user()->get_user_detail->m_unitfakultas_id)->first();
        $dataFormulir = $get_id_unitFakultas->m_formulir();

        return $dataFormulir;
    }

    public function formulirFormCreate($request)
    {
       $alias = '';
       $words = explode(" ", $request->name);
       $acronym = "";

       foreach ($words as $w) {
           $acronym .= $w[0];
       }
       $alias = $acronym;

       $data = $request->only(['name','type']);
       $data['table_name'] = str_replace(' ','_', strtolower($request->name));
       $data['alias'] = strtoupper($alias);
       $data['created_by'] = Auth::user()->id;
       $data['updated_by'] = Auth::user()->id;
       $dataFormulir = MFormulir::create($data);

       $dataPeriode = Periode::find($request->periode);
       $dataFormulir->m_periode()->attach($dataPeriode);

        $field = [];
        for ($i=0; $i < count($request->type_input); $i++){
            $field[$i]['m_type_id'] = $request->type_input[$i];
            $field[$i]['pertanyaan'] = $request->label_input[$i];

            //validasi wajib di isi
            if ($request->is_required){
                for ($d=0; $d < count($request->get('is_required')); $d++){
                    $field[$i]['is_required'][$d] = $request->is_required[$d];
                }
            }

            //validasi jawaban singkat
            if ($request->is_type){
                if($request->is_type[$i] == 'teks'){
                    $field[$i]['merge'] = collect($request->is_type[$i])->merge($request->is_text[$i])->toArray();
                    $field[$i]['serialize'] = serialize($field[$i]['merge']);
                }else{
                    $field[$i]['merge'] = collect($request->is_type[$i])->merge($request->is_numeric[$i])->toArray();
                    $field[$i]['serialize'] = serialize($field[$i]['merge']);
                }
            }

            //validasi checkbox
            if($request->is_type_checkbox){
                $field[$i]['merge'] = collect($request->is_type_checkbox[$i])->merge($request->is_numeric_checkbox[$i])->toArray();
                $field[$i]['serialize'] = serialize($field[$i]['merge']);
            }

            //validasi paragraf
            if($request->is_type_paragraf){
                $field[$i]['merge'] = collect($request->is_type_paragraf[$i])->merge($request->is_numeric_paragraf[$i])->toArray();
                $field[$i]['serialize'] = serialize($field[$i]['merge']);
            }

            //validasi upload file
            if($request->is_type_upload){
                for($c=0; $c < count($request->is_type_upload); $c++){
                    $field[$i]['merge'] = collect($request->is_type_upload)->merge($request->is_size_file[$i])->toArray();
                    $field[$i]['serialize'] = serialize($field[$i]['merge']);
                }
            }

           $datas['m_type_id'] = $request->type_input[$i];
           $datas['pertanyaan'] = $request->label_input[$i];
        //    for ($desc=0; $desc < count($request->get('deskripsi')); $desc++){
        //        $datas['deskripsi'] = $request->get('deskripsi')[$desc];
        //    }
           $datas['created_by'] = Auth::user()->id;
           $datas['updated_by'] = Auth::user()->id;
           $dataFormulirForm[$i] = MTypeDetail::create($datas);

           $typeData = 'string';
           $formulirDetail = new Request();
           $formulirDetail['label_input'] = $request->label_input[$i];
           $formulirDetail['type_input'] = $typeData;
           $formulirDetail['queque'] = $i + 1;
           $formulirDetails = $this->formulirDetailService->create($formulirDetail);
           $dataFormulir->m_formulir_detail()->attach($formulirDetails);

           $dataType[$i] = MType::whereIn('id', [$field[$i]['m_type_id']])->get()->toArray();
        }

        // dd($field);

        foreach ($dataType as $key => $value){
            $array[] = [
                'type_id'   => $value[0]['id'],
                'name'      => $value[0]['name'],
            ];
            $type_id[$key] = $array[$key]['type_id'];
            $names[$key]    = str_replace(' ','_', strtolower($array[$key]['name']));
        }

        $temporary = [];
        for($a=0; $a < count($names); $a++){
            $temporary['m_formulirs_id'][$a] = $dataFormulir['id'];
            $temporary['type_detail_id'][$a] = $dataFormulirForm[$a]['id'];
            $temporary['value'][$a] = serialize($request->get($names[$a]));

            $arrayIsianForm = new Request();
            $arrayIsianForm['m_formulirs_id'] = $dataFormulir['id'];
            $arrayIsianForm['m_type_detail_id'] = $dataFormulirForm[$a]['id'];
            $arrayIsianForm['value'] = $request->get($names[$a]);
            $this->formulirDetailService->createIsianForm($arrayIsianForm);
        }

        $table_name = $data['table_name'];
        if (!Schema::hasTable($table_name)) {
            Schema::create($table_name, function (Blueprint $table) use ($field) {
                $table->increments('id');
                if (count($field) > 0) {
                    foreach ($field as $fields) {
                        $table->string(str_replace(' ','_',strtolower($fields['pertanyaan'])))->nullable()->default(null);
                    }
                }
                $table->bigInteger('created_by')->nullable();
                $table->bigInteger('updated_by')->nullable();
                $table->timestamps();
            });
        }

        return $dataFormulir;
    }

}
