<?php
namespace App\Services;
use Illuminate\Support\Facades\Auth;
use Modules\Data\Entities\Periode;

class PeriodeServices
{

    public function get($request)
    {
        $dataPeriode = Periode::orderBy('id')->get();
        return $dataPeriode;
    }

    public function create($request)
    {
        $data = $request->only(['semester','tgl_mulai','tgl_selesai','tahun']);
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        $dataPeriode = Periode::create($data);

        return $dataPeriode;
    }

    public function show($request , $id)
    {
        $dataPeriode = Periode::whereId($id)->first();
        return $dataPeriode;
    }

    public function update($request,$id)
    {
        $data = $request->only(['semester','tgl_mulai','tgl_selesai','tahun']);
        $data['updated_by'] = Auth::user()->id;
        $dataPeriode = Periode::whereId($id)->update($data);
        return $dataPeriode;

    }

    public function delete($request,$id)
    {
        Periode::whereId($id)->update(['deleted_by'=>Auth::user()->id]);
        $dataPeriode = Periode::whereId($id)->delete($id);
        return $dataPeriode;
    }

}
