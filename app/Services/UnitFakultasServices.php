<?php
namespace App\Services;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Session;
use Modules\Data\Entities\UnitFakultas;

class UnitFakultasServices
{

    public function get($request)
    {
        $dataUnitFakultas = UnitFakultas::orderBy('name')->get();
        return $dataUnitFakultas;
    }

    public function query($request)
    {
        $type = $request->type;
        $nama = $request->nama;

        if(!Gate::check('unit-fakultas')){
            $dataUnitFakultas = UnitFakultas::query()->where('type', 'unit');
        }else{
            $dataUnitFakultas = UnitFakultas::query();
        }

        if(!empty($type)  && $type!='Pilih Type'){
            $dataUnitFakultas = $dataUnitFakultas->where('type',$type);
        }

        if(!empty($nama) && $nama!='Pilih Nama'){
            $dataUnitFakultas = $dataUnitFakultas->where('name',$nama);
        }

        return $dataUnitFakultas;
    }

    public function byType($request)
    {
        $dataUnitFakultas = UnitFakultas::groupBy('type')->select('id','type','name')->get();
        return $dataUnitFakultas;
    }

    public function byNama($request,$type)
    {
        $dataUnitFakultas = UnitFakultas::where('type',$type)->select('id','name as text')->get();
        return $dataUnitFakultas;
    }

    public function create($request)
    {
        $namaFormulir  = $request->name;
        $hitungKalimat = str_word_count($namaFormulir);
        for ($i=0; $i < $hitungKalimat; $i++){
                $perKalimat[] = implode(" ", array_slice(explode(" ", $namaFormulir), $i, 1));
                $potongKalimat[] = substr($perKalimat[$i], 0, 1);
                $hasilManipulasi = join($potongKalimat);
        }

        $data = $request->only(['type','name']);
        $data['shortname']  = strtoupper($hasilManipulasi);
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        $dataUnitFakultas = UnitFakultas::create($data);
        $update_id = $dataUnitFakultas->update(['shortname' => $dataUnitFakultas->id.(strtoupper($hasilManipulasi))]);

        return $dataUnitFakultas;
    }

    public function show($request , $id)
    {
        $dataUnitFakultas = UnitFakultas::whereId($id)->first();
        return $dataUnitFakultas;
    }

    public function update($request,$id)
    {
        $namaFormulir  = $request->name;
        $hitungKalimat = str_word_count($namaFormulir);
        for ($i=0; $i < $hitungKalimat; $i++){
                $perKalimat[] = implode(" ", array_slice(explode(" ", $namaFormulir), $i, 1));
                $potongKalimat[] = substr($perKalimat[$i], 0, 1);
                $hasilManipulasi = join($potongKalimat);
        }

        $data = $request->only(['type','name']);
        $data['shortname']  = strtoupper($hasilManipulasi);
        $data['updated_by'] = Auth::user()->id;
        $dataUnitFakultas = UnitFakultas::whereId($id)->update($data);

        return $dataUnitFakultas;
    }

    public function delete($request,$id)
    {
        $dataUnitFakultas = UnitFakultas::whereId($id)->delete($id);
        return $dataUnitFakultas;
    }

}
