<?php
namespace App\Services;
use App\User;
use Illuminate\Support\Facades\Auth;
use Modules\Data\Entities\UnitFakultas;
use Spatie\Permission\Models\Role;

class UsersServices
{

    public function get($request)
    {
        $dataUsers = User::orderBy('name')->with('get_user_detail');
        return $dataUsers;
    }

    public function create($request)
    {
        //user permission
        $role = Role::whereIn('id',$request->role)->select('id','name')->get();
        $arr = [];
        foreach($role as $key => $value){
            $arr[$key]['id'] = $value->id;
            $arr[$key]['name'] = $value->name;
            $arr[$key]['alias'] = strtolower($value->name);
        }

        $data = $request->only(['name','email','username']);
        $data['permission'] = serialize($arr);
        $data['password'] = bcrypt($request->password);
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;

        // $filename="default.png";
        // if($request->hasFile('profile')){
        //     $file = $request->file('profile');
        //     $filename = rand().time(). '.' . $file->getClientOriginalExtension();
        //     $file->move(public_path().'/upload/profile/', $filename);
        // }

        // $data['profile'] = $filename;

        $dataUsers = User::create($data);

        $dataUsers->get_user_detail()->create([
            'type' => $request->type,
            'user_id' => $dataUsers->id,
            'm_unitfakultas_id' => $request->unit_fakultas,
            'created_by' => Auth::user()->id,
            'updated_by' => Auth::user()->id,
        ]);

        $dataUsers->assignRole($request->role);

        return $dataUsers;

    }

    public function show($request , $id)
    {
        $dataUsers = User::whereId($id)->first();
        return $dataUsers;
    }

    public function update($request,$id)
    {
        $role = Role::whereIn('id',$request->role)->select('id','name')->get();
        $arr = [];
        foreach($role as $key => $value){
            $arr[$key]['id'] = $value->id;
            $arr[$key]['name'] = $value->name;
            $arr[$key]['alias'] = strtolower($value->name);
        }

        $data = $request->only(['name','username','email']);
        $data['permission'] = serialize($arr);
        $data['updated_by'] = Auth::user()->id;

        if(!is_null($request->password)){
            $data['password'] = bcrypt($request->password);
        }

        // if($request->hasFile('profile')){
        //  $file = $request->file('profile');
        //  $filename = rand().time(). '.' . $file->getClientOriginalExtension();
        //  $file->move(public_path().'/upload/profile/', $filename);
        //  $data['profile'] = $filename;
        // }

        $dataUsers = User::whereId($id)->first();
        $dataUsers->assignRole($request->role);
        $dataUsers->update($data);

        return $dataUsers;

    }

    public function delete($request,$id)
    {
        User::whereId($id)->update(['deleted_by'=>Auth::user()->id]);
        $dataUsers = User::whereId($id)->delete($id);

        return $dataUsers;
    }

    public function getUnitFakultas($request , $type)
    {
        $dataUsers = UnitFakultas::where('type', $type)->list('name','id');
        return $dataUsers;
    }

}
