<?php
namespace App\Services;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Modules\Data\Entities\FormulirDetail;
use Modules\Data\Entities\IsianFormulirForm;
use Modules\Data\Entities\MTypeDetail;

class FormulirDetailServices
{

    public function get($request)
    {
        $dataFormulirDetail = FormulirDetail::orderBy('id')->get();
        return $dataFormulirDetail;
    }

    public function create($request)
    {
        $data['name'] = str_replace(' ','_', $request->label_input);
        $data['type'] = $request->type_input;
        $data['queque'] = $request->queque;
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        $dataFormulirDetail = FormulirDetail::create($data);

        return $dataFormulirDetail;
    }

    public function createForm($request)
    {
        $data['m_type_id'] = $request->m_type_id;
        $data['pertanyaan'] = $request->pertanyaan;
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        $dataFormulirForm = MTypeDetail::create($data);

        return $dataFormulirForm;
    }

    public function createIsianForm($request)
    {
        $data['m_formulirs_id'] = $request->m_formulirs_id;
        $data['m_type_detail_id'] = $request->m_type_detail_id;
        $data['value'] = serialize($request->value);
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        $dataIsianFormulirForm = IsianFormulirForm::create($data);

        return $dataIsianFormulirForm;
    }

    public function show($request , $id)
    {
        $dataFormulirDetail = FormulirDetail::whereId($id)->first();
        return $dataFormulirDetail;
    }

    public function update($request,$id)
    {
        $data['name'] = str_replace(' ','_', $request->label_input);
        $data['type'] = $request->type_input;
        $data['queque'] = $request->queque;
        $data['updated_by'] = Auth::user()->id;
        $dataFormulirDetail = FormulirDetail::whereId($id)->update($data);

        return $dataFormulirDetail;
    }

    public function delete($request,$id)
    {
        FormulirDetail::whereId($id)->update(['deleted_by'=>Auth::user()->id, 'deleted_at'=>Carbon::now()]);
        $dataFormulirDetail = FormulirDetail::whereId($id)->delete($id);
        return $dataFormulirDetail;
    }

}
