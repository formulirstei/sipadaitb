<?php
namespace App\Services;
use Illuminate\Support\Facades\Auth;
use Spatie\Permission\Models\Permission;

class PermissionServices
{

    public function get($request)
    {
        $dataPermission = Permission::orderBy('name')->get();
        return $dataPermission;
    }

    public function create($request)
    {
        $data = $request->only(['name']);
        $dataPermission = Permission::create($data);

        return $dataPermission;

    }

    public function show($request , $id)
    {
        $dataPermission = Permission::whereId($id)->first();
        return $dataPermission;
    }

    public function update($request,$id)
    {
        $data = $request->only(['name']);
        $dataPermission = Permission::whereId($id)->update($data);
        return $dataPermission;

    }

    public function delete($request,$id)
    {
        $dataPermission = Permission::whereId($id)->delete($id);
        return $dataPermission;
    }



}
