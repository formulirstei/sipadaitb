<?php

namespace App\Helpers;

use Carbon\Carbon;

class DatatableCustom
{

    public static function initDatatable($query, $request)
    {

        $page = 1;
        $per_page = 10;


        $params = $request->all();
        $field = $query->getModel();
        $table = $field->getTable();
        $field = $field->getFillable();
        $order_field = isset($field[0]) ? $field[0] : 'name';
        $order_sort = 'asc';

        if (isset($params['pagination']['page'])) {
            $page = $params['pagination']['page'];
        }

        if (isset($params['pagination']['perpage'])) {
            $per_page = $params['pagination']['perpage'];
            if ($per_page == -1) {
                $per_page = 10;
            }
        }

        if (isset($params['query']['generalSearch'])) {
            $query->where(function ($q) use ($table, $field, $params) {
                if ($table == "permissions" or $table == "roles") {
                    $q->where('name', 'LIKE', "%" . $params['query']['generalSearch'] . "%");
                    $q->orWhere('guard_name', 'LIKE', "%" . $params['query']['generalSearch'] . "%");
                }

                foreach ($field as $index => $valField) {
                    if ($index == 0) {
                        $q->where($table . "." . $valField, 'LIKE', "%" . $params['query']['generalSearch'] . "%");
                    } else {
                        $q->orWhere($table . "." . $valField, 'LIKE', "%" . $params['query']['generalSearch'] . "%");
                    }
                }


            });
        }
        if (isset($params['sort']['field'])) {
            $order_field = $params['sort']['field'];
            if ($params['sort']['field'] == "register_date_string") {
                $order_field = "register_date";
            }
            if ($params['sort']['field'] == "created_at_string") {
                $order_field = "created_at";
            }

            $order_sort = $params['sort']['sort'];
        }

        $total = $query->limit($per_page)->count();

        $results = $query->skip(($page - 1) * $per_page)
            ->take($per_page)->orderBy($order_field, $order_sort)
            ->get();

        $imports = [
            'meta' => [
                "page" => $page,
                "pages" => ceil($total / $per_page),
                "perpage" => $per_page,
                "total" => $total,
                "sort" => $order_sort,
                "field" => $order_field
            ],
            'data' => $results->toArray()
        ];
        return $imports;

    }

}

?>
