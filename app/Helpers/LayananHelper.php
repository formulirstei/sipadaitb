<?php

namespace App\Helpers;

use Carbon\Carbon;

class LayananHelper
{

    public static function changeTanggal($timestamp)
    {
        if (is_null($timestamp)) {
            $carbon = "-";
            echo $carbon;
        } else {
            $carbon = Carbon::parse($timestamp)->format('N,j,n,Y');
            $tgl = explode(",", $carbon);
            $array_hari = array(1 => "Senin", "Selasa", "Rabu", "Kamis", "Jumat",
                "Sabtu", "Minggu");
            $array_bulan = array(1 => "Januari", "Februari", "Maret", "April", "Mei",
                "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
            $hari = $array_hari[date($tgl[0])];
            $tanggal = date($tgl[1]);
            $bulan = $array_bulan[$tgl[2]];
            $tahun = date($tgl[3]);
            echo $hari . ", " . $tanggal . "&nbsp" . $bulan . "&nbsp" . $tahun;
        }
    }

    public static function changeTanggalDate($date, $type)
    {
        if (is_null($date)) {
            echo "-";
        } else {
            $carbon = Carbon::parse($date)->format('N,j,n,Y H:i');
            $tgl = explode(",", $carbon);
            $array_hari = array(1 => "Senin", "Selasa", "Rabu", "Kamis", "Jumat",
                "Sabtu", "Minggu");
            $array_bulan = array(1 => "Januari", "Februari", "Maret", "April", "Mei",
                "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
            $hari = $array_hari[date($tgl[0])];
            $tanggal = date($tgl[1]);
            $bulan = $array_bulan[$tgl[2]];
            $tahun = date($tgl[3]);
            if ($type == "return") {
                return $hari . ", " . $tanggal . "&nbsp" . $bulan . "&nbsp" . $tahun;
            } else {
                echo $hari . ", " . $tanggal . " " . $bulan . " " . $tahun;
            }
        }

    }

    public static function formatSizeUnits($bytes)
    {
        if ($bytes >= 1073741824) {
            $bytes = number_format($bytes / 1073741824, 2) . ' GB';
        } elseif ($bytes >= 1048576) {
            $bytes = number_format($bytes / 1048576, 2) . ' MB';
        } elseif ($bytes >= 1024) {
            $bytes = number_format($bytes / 1024, 2) . ' KB';
        } elseif ($bytes > 1) {
            $bytes = $bytes . ' bytes';
        } elseif ($bytes == 1) {
            $bytes = $bytes . ' byte';
        } else {
            $bytes = '0 bytes';
        }
        return $bytes;
    }
}

?>
