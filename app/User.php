<?php

namespace App;

use App\Helpers\LayananHelper;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Modules\Privilege\Entities\UserDetail;
use Spatie\Permission\Traits\HasRoles;

class User extends Authenticatable
{
    use Notifiable;
    use HasRoles;

    protected $fillable = ['id', 'name', 'username', 'email','password','permission','created_by','updated_by','deleted_by','created_at', 'updated_at', 'deleted_at'];
    // protected $hidden = [
    //     'password',
    // ];
    protected $guard_name = 'web';

    public function get_user_detail(){
        return $this->hasOne(UserDetail::class, 'user_id', 'id');
    }

    public function getRoleHtmlAttribute(){
        $permissions = unserialize($this->permission);
        $message ="";
        if(!empty($permissions)){
            foreach($permissions as $value){
                $message.='<span class="kt-badge kt-badge--info kt-badge--inline kt-badge--pill kt-badge--rounded">'.$value['name'].'</span> ';
            }
        }
        return $message;
    }

    public function getCreatedAt($type){
        $tanggal="";
        if ($this->created_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->created_at, $type);
        }
        return $tanggal;
    }


    public function getUpdatedAt($type){
        $tanggal="";
        if ($this->updated_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->updated_at, $type);
        }
        return $tanggal;
    }


    public function getDeletedAt($type){
        $tanggal="";
        if ($this->deleted_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->deleted_at, $type);
        }
        return $tanggal;
    }


}

