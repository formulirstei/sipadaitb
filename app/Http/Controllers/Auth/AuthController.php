<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\DB;

class AuthController extends Controller
{

    public function index()
    {
        return view('auth.index');
    }

    public function getLogin(Request $request)
    {
        $username   = $request->input('username');
        $password   = $request->input('password');

        if(Auth::guard('web')->attempt(['username' => $username, 'password' => $password])) {
            return response()->json([
                'success' => true,
                'message' => 'Login Berhasil'
            ], 200);
        } else {
            return response()->json([
                'success' => false,
                'message' => 'Terjadi Kesalahan , Login Kembali'
            ], 401);
        }
    }

    public function getLogout(Request $request)
    {
        Auth::logout();
        Session::flush();
        return redirect('/');
    }

}
