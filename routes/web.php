<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Auth\AuthController;
use Illuminate\Support\Facades\Auth;
use Modules\Data\Entities\Formulir;
use Modules\Data\Entities\MFormulir;
use Modules\Data\Entities\Periode;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [AuthController::class, 'index'])->name('login');

Route::post('login', [AuthController::class, 'getLogin']);
Route::get('logout', [AuthController::class, 'getLogout']);

Route::get('/ada', function(){

        $data = ['semester','tgl_mulai','tgl_selesai','tahun'];
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        $dataPeriode = Periode::create($data);

        $data = [];
        $data['table_name'] = str_replace(' ','_', strtolower('ada'));
        $data['alias'] = strtoupper('sds');
        $data['created_by'] = Auth::user()->id;
        $data['updated_by'] = Auth::user()->id;
        $dataFormulir = MFormulir::create($data);
        $dataFormulir->m_periode()->attach($dataPeriode);

        return true;


});
