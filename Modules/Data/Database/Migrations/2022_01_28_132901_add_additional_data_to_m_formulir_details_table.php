<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalDataToMFormulirDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_formulir_details', function (Blueprint $table) {
            $table->string('additional_data')->nullable()->after('queque');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_formulir_details', function (Blueprint $table) {
            $table->dropColumn('additional_data');
        });
    }
}
