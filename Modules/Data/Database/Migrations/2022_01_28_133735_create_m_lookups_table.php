<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMLookupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_lookups', function (Blueprint $table) {
            $table->id();
            $table->string('lookup_type')->nullable()->default(null);
            $table->string('lookup_name')->nullable()->default(null);
            $table->string('lookup_value')->nullable()->default(null);
            $table->string('lookup_queque')->nullable()->default(null);
            $table->string('lookup_code')->nullable()->default(null);
            $table->string('additional_data')->nullable()->default(null);
            $table->string('status')->nullable()->default(null);
            $table->bigInteger('approved_by')->nullable()->default(null);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamp('approved_at')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_lookups');
    }
}
