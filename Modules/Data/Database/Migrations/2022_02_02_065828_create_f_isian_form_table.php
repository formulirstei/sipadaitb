<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFIsianFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('f_isian_form', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('m_formulirs_id')->unsigned();
            $table->foreign('m_formulirs_id')->references('id')->on('m_formulirs')->onDelete('cascade');
            $table->bigInteger('m_type_detail_id')->unsigned();
            $table->foreign('m_type_detail_id')->references('id')->on('m_type_detail')->onDelete('cascade');
            $table->string('value');
            $table->enum('status',['active','nonactive','pending'])->nullable()->default(null);
            $table->enum('approved_status',['accept','decline','pending'])->nullable()->default(null);
            $table->bigInteger('approved_by')->nullable()->default(null);
            $table->dateTime('approved_at')->nullable()->default(null);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('f_isian_form');
    }
}
