<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMFormulirsTable extends Migration
{
    public function up()
    {
        Schema::create('m_formulirs', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('table_name');
            $table->string('alias');
            $table->enum('status',['active','nonactive','pending'])->nullable()->default(null);
            $table->enum('approved_status',['accept','decline','pending'])->nullable()->default(null);
            $table->enum('is_table',['0','1'])->nullable()->default('0');
            $table->enum('is_success',['0','1'])->nullable()->default('1');
            $table->bigInteger('approved_by')->nullable()->default(null);
            $table->dateTime('approved_at')->nullable()->default(null);
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->integer('deleted_by')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    public function down()
    {
        Schema::dropIfExists('m_formulirs');
    }
}
