<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMFormulirablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_formulirables', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('m_formulir_id');
            $table->foreign('m_formulir_id')->references('id')->on('m_formulirs')->onDelete('cascade');
            $table->integer('m_formulirable_id');
            $table->string('m_formulirable_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_formulirables');
    }
}
