@extends('layouts.template_backend',['title'=>'Edit Header Formulir'])
@section('sidebar')
    @include('page.page_header',['title'=>'Edit Header Formulir',(Auth::user()->can('edit formulir') ? 'add_data' : '')=>'Tambah Header Formulir','url_data'=>URL('data/formulir-edit-header/'.$dataFormulir->id ),'breadcumb'=>array('Data;#','Formulir;#') ,'url_delete'=>URL('data/deleteDataFormulir'), 'reload'=>false , 'delete_multiple'=>true , 'back'=>true])
@endsection
@section('content')
    @include('errors.validasi')

    <div class="row">
        <div class="form-group col-12">
            <span style="font-size: 14px">Formulir ini digunakan oleh :</span>
            <div class="badge ml-2" style="background: #FECDD3; font-size: 14px">
                {{isset($dataFormulir->m_unitfakultas()->first()->type) ? $dataFormulir->m_unitfakultas()->first()->type : '-' }} {{isset($dataFormulir->m_unitfakultas()->first()->name) ? $dataFormulir->m_unitfakultas()->first()->name : '-' }}
            </div>
        </div>
    </div>

    <input type="hidden" name="id" id="id" value="{{ $dataFormulir->id }}">
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable" id="datatableData"></div>
        </div>
    </div>

    @include('modal.mapping', [
      'modal_title' => 'Mapping Formulir',
      'modal_id'    => 'detail-modal',
      'modal_size'  => 'lg'
    ])

@endsection
@push('scripts')
    <script>
        var id = $('#id').val();
        var urlAjax = "{{URL('data/formulir/edit-header')}}/"+id;
        datatableData = $('#datatableData').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: urlAjax,
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState :{
                    cookie: false,
                    webstorage: false
                }
            },
            layout: {
                scroll: false,
                footer: false,
                spinner:{
                    message: 'Sedang Memuat Data...'
                }
            },
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
                delay: 300,
            },
            columns: [{
                field: 'id',
                title: '#',
                sortable: false,
                width: 20,
                selector: {
                    class: 'kt-checkbox--solid'
                },
                textAlign: 'center',
            }, {
                field: "name",
                title: "Nama Header",
            },{
                field: "created_at",
                title: "Tanggal Buat",
                template: function(row){
                    var d = new Date(row.created_at);
                    var day = d.getDate();
                    var month = d.getMonth() + 1;
                    var year = d.getFullYear();
                    if (day < 10) {
                        day = "0" + day;
                    }
                    if (month < 10) {
                        month = "0" + month;
                    }
                    var date = day + "-" + month + "-" + year;

                    return date;
                }
            }, {
                field: "Aksi",
                title: "Aksi",
                sortable: false,
                autoHide: false,
                overflow: 'visible',
                width: 100,
                template: function(row) {
                    var actions = '<div class="btn-group" permission="group">';
                    @if (Gate::check('edit formulir') or Gate::check('hapus formulir'))
                        @if(Gate::check('edit formulir'))
                            actions += '<a href="{{URL('data/formulir-header-edit')}}/'+id+'/'+row.id+'" class="btn btn-icon btn-warning btn-sm icon-white" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Edit Data"><i class="la la-pencil"></i></a>';
                        @endif
                        @if (Gate::check('hapus formulir'))
                            actions += '<button onclick="deleteData(this)" data-id="'+row.id+'" data-url="{{URL('data/formulir-header-edit')}}/'+id+'/'+row.id+'/delete" type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Hapus Data"><i class="la la-trash-o"></i></button>';
                        @endif
                    @else
                        actions += '<button type="button" class="btn btn-font-sm  btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="" data-original-title="Aksi Tidak Tersedia"><i class="la la-close"></i> Tidak Tersedia</button>';
                    @endif
                        actions += '</div>';

                    return actions;
                }
            }]
        });

    </script>
@endpush
