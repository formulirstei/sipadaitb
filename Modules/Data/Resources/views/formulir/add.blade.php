@extends('layouts.template_backend',['title'=>((!$createMode) ? "Ubah" : "Tambah").' Formulir Table'])
@section('sidebar')

@can('tambah formulir is admin')
    @include('page.page_header',['title'=>'Formulir Table','breadcumb'=>array('Data;'.URL('data/formulir'),((!$createMode) ? "Ubah" : "Tambah").' Formulir Table;#'), 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endcan
@can('tambah formulir is creator')
    @include('page.page_header',['title'=>'Formulir Table','breadcumb'=>array('Data;'.URL('data/formulir'),((!$createMode) ? "Ubah" : "Tambah").' Formulir Table;#'), 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endcan

@endsection
@section('content')
    @include('errors.validasi')

    <form permission="form" method="post"
          action="{{ $createMode ? URL("data/formulir") : URL("data/formulir/".$dataFormulir->id."") }}"
          enctype="multipart/form-data">
        @csrf

        @can('tambah formulir is creator')
            <div class="row">
                <div class="form-group col-md-6 ">
                    <div class="badge mr-2" style="background: #FECDD3; font-size: 14px">
                        Formulir ini dibuat untuk {{isset(Auth::user()->get_user_detail->get_unit_fakultas->name) ? Auth::user()->get_user_detail->get_unit_fakultas->name : '-' }}
                    </div>
                </div>
            </div>
        @endcan

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        {{((!$createMode) ? "Ubah" : "Tambah")}} Formulir
                    </h3>
                </div>
            </div>

            @if (!$createMode)
                @method('put')
            @endif

            <input type="hidden" name="type" value="formulir_table">

            <div class="kt-portlet__body">
                <div class="row">
                    <div class="form-group col-md-6 ">
                        <label>Type Formulir<span class="kt-font-danger">*</span></label>
                        <select name="type_formulir" class="form-control select2 {{ $errors->has('name') ? 'is-invalid' :'' }}" id="pilihTypeForm">
                            <option value="" selected disabled>Pilih Type Formulir</option>
                                <option value="Baru">Baru</option>
                                <option value="Import">Import</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-portlet" id="tableBaru" style="display: none">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Data Formulir
                    </h3>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="row">
                    <div class="form-group col-md-6 ">
                        <label>Nama Formulir<span class="kt-font-danger">*</span></label>
                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Masukan Nama Formulir" value="{{old('name',$dataFormulir->name)}}">
                    </div>
                    <div class="form-group col-md-6 ">
                        <label>Semester<span class="kt-font-danger">*</span></label>
                        <select name="periode" class="form-control select2 {{ $errors->has('name') ? 'is-invalid' :'' }}" id="periode_new">
                            <option value="" selected disabled>Pilih Semester</option>
                            @foreach ($dataPeriode as $value)
                                <option value="{{ $value->id }}">{{ $value->semester }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 " style="display: none" id="tgl_mulai_new">
                        <label>Tanggal Mulai<span class="kt-font-danger">*</span></label>
                        <input type="text" name="tgl_mulai" readonly id="val_tgl_mulai_new" class="form-control {{ $errors->has('tgl_mulai') ? 'is-invalid' :'' }}" autocomplete="off" value="{{old('tgl_mulai')}}  ">
                    </div>
                    <div class="form-group col-md-6 " style="display: none" id="tgl_selesai_new">
                        <label>Tanggal Selesai<span class="kt-font-danger">*</span></label>
                        <input type="text" name="tgl_selesai" readonly id="val_tgl_selesai_new" class="form-control {{ $errors->has('tgl_selesai') ? 'is-invalid' :'' }}" autocomplete="off" value="{{old('tgl_selesai')}} ">
                    </div>
                    <div class="form-group col-md-12 mt-4">
                        <div class="kt-section__content">
                            <button class="btn btn-info btn-sm" id="btnTambahField" type="button">
                                <i class="flaticon2-add-1 font-sizebtn"></i> Tambah Data Input
                            </button>

                            <div class="table-responsive kt-margin-t-15">
                                <table class="table table-striped table-bordered" id="tableField">
                                    <thead class="bg-secondary dark">
                                        <tr>
                                            <th>Label Input</th>
                                            <th>Type Input</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input type="text" name="label_input[]" class="form-control"
                                                    placeholder="Masukan Nama Label Input" >
                                            </td>
                                            <td>
                                                <select name="type_input[]" class="form-control select2">
                                                    <option value="integer">Angka</option>
                                                    <option value="string">Text</option>
                                                    {{-- <option value="">Gambar Upload</option>
                                                    <option value="">File Upload</option> --}}
                                                    <option value="date">Tanggal</option>
                                                </select>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-icon btn-sm delete" type="button">
                                                    <i class="flaticon2-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions clearfix">
                    <div class="buttons-group float-right">
                        @if($createMode)
                            <button type="submit" class="btn btn-success " name="actionsave"
                                    value="save"><i class="flaticon2-writing font-sizebtn"></i>Simpan
                            </button>
                            <button type="submit" class="btn btn-info " name="actionsave"
                                    value="savenew"><i class="flaticon2-writing font-sizebtn"></i>Simpan & Tambah Baru
                            </button>
                        @else
                            <button type="submit" class="btn btn-info "><i
                                    class="flaticon2-writing font-sizebtn"></i>Simpan Perubahan
                            </button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet" style="display: none" id="importData">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Import Data Formulir
                    </h3>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="row" >
                    <div class="form-group col-md-6 ">
                        <label>Semester<span class="kt-font-danger">*</span></label>
                        <select name="periode" class="form-control select2 {{ $errors->has('name') ? 'is-invalid' :'' }}" id="periode_import">
                            <option value="" selected disabled>Pilih Semester</option>
                            @foreach ($dataPeriode as $value)
                                <option value="{{ $value->id }}">{{ $value->semester }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 " style="display: none" id="tgl_mulai_import">
                        <label>Tanggal Mulai<span class="kt-font-danger">*</span></label>
                        <input type="text" name="tgl_mulai" readonly id="val_tgl_mulai_import" class="form-control {{ $errors->has('tgl_mulai') ? 'is-invalid' :'' }}" autocomplete="off" value="{{old('tgl_mulai')}}  ">
                    </div>

                    <div class="form-group col-md-6 " style="display: none" id="tgl_selesai_import">
                        <label>Tanggal Selesai<span class="kt-font-danger">*</span></label>
                        <input type="text" name="tgl_selesai" readonly id="val_tgl_selesai_import" class="form-control {{ $errors->has('tgl_selesai') ? 'is-invalid' :'' }}" autocomplete="off" value="{{old('tgl_selesai')}} ">
                    </div>
                </div>

                <div class="row" >
                    <div class="form-group col-md-6 ">
                        <label>Import Dengan Data?</label>
                        <select name="pilih_import" class="form-control select2" id="pilih_import">
                            <option value="" selected disabled>Pilih Data</option>
                                <option value="ya">Ya, Import dengan data</option>
                                <option value="tidak">Tidak</option>
                        </select>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-12 mt-4">
                        <div class="kt-section__content">


                            <button class="btn btn-info btn-sm" id="btnTambahFieldImport" type="button">
                                <i class="flaticon2-add-1 font-sizebtn"></i> Tambah Import
                            </button>

                            <div class="table-responsive kt-margin-t-15">
                                <table class="table table-striped table-bordered" id="tableFieldImport">
                                    <thead class="bg-secondary dark">
                                        <tr>
                                            <th>Nama Formulir</th>
                                            <th>Daftar Formulir</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>
                                                <input type="text" name="input_nama_formulir[]" class="form-control input_nama_formulir" placeholder="Masukan Nama Formulir">
                                            </td>
                                            <td>
                                                <select name="select_master_formulir[]" class="form-control select2 select_master_formulir">
                                                    <option value="" selected disabled>Pilih formulir</option>
                                                    @foreach ($dataFormulirs as $dataFormulir )
                                                        <option value="{{$dataFormulir->id}}">{{$dataFormulir->name}}</option>
                                                    @endforeach
                                                </select>
                                            </td>
                                            <td>
                                                <button class="btn btn-danger btn-icon btn-sm delete" type="button">
                                                    <i class="flaticon2-trash"></i>
                                                </button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions clearfix">
                    <div class="buttons-group float-right">
                        @if($createMode)
                            <button type="submit" class="btn btn-success " name="actionsave"
                                    value="save"><i class="flaticon2-writing font-sizebtn"></i>Simpan
                            </button>
                            <button type="submit" class="btn btn-info " name="actionsave"
                                    value="savenew"><i class="flaticon2-writing font-sizebtn"></i>Simpan & Tambah Baru
                            </button>
                        @else
                            <button type="submit" class="btn btn-info "><i
                                    class="flaticon2-writing font-sizebtn"></i>Simpan Perubahan
                            </button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
<script>
    document.querySelector(".form_name").addEventListener("keypress", function (evt) {
        if (evt.which != 8 && evt.which != 0 && evt.which != 32 && evt.which < 48 || evt.which > 57 && evt.which < 65)
        {
            evt.preventDefault();
        }
    });


    $("#btnTambahField").click(function () {
    var html  = '<tr><td><input type="text" name="label_input[]" class="form-control" placeholder="Masukan Nama Label Input">';
        html += '</td>' +
        '<td>' +
            '<select name="type_input[]" class="form-control select2">'+
                '<option value="integer">Angka</option>'+
                '<option value="string">Text</option>'+
                // '<option value="string">File</option>'+
                '<option value="date">Tanggal</option>'+
            '</select>' +
        '</td>' +
        '<td>' +
        '<button class="btn btn-danger btn-icon btn-sm delete" type="button" >' +
        '<i class="flaticon2-trash"></i>' +
        '</button>' +
        '</td>' +
        '</tr>';
    $("#tableField tbody").append(html);
    $(".select2").select2({
        placeholder: "Pilih Data",
        allowClear: true,
        width: '100%'
    });
});


$("#btnTambahFieldImport").click(function () {
    var html  = '<tr><td><input type="text" name="input_nama_formulir[]" class="form-control input_nama_formulir" placeholder="Masukan Nama Formulir">';
        html += '</td>' +
        '<td>' +
            '<select name="select_master_formulir[]" class="form-control select2 select_master_formulir">'+
                '<option value="" selected disabled>Pilih formulir</option>'+
                '@foreach ($dataFormulirs as $dataFormulir )'+
                    '<option value="{{$dataFormulir->id}}">{{$dataFormulir->name}}</option>'+
                '@endforeach'+
            '</select>'+
        '</td>' +
        '<td>' +
        '<button class="btn btn-danger btn-icon btn-sm delete" type="button" >' +
        '<i class="flaticon2-trash"></i>' +
        '</button>' +
        '</td>' +
        '</tr>';
    $("#tableFieldImport tbody").append(html);
    $(".select2").select2({
        placeholder: "Pilih Data",
        allowClear: true,
        width: '100%'
    });
});


$('#tableField').on('click', '.delete', function (e) {
    $(this).closest('tr').remove();
});
$('#tableFieldImport').on('click', '.delete', function (e) {
    $(this).closest('tr').remove();
});

$("body").on("change", ".select_master_formulir", function () {
    $get_name_formulir = $(this).closest('tr').find('.select_master_formulir option:selected').text();
    $(this).closest('tr').find('.input_nama_formulir').val($get_name_formulir);
});

$( "#periode_new" ).change(function() {
    var valPeriode = $('#periode_new').val();
    $.ajax({
            type: "GET",
            url: "{{URL('data/getPeriode')}}/"+valPeriode,
            success: function(val) {
                if(val.status=="success"){
                    $('#tgl_mulai_new').show();
                    $('#tgl_selesai_new').show();
                        var d = new Date(val['html'][0]['tgl_mulai']);var day = d.getDate();var month = d.getMonth() + 1;var year = d.getFullYear();if (day < 10) {day = "0" + day;}
                        if (month < 10) {month = "0" + month;}var date = day + "-" + month + "-" + year;
                        var e = new Date(val['html'][0]['tgl_selesai']);var day = e.getDate();var month = e.getMonth() + 1;var year = e.getFullYear();if (day < 10) {day = "0" + day;}
                        if (month < 10) {month = "0" + month;}var date2 = day + "-" + month + "-" + year;
                    $('#val_tgl_mulai_new').val(date);
                    $('#val_tgl_selesai_new').val(date2);
                    hideLoading();
                }else{
                    toastr.error(val.message);
                    $('#tgl_mulai_new').hide();
                    $('#tgl_selesai_new').hide();
                    hideLoading();
                }
            },
            error: function() {
                toastr.error("Terjadi Kesalahan , Silahkan Coba Kembali");
                hideLoading();
            }
        });
});










$( "#periode_import" ).change(function() {
    var valPeriode = $('#periode_import').val();
    $.ajax({
            type: "GET",
            url: "{{URL('data/getPeriode')}}/"+valPeriode,
            success: function(val) {
                if(val.status=="success"){
                    $('#tgl_mulai_import').show();
                    $('#tgl_selesai_import').show();
                        var d = new Date(val['html'][0]['tgl_mulai']);var day = d.getDate();var month = d.getMonth() + 1;var year = d.getFullYear();if (day < 10) {day = "0" + day;}
                        if (month < 10) {month = "0" + month;}var date = day + "-" + month + "-" + year;
                        var e = new Date(val['html'][0]['tgl_selesai']);var day = e.getDate();var month = e.getMonth() + 1;var year = e.getFullYear();if (day < 10) {day = "0" + day;}
                        if (month < 10) {month = "0" + month;}var date2 = day + "-" + month + "-" + year;
                    $('#val_tgl_mulai_import').val(date);
                    $('#val_tgl_selesai_import').val(date2);
                    hideLoading();
                }else{
                    toastr.error(val.message);
                    $('#tgl_mulai_import').hide();
                    $('#tgl_selesai_import').hide();
                    hideLoading();
                }
            },
            error: function() {
                toastr.error("Terjadi Kesalahan , Silahkan Coba Kembali");
                hideLoading();
            }
        });
});


$('#pilihTypeForm').on('change', function (e) {
    var type = $("#pilihTypeForm").val();
        if (type == "Baru") {
            $("#tableBaru").show();
            $("#importData").hide();
            $("#pilihImport").hide();
        }else{
            $("#tableBaru").hide();
            $("#importData").show();
            $("#pilihImport").show();
        }
});
</script>
@endpush
