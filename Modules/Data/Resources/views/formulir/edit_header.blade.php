@extends('layouts.template_backend',['title'=>' Edit Header Formulir'])
@section('sidebar')
    @include('page.page_header',['title'=>'Edit Header Formulir','breadcumb'=>array('Data;#','Edit Header Formulir;'.URL('data/formulir-edit-header'),("Tambah").' Edit Header Formulir;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endsection
@section('content')
    @include('errors.validasi')





    <form permission="form" method="post"
          action="{{ URL("data/formulir-edit-header/".$dataFormulir->id."/".$dataFormulirDetail->id."/update") }}"
          enctype="multipart/form-data">
        @csrf
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Edit Header Formulir
                    </h3>
                </div>
            </div>
            @method('put')
            <div class="kt-portlet__body">
                <div class="row">
                    <div class="form-group col-md-4 ">
                        <label>Nama Header<span class="kt-font-danger">*</span></label>
                        <input type="hidden" name="kolom_lama" class="form-control" value="{{ $dataFormulirDetail->name }}">
                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' :'' }}" autocomplete="off" placeholder="Masukan Nama Formulir" value="{{old('name', $dataFormulirDetail->name)}}">
                    </div>

                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions clearfix">
                    <div class="buttons-group float-right">
                        <button type="submit" class="btn btn-info "><i
                                class="flaticon2-writing font-sizebtn"></i>Simpan Perubahan
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
<script>

</script>
@endpush
