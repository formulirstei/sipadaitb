<table class="table table-bordered table-striped table-condensed">
    <tbody>

    <tr>
        <td>Digunakan Oleh </td>
            <td>{{isset($dataFormulir->m_unitfakultas()->first()->type) ? $dataFormulir->m_unitfakultas()->first()->type : '-' }} {{isset($dataFormulir->m_unitfakultas()->first()->name) ? $dataFormulir->m_unitfakultas()->first()->name : '-' }}
        </td>
    </tr>

    <tr>
        <td>Nama Formulir </td>
        <td>{{ $dataFormulir->name }}</td>
    </tr>

    <tr>
        <td>Nama Table </td>
        <td>{{ $dataFormulir->table_name }}</td>
    </tr>

    <tr>
        <td>Semester </td>
        <td>{{ $dataFormulir->m_periode()->first()->semester }}</td>
    </tr>

    <tr>
        <td>Tanggal Mulai </td>
        <td>{{ date('d F Y', strtotime($dataFormulir->m_periode()->first()->tgl_mulai)) }}</td>
    </tr>

    <tr>
        <td>Tanggal Selesai </td>
        <td>{{ date('d F Y', strtotime($dataFormulir->m_periode()->first()->tgl_selesai)) }}</td>
    </tr>

    <tr>
        <td class="w-25">Di Buat Oleh</td>
        <td class="w-75">{{(!empty($dataFormulir->get_created_by) ? $dataFormulir->get_created_by->name : '-')}}</td>
    </tr>

    <tr>
        <td class="w-25">Di Ubah Oleh</td>
        <td class="w-75">{{(!empty($dataFormulir->get_updated_by) ? $dataFormulir->get_updated_by->name : '-')}}</td>
    </tr>

    <tr>
        <td class="pad-l-20 w-25">Created at</td>
        <td class="pad-l-20 w-75">{{ $dataFormulir->getCreatedAt('echo') }}</td>
    </tr>
    <tr>
        <td class="pad-l-20 w-25">Updated at</td>
        <td class="pad-l-20 w-75">{{ $dataFormulir->getCreatedAt('echo') }}</td>
    </tr>

    </tbody>
</table>

