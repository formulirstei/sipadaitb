@extends('layouts.template_backend',['title'=>'Data Formulir'])
@section('sidebar')
    @if (Gate::check('tambah formulir is admin'))
        @include('page.page_header',['title'=>'Formulir Table',(Auth::user()->can('tambah formulir is admin') ? 'add_data' : '')=>'Tambah Formulir','url_data'=>'modal','breadcumb'=>array('Data;#','Formulir;#') ,'url_delete'=>URL('data/deleteDataFormulir'), 'reload'=>true , 'delete_multiple'=>true , 'back'=>false])
    @endif
    @if (Gate::check('tambah formulir is creator'))
        @include('page.page_header',['title'=>'Formulir Table',(Auth::user()->can('tambah formulir is creator') ? 'add_data' : '')=>'Tambah Formulir','url_data'=>'modal','breadcumb'=>array('Data;#','Formulir;#') ,'url_delete'=>URL('data/deleteDataFormulir'), 'reload'=>true , 'delete_multiple'=>true , 'back'=>false])
    @endif
@endsection
@section('content')
    @include('errors.validasi')

    <div class="kt-portlet" data-ktportlet="true">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Filter Data Formulir
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-group">
                    <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md">
                        <i class="la la-angle-down"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="form-group col-md-3 mb-2">
                    <label for="">Berdasarkan Nama</label>
                    <select class="form-control select2" id="filterNamaFormulir">
                        <option value="">Pilih Type</option>
                        @foreach ($dataFormulirs as $value)
                            <option value="{{ $value->name }}">{{ $value->name }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group col-md-3 mb-2">
                    <label for="">Berdasarkan Nama Table</label>
                    <select class="form-control select2" id="filterNamaTabel">
                        <option value="">Pilih Type</option>
                    </select>
                </div>

            </div>
        </div>
    </div>

    <div class="modal fade text-left" id="pilihFormulir" tabindex="-1">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header bg-info white">
                    <h4 class="modal-title white">
                        Pilih tipe formulir
                    </h4>
                    <button type="button" data-dismiss="modal" class="close white" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body text-center">
                    <h5>Silahkan pilih tipe formulir yang akan dibuat</h5>
                    <div class="text-center mt-5">
                        <a href="{{ URL('data/formulir/create') }}" type="button" class="btn btn-outline-secondary"> Formulir Table </a>
                        <a href="{{ URL('data/formulir-form/create') }}" type="button" class="btn btn-outline-secondary"> Formulir Form </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable" id="datatableData"></div>
        </div>
    </div>

    @include('modal.mapping', [
      'modal_title' => 'Detail Formulir',
      'modal_id'    => 'detail-modal',
      'modal_size'  => 'lg'
    ])

@endsection
@push('scripts')
            <script>
                var urlAjax = "{{URL('data/formulir')}}";
                datatableData = $('#datatableData').KTDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                method: 'GET',
                                url: urlAjax,
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                        saveState :{
                            cookie: false,
                            webstorage: false
                        }
                    },
                    layout: {
                        scroll: false,
                        footer: false,
                        spinner:{
                            message: 'Sedang Memuat Data...'
                        }
                    },
                    sortable: true,
                    pagination: true,
                    search: {
                        input: $('#generalSearch'),
                        delay: 300,
                    },
                    columns: [{
                        field: 'id',
                        title: '#',
                        sortable: false,
                        width: 20,
                        selector: {
                            class: 'kt-checkbox--solid'
                        },
                        textAlign: 'center',
                    }, {
                        field: "name",
                        title: "Nama Formulir",
                    }, {
                        field: "table_name",
                        title: "Nama Table",
                    },{
                        field: "type",
                        title: "Formulir Type",
                        template:function(row){
                            var type = row.type;
                            if (type == 'formulir_table') {
                                type = 'Table'
                            } else {
                                type = 'Form'
                            }
                            return type;
                        }
                    } ,{
                        field: "created_at",
                        title: "Tanggal Buat",
                        template: function(row){
                            var d = new Date(row.created_at);
                            var day = d.getDate();
                            var month = d.getMonth() + 1;
                            var year = d.getFullYear();
                            if (day < 10) {
                                day = "0" + day;
                            }
                            if (month < 10) {
                                month = "0" + month;
                            }
                            var date = day + "-" + month + "-" + year;

                            return date;
                        }
                    }, {
                        field: "Aksi",
                        title: "Aksi",
                        sortable: false,
                        autoHide: false,
                        overflow: 'visible',
                        width: 100,
                        template: function(row) {
                            var actions = '<div class="btn-group" permission="group">';
                            @if (Gate::check('mapping formulir') or Gate::check('detail formulir') or Gate::check('edit formulir') or Gate::check('hapus formulir'))
                                @if(Gate::check('mapping formulir'))
                                    @if (Gate::check('tambah formulir is admin'))
                                        actions += '<a href="{{URL('data/formulir')}}/'+row.id+'/mapping" class="btn btn-icon btn-success btn-sm white" data-toggle="kt-tooltip"  data-placement="left" title="" data-original-title="Mapping Formulir"><i class="la la-git"></i></a>';
                                    @endif
                                @endif
                                @if(Gate::check('detail formulir'))
                                    actions += '<a data-modal-url="{{URL('data/formulir')}}/'+row.id+'" class="btn btn-icon btn-info btn-sm btn-detail white" data-toggle="kt-tooltip"  data-placement="left" title="" data-original-title="Detail Data"><i class="la la-eye"></i></a>';
                                @endif
                                @if (Gate::check('edit formulir'))
                                    actions += '<a href="{{URL('data/formulir/edit-header')}}/'+row.id+'" class="btn btn-icon btn-warning btn-sm icon-white" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Edit Data"><i class="la la-pencil"></i></a>';
                                @endif
                                @if (Gate::check('hapus formulir'))
                                    actions += '<button onclick="deleteData(this)" data-id="'+row.id+'" data-url="{{URL('data/deleteDataFormulir')}}" type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Hapus Data"><i class="la la-trash-o"></i></button>';
                                @endif
                            @else
                                actions += '<button type="button" class="btn btn-font-sm  btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="" data-original-title="Aksi Tidak Tersedia"><i class="la la-close"></i> Tidak Tersedia</button>';
                            @endif
                                actions += '</div>';

                            return actions;
                        }
                    }]
                });


        $( "#filterNamaFormulir" ).change(function() {
            $("#filterNamaTabel").select2('destroy').empty().select2({data: [ {id: '', text: 'Pilih Nama Formulir'}]});
            getDataNama();
        });

        $( "#filterNamaTabel" ).change(function() {
            loadFilter();
        });

        function getDataNama(){
            showLoading();
            var valNamaFormulir = $('#filterNamaFormulir').val();
            $.ajax({
                type: "GET",
                url: "{{URL('data/dataFormulir')}}/"+valNamaFormulir,
                success: function(val) {
                    if(val.status=="success"){
                        $("#filterNamaTabel").select2('destroy').empty().select2({data: [ {id: '', text: 'Pilih Nama Tabel'},]}).select2({data: val.html});
                        loadFilter();
                        hideLoading();
                    }else{
                        toastr.error(val.message);
                        hideLoading();
                    }
                },
                error: function() {
                    toastr.error("Terjadi Kesalahan , Silahkan Coba Kembali");
                    hideLoading();
                }
            });
        }

        function loadFilter(){
            var valNamaFormulir = $('#filterNamaFormulir').val();
            var valNamaTable = $('#filterNamaTabel option:selected').text();
            datatableData.setDataSourceParam('namaFormulir', valNamaFormulir);
            datatableData.setDataSourceParam('namaTable', valNamaTable);

            datatableData.load();
        }

            </script>
@endpush
