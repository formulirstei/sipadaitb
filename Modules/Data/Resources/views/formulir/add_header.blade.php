@extends('layouts.template_backend',['title'=>' Tambah Header Formulir'])
@section('sidebar')
    @include('page.page_header',['title'=>'Header Formulir','breadcumb'=>array('Data;#','Header Formulir;'.URL('data/formulir-edit-header'),("Tambah").' Header Formulir;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endsection
@section('content')
    @include('errors.validasi')

    <form permission="form" method="post"
          action="{{ URL("data/formulir-edit-header/".$dataFormulir->id."/create") }}"
          enctype="multipart/form-data">
        @csrf

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Tambah Header Formulir
                    </h3>
                </div>
            </div>

            
            @method('post')

            <div class="kt-portlet__body">
                <div class="row">
                    
                    <div class="form-group col-md-4 ">
                        <label>Tambah Setelah<span class="kt-font-danger">*</span></label>
                        <select name="tambah_setelah" class="form-control select2 {{ $errors->has('tambah_setelah') ? 'is-invalid' :'' }}">
                            <option value="" selected disabled>Pilih Type Formulir</option>
                                <option value="0">Paling Awal</option>
                                @foreach ($dataFormulirDetail as $value)
                                    <option value="{{ strtolower($value->name) }}">Setelah {{ str_replace('_',' ', $value->name) }}</option>
                                @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-4 ">
                        <label>Nama Header<span class="kt-font-danger">*</span></label>
                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' :'' }}" autocomplete="off" placeholder="Masukan Nama Formulir" value="{{old('name',$dataFormulirDetails->name)}}">
                    </div>

                    <div class="form-group col-md-4" id="importData">
                        <label>Type<span class="kt-font-danger">*</span></label>
                        <select name="type" class="form-control select2 {{ $errors->has('type') ? 'is-invalid' :'' }}">
                            <option value="" selected disabled>Pilih Data</option>
                            <option value="integer">Angka</option>
                            <option value="string">Text</option>
                            {{-- <option value="string">File</option> --}}
                            <option value="date">Tanggal</option>
                        </select>
                    </div>

                </div>
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions clearfix">
                    <div class="buttons-group float-right">
                        @if($createMode)
                            <button type="submit" class="btn btn-success " name="actionsave"
                                    value="save"><i class="flaticon2-writing font-sizebtn"></i>Simpan
                            </button>
                            <button type="submit" class="btn btn-info " name="actionsave"
                                    value="savenew"><i class="flaticon2-writing font-sizebtn"></i>Simpan & Tambah Baru
                            </button>
                        @else
                            <button type="submit" class="btn btn-info "><i
                                    class="flaticon2-writing font-sizebtn"></i>Simpan Perubahan
                            </button>
                        @endif
                    </div>
                </div>
            </div>


        </div>
    </form>
@endsection

@push('scripts')
<script>
   
</script>
@endpush
