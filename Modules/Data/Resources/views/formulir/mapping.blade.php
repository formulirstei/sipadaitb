@extends('layouts.template_backend',['title' => 'Mapping Formulir'])
@section('sidebar')
    @include('page.page_header',['title'=>'Mapping Formulir','breadcumb'=>array('Data;#','Formulir;'.URL('data/formulir'), 'Mapping Formulir;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endsection
@section('content')
    @include('errors.validasi')

    <div class="row">
        <div class="form-group col-12">
            <span style="font-size: 14px">Formulir ini digunakan oleh :</span>
            <div class="badge ml-2" style="background: #FECDD3; font-size: 14px">
                {{isset($dataFormulir->m_unitfakultas()->first()->type) ? $dataFormulir->m_unitfakultas()->first()->type : '-' }} {{isset($dataFormulir->m_unitfakultas()->first()->name) ? $dataFormulir->m_unitfakultas()->first()->name : '-' }}
            </div>
        </div>
    </div>

    <form method="post" @if ($dataFormulir->type == 'formulir_table') action="{{ URL('data/formulir-mapping/'.$dataFormulir->id) }}" @else action="{{ URL('data/formulir-mapping-form/'.$dataFormulir->id) }}" @endif>
        @csrf
        @method('post')
        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Data Formulir
                    </h3>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="row">
                    <div class="form-group col-md-6 ">
                        <label>Nama Formulir</label>
                        <input type="text" name="name" class="form-control" value="{{old('name', $dataFormulir->name )}}" readonly>
                    </div>
                    <div class="form-group col-md-6 ">
                        <label>Periode</label>
                        <input type="text" name="periode" class="form-control" value="Semester {{ $dataFormulir->m_periode()->first()->semester }} - {{ date('Y', strtotime($dataFormulir->m_periode()->first()->tahun)) }}" readonly>
                    </div>
                </div>

                <div class="row">
                    <div class="form-group col-md-6 ">
                        <label>Dibuat Oleh</label>
                        <input type="text" name="#" class="form-control" value="{{isset($dataFormulir->get_created_by) ? $dataFormulir->get_created_by->name : '-' }}" readonly>
                    </div>
                    <div class="form-group col-md-6 ">
                        <label>Tanggal Buat</label>
                        <input type="text" name="#" class="form-control" value="{{isset($dataFormulir->get_created_at) ? $dataFormulir->get_created_at : '-' }}" readonly>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions clearfix">
                    <div class="h5">Formulir ini telah dibagikan kepada :</div>
                    @php
                        $a = array("#FEF2F2","#FEFCE8","#F7FEE7","#ECFEFF", "#EEF2FF", "#FDF2F8", "#F8FAFC");
                    @endphp
                    @foreach ($formulirTerpakai as $value)
                        <div class="badge mr-2 mt-2" style="background: {{$a[array_rand($a)]}}; font-size: 10px">
                            {{$value->name}}
                        </div>
                    @endforeach
                </div>
            </div>
        </div>

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Mapping Formulir
                    </h3>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="row mt-3">
                    <div class="form-group col-md-12">
                        {{-- <label>Unit dan Fakultas</label><span class="kt-font-danger">*</span> <br> --}}
                        <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success"><input type="checkbox" class="checked-all-unit_fakultas"> Untuk Semua Unit dan Fakultas <span></span> </label>
                        {{-- @if (empty($dataUnitFakultas))
                            <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success"><input type="checkbox" class="checked-all-unit_fakultas"> Untuk Semua Unit dan Fakultas <span></span> </label>

                        @else
                            <h6 class="kt-portlet__head-title text-danger">
                                Tidak ada unit dan Fakultas
                            </h6>
                        @endif --}}
                        <br>
                        <div class="row mt-4">
                            @foreach ($dataUnitFakultas as $value)
                            <div class="col-md-4">
                                <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success checked-row">
                                <input type="checkbox" name="unit_fakultas[]"
                                value="{{ $value->id }}" {{ (is_array(old('unit_fakultas',$unitFakultas)) && in_array($value->id, old('unit_fakultas',$unitFakultas))) ? ' checked' : '' }}>
                                {{ $value->name }} <span></span> </label>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions clearfix">
                    {{-- @if (empty($dataUnitFakultas)) --}}
                    <div class="buttons-group float-right">
                        <button type="submit" class="btn btn-info "><i
                                class="flaticon2-writing font-sizebtn"></i>Simpan
                        </button>
                    </div>
                    {{-- @endif --}}
                </div>
            </div>
        </div>
    </form>
@endsection
@push('scripts')
    <script>
        $(".checked-all-unit_fakultas").click(function(){
            $('.checked-row input').not(this).prop('checked', this.checked);
        });
    </script>
@endpush
