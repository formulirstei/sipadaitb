@extends('layouts.template_backend',['title'=>'Data Unit Fakultas'])
@section('sidebar')
    @include('page.page_header',['title'=>'Data Unit & Fakultas',(Auth::user()->can('tambah unit-fakultas') ? 'add_data' : '')=>'Tambah Unit & Fakultas','url_data'=>URL('data/unit-fakultas/create'),'breadcumb'=>array('Data;#','Unit Fakultas;#') ,'url_delete'=>URL('data/deleteDataUnitFakultas'), 'reload'=>true , 'delete_multiple'=>true , 'back'=>false])
@endsection
@section('content')
    @include('errors.validasi')

    <div class="kt-portlet" data-ktportlet="true">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h3 class="kt-portlet__head-title">
                    Filter Data Unit & Fakultas
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-group">
                    <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md"><i
                            class="la la-angle-down"></i></a>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="row">
                <div class="form-group col-md-3 mb-2">
                    <label for="">Berdasarkan Type</label>
                    <select class="form-control select2" id="filterType">
                        <option value="">Pilih Type</option>
                        @foreach ($dataUnitFakultasType as $value)
                            <option value="{{ $value->type }}">{{ $value->type }}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group col-md-3 mb-2">
                    <label for="">Berdasarkan Nama</label>
                    <select class="form-control select2" id="filterNama">
                        <option value="">Pilih Nama Unit & Fakultas</option>
                    </select>
                </div>

            </div>
        </div>
    </div>

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable" id="datatableData"></div>
        </div>
    </div>

    @include('modal.modal', [
      'modal_title' => 'Detail Unit Fakultas',
      'modal_id'    => 'detail-modal',
      'modal_size'  => 'lg'
    ])

@endsection
@push('scripts')
            <script>
                var urlAjax = "{{URL('data/unit-fakultas')}}";
                datatableData = $('#datatableData').KTDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                method: 'GET',
                                url: urlAjax,
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                        saveState :{
                            cookie: false,
                            webstorage: false
                        }
                    },
                    layout: {
                        scroll: false,
                        footer: false,
                        spinner:{
                            message: 'Sedang Memuat Data...'
                        }
                    },
                    sortable: true,
                    pagination: true,
                    search: {
                        input: $('#generalSearch'),
                        delay: 300,
                    },
                    columns: [{
                        field: 'id',
                        title: '#',
                        sortable: false,
                        width: 20,
                        selector: {
                            class: 'kt-checkbox--solid'
                        },
                        textAlign: 'center',
                    },{
                        field: "shortname",
                        title: "Shortname",
                    },{
                        field: "type",
                        title: "Type",
                    },{
                        field: "name",
                        title: "Nama",
                    },{
                        field: "Aksi",
                        title: "Aksi",
                        sortable: false,
                        autoHide: false,
                        overflow: 'visible',
                        width: 100,
                        template: function(row) {
                            var actions = '<div class="btn-group" permission="group">';
                            @if (Gate::check('detail unit-fakultas') or Gate::check('edit unit-fakultas') or Gate::check('hapus unit-fakultas'))
                                @if(Gate::check('detail unit-fakultas'))
                                    actions += '<a data-modal-url="{{URL('data/unit-fakultas')}}/'+row.id+'" class="btn btn-icon btn-info btn-sm btn-detail white" data-toggle="kt-tooltip"  data-placement="left" title="" data-original-title="Detail Data"><i class="la la-eye"></i></a>';
                                @endif
                                @if (Gate::check('edit unit-fakultas'))
                                    actions += '<a href="{{URL('data/unit-fakultas')}}/'+row.id+'/edit" class="btn btn-icon btn-warning btn-sm icon-white" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Edit Data"><i class="la la-pencil"></i></a>';
                                @endif
                                @if (Gate::check('hapus unit-fakultas'))
                                    actions += '<button onclick="deleteData(this)" data-id="'+row.id+'" data-url="{{URL('data/deleteDataUnitFakultas')}}" type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Hapus Data"><i class="la la-trash-o"></i></button>';
                                @endif
                            @else
                                actions += '<button type="button" class="btn btn-font-sm  btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="" data-original-title="Aksi Tidak Tersedia"><i class="la la-close"></i> Tidak Tersedia</button>';
                            @endif
                                actions += '</div>';

                            return actions;
                        }
                    }]
                });


        $( "#filterType" ).change(function() {
            $("#filterNama").select2('destroy').empty().select2({data: [ {id: '', text: 'Pilih Type'}]});
            getDataNama();
        });

        $( "#filterNama" ).change(function() {
            loadFilter();
        });

        function getDataNama(){
            showLoading();
            var valType = $('#filterType').val();
            $.ajax({
                type: "GET",
                url: "{{URL('data/unitFakultasNama')}}/"+valType,
                success: function(val) {
                    if(val.status=="success"){
                        $("#filterNama").select2('destroy').empty().select2({data: [ {id: '', text: 'Pilih Nama Unit Fakultas'},]}).select2({data: val.html});
                        loadFilter();
                        hideLoading();
                    }else{
                        toastr.error(val.message);
                        hideLoading();
                    }
                },
                error: function() {
                    toastr.error("Terjadi Kesalahan , Silahkan Coba Kembali");
                    hideLoading();
                }
            });
        }

        function loadFilter(){
            var valType = $('#filterType').val();
            var valNama = $('#filterNama option:selected').text();
            datatableData.setDataSourceParam('type', valType);
            datatableData.setDataSourceParam('nama', valNama);

            datatableData.load();
        }

    </script>
@endpush
