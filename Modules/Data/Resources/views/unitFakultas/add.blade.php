@extends('layouts.template_backend',['title'=>((!$createMode) ? "Ubah" : "Tambah").' Unit Fakultas'])
@section('sidebar')
    @include('page.page_header',['title'=>'Unit Fakultas','breadcumb'=>array('Data;#','Unit Fakultas;'.URL('data/unit-fakultas'),((!$createMode) ? "Ubah" : "Tambah").' Unit Fakultas;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endsection
@section('content')
    @include('errors.validasi')

    <form permission="form" method="post"
          action="{{ $createMode ? URL("data/unit-fakultas") : URL("data/unit-fakultas/".$dataUnitFakultas->id."") }}"
          enctype="multipart/form-data">
        @csrf

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        {{((!$createMode) ? "Ubah" : "Tambah")}} Unit & Fakultas
                    </h3>
                </div>
            </div>

            @if (!$createMode)
              @method('put')
            @endif

            <div class="kt-portlet__body">
                <div class="row">
                    <div class="form-group col-md-6 ">
                        <label>Type<span class="kt-font-danger">*</span></label>
                        <select name="type" class="form-control select2 {{ $errors->has('type') ? 'is-invalid' :'' }}">
                            <option value="" selected disabled>Pilih Data</option>
                            <option value="unit"
                                    @if(old('type',$dataUnitFakultas->type)=='unit') selected @endif>
                                    Unit
                            </option>
                            <option value="fakultas"
                                    @if(old('type',$dataUnitFakultas->type)=='fakultas') selected @endif>
                                    Fakultas
                            </option>
                        </select>
                    </div>

                    <div class="form-group col-md-6 ">
                        <label>Nama<span class="kt-font-danger">*</span></label>
                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' :'' }}" placeholder="Masukan Nama" value="{{old('name',$dataUnitFakultas->name)}}">
                    </div>
                </div>

                </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions clearfix">
                    <div class="buttons-group float-right">
                        @if($createMode)
                            <button type="submit" class="btn btn-success " name="actionsave"
                                    value="save"><i class="flaticon2-writing font-sizebtn"></i>Simpan
                            </button>
                            <button type="submit" class="btn btn-info " name="actionsave"
                                    value="savenew"><i class="flaticon2-writing font-sizebtn"></i>Simpan & Tambah Baru
                            </button>
                        @else
                            <button type="submit" class="btn btn-info "><i
                                    class="flaticon2-writing font-sizebtn"></i>Simpan Perubahan
                            </button>
                        @endif
                    </div>
                </div>
            </div>


        </div>
    </form>
@endsection

@push('scripts')
    <script>

    </script>
@endpush
