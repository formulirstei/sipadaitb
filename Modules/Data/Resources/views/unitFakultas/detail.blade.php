<table class="table table-bordered table-striped table-condensed">
    <tbody>

    <tr>
        <td>Shortname </td>
        <td>{{ $dataUnitFakultas->shortname }}</td>
    </tr>

    <tr>
        <td>Type </td>
        <td>{{ $dataUnitFakultas->type }}</td>
    </tr>

    <tr>
        <td>Nama </td>
        <td>{{ $dataUnitFakultas->name }}</td>
    </tr>

    <tr>
        <td class="w-25">Di Buat Oleh</td>
        <td class="w-75">{{(!empty($dataUnitFakultas->get_created_by) ? $dataUnitFakultas->get_created_by->name : '-')}}</td>
    </tr>

    <tr>
        <td class="w-25">Di Ubah Oleh</td>
        <td class="w-75">{{(!empty($dataUnitFakultas->get_updated_by) ? $dataUnitFakultas->get_updated_by->name : '-')}}</td>
    </tr>

    <tr>
        <td class="pad-l-20 w-25">Created at</td>
        <td class="pad-l-20 w-75">{{ $dataUnitFakultas->getCreatedAt('echo') }}</td>
    </tr>
    <tr>
        <td class="pad-l-20 w-25">Updated at</td>
        <td class="pad-l-20 w-75">{{ $dataUnitFakultas->getCreatedAt('echo') }}</td>
    </tr>

    </tbody>
</table>

