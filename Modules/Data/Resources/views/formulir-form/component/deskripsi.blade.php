<div class="mt-2 descriptionGroupOriginal" style="width: 100%; display:none">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <textarea class="custom-textarea" name="deskripsi" style="width: 100%; max-height: 45px; min-height: 45px"></textarea>
            </div>
        </div>
        <div class="col-md-6" style="margin-top: 30px">
            <a style="cursor: pointer;" class="removeDeskripsi">Hapus Deskripsi</a>
        </div>
    </div>
</div>
