<div class="kt-portlet mt-3 border fieldGroupOriginal" id="fieldGroupOriginal" style="display: none">
    <div class="kt-portlet__body">
        <div class="row tableGroup">
            <div class="table-responsive kt-margin-t-15 tableBody">
                <table class="table table-bordered" >
                    <thead class="bg-secondary dark">
                        <tr>
                            <th>Pertanyaan</th>
                            <th>Type Input</th>
                            <th>Tools</th>
                            <th style="width: 100px">Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <input type="text" name="label_input[]" class="form-control lebelInput" placeholder="Masukan pertanyaan" >
                            </td>
                            <td>
                                <select name="type_input[]" class="form-control selectTypeInput">
                                    <option value="" selected disabled>Pilih tipe input</option>
                                    @foreach($dataType as $value)
                                        <option value="{{ $value->id }}">{{ $value->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td style="width: 200px">
                                <button class="btn btn-outline-secondary btn-icon btn-sm btn-tooltip tambahDeskripsi" data-toggle="kt-tooltip"  data-placement="top" data-original-title="Tambah Deskripsi" type="button">
                                    <i class="flaticon2-list-2"></i>
                                </button>
                                <button class="btn btn-outline-secondary btn-icon btn-sm btn-tooltip validasiJawaban" data-toggle="kt-tooltip"  data-placement="top" data-original-title="Validasi Jawaban" type="button">
                                    <i class="flaticon2-lock"></i>
                                </button>
                                <label class="container btn-tooltip" data-toggle="kt-tooltip"  data-placement="top" data-original-title="Wajib Diisi">Required
                                    <input type="checkbox" name="is_required">
                                    <span class="checkmark"></span>
                                </label>
                            </td>
                            <td>
                                <button class="btn btn-danger btn-icon btn-sm delete" id="remove" type="button">
                                    <i class="flaticon2-trash"></i>
                                </button>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
