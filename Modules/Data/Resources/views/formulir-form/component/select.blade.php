<div class="mt-2 selectGroupOriginal" style="width: 100%; display:none">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" name="dropdown[]" class="custom-input {{ $errors->has('') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Opsi Select" value="{{old('')}}">
            </div>
        </div>
        <div class="col-md-4" style="margin-top: 20px">
            <a style="cursor: pointer;" class="tambahOpsiSelect" id="tambahOpsiSelect">Tambah Opsi</a> |
            <a style="cursor: pointer;" class="hapusOpsiSelect" id="hapusOpsiSelect">Hapus Opsi</a>
        </div>
    </div>
</div>
