<div class="mt-2 validasiUploadFileOriginal" style="width: 100%; display:none">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <div class="row mt-3">
                    <div class="col-md-3">
                        <input type="checkbox" name="is_type_upload[]" value="pdf">
                        <label for="">PDF</label>
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" name="is_type_upload[]" value="doc">
                        <label for="">DOC</label>
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" name="is_type_upload[]" value="docx">
                        <label for="">DOCX</label>
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" name="is_type_upload[]" value="xls">
                        <label for="">XLS</label>
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" name="is_type_upload[]" value="txt">
                        <label for="">TXT</label>
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" name="is_type_upload[]" value="jpg">
                        <label for="">JPG</label>
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" name="is_type_upload[]" value="jpeg">
                        <label for="">JPEG</label>
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" name="is_type_upload[]" value="png">
                        <label for="">PNG</label>
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" name="is_type_upload[]" value="gif">
                        <label for="">GIF</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <input type="number" name="is_size_file[]" class="custom-input {{ $errors->has('') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Maksimal size dalam MB" value="{{old('')}}">
            </div>
        </div>
        <div class="col-md-4" style="margin-top: 20px">
            <a style="cursor: pointer;" class="hapusValidasi">Hapus Validasi</a>
        </div>
    </div>
</div>
