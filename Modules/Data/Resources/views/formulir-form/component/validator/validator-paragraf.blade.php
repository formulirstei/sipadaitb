<div class="mt-2 validasiParagrafOriginal" style="width: 100%; display:none">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <select name="is_type_paragraf[]" class="custom-input validasiParagrafType">
                    <option value="karakter_maksimal">Jumlah Karakter Maksimal</option>
                    <option value="karakter_minimal">Jumlah Karakter Minimal</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <input type="number" name="is_numeric_paragraf[]" class="custom-input {{ $errors->has('') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Masukan angka" value="{{old('')}}">
            </div>
        </div>
        <div class="col-md-4" style="margin-top: 20px">
            <a style="cursor: pointer;" class="hapusValidasi">Hapus Validasi</a>
        </div>
    </div>
</div>
