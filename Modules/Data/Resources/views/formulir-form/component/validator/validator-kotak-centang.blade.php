<div class="mt-2 validasiKotakCentangOriginal" style="width: 100%; display:none">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <select name="is_type_checkbox[]" class="custom-input validasiKotakCentangType">
                    <option value="paling_sedikit">Pilih paling sedikit</option>
                    <option value="paling_banyak">Pilih paling banyak</option>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <input type="number" name="is_numeric_checkbox[]" class="custom-input {{ $errors->has('') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Masukan angka" value="{{old('')}}">
            </div>
        </div>
        <div class="col-md-4" style="margin-top: 20px">
            <a style="cursor: pointer;" class="hapusValidasi">Hapus Validasi</a>
        </div>
    </div>
</div>
