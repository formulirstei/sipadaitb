<div class="mt-2 validasiJawabanSingkatOriginal" style="width: 100%; display:none">
    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <select name="is_type[]" class="custom-input validasiJawabanSingkatType">
                    <option value="teks">Teks</option>
                    <option value="angka">Angka</option>
                </select>
            </div>
        </div>
        <div class="col-md-3 typeTeks" >
            <div class="form-group">
                <select name="is_text[]" class="custom-input">
                    <option value="bebas">Bebas</option>
                    <option value="email">Email</option>
                    <option value="url">URL</option>
                </select>
            </div>
        </div>
        <div class="col-md-3 typeAngka" style="display: none">
            <div class="form-group">
                <input type="number" name="is_numeric[]" class="custom-input {{ $errors->has('') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Panjang angka dalam digit" value="{{old('')}}">
            </div>
        </div>
        <div class="col-md-4" style="margin-top: 20px">
            <a style="cursor: pointer;" class="hapusValidasi">Hapus Validasi</a>
        </div>
    </div>
</div>
