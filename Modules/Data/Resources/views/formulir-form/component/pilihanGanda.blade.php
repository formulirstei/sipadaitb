<div class="mt-2 pilihanGandaGroupOriginal" style="width: 100%; display:none">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" name="pilihan_ganda[]" class="custom-input {{ $errors->has('') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Opsi Pilihan Ganda" value="{{old('')}}">
            </div>
        </div>
        <div class="col-md-4" style="margin-top: 20px">
            <a style="cursor: pointer;" class="tambahOpsiPilihanGanda" id="tambahOpsiPilihanGanda">Tambah Opsi</a> |
            <a style="cursor: pointer;" class="hapusOpsiPilihanGanda" id="hapusOpsiPilihanGanda">Hapus Opsi</a>
        </div>
    </div>
</div>
