<div class="mt-2 kotakCentangGroupOriginal" style="width: 100%; display:none">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <input type="text" name="kotak_centang[]" class="custom-input {{ $errors->has('') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Opsi Kotak Centang" value="{{old('')}}">
            </div>
        </div>
        <div class="col-md-4" style="margin-top: 20px">
            <a style="cursor: pointer;" class="tambahOpsiKotakCentang" id="tambahOpsiKotakCentang">Tambah Opsi</a> |
            <a style="cursor: pointer;" class="hapusOpsiKotakCentang" id="hapusOpsiKotakCentang">Hapus Opsi</a>
        </div>
    </div>
</div>
