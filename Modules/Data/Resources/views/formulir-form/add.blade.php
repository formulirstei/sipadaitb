@extends('layouts.template_backend',['title'=>((!$createMode) ? "Ubah" : "Tambah").' Formulir'])
@section('sidebar')

@can('tambah formulir is admin')
    @include('page.page_header',['title'=>'Formulir','breadcumb'=>array('Data;'.URL('data/formulir'),((!$createMode) ? "Ubah" : "Tambah").' Formulir;#'), 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endcan
@can('tambah formulir is creator')
    @include('page.page_header',['title'=>'Formulir','breadcumb'=>array('Data;'.URL('data/formulir'),((!$createMode) ? "Ubah" : "Tambah").' Formulir;#'), 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endcan


@endsection
@section('content')

    @include('errors.validasi')
    <form id="form" name="content" permission="form" method="post"
          action="{{ $createMode ? URL("data/formulir-form") : URL("data/formulir-form/".$dataFormulir->id."") }}"
          enctype="multipart/form-data">
        @csrf

        @can('tambah formulir is creator')
            <div class="row">
                <div class="form-group col-md-6 ">
                    <div class="badge mr-2" style="background: #FECDD3; font-size: 14px">
                        Formulir ini dibuat untuk {{isset(Auth::user()->get_user_detail->get_unit_fakultas->name) ? Auth::user()->get_user_detail->get_unit_fakultas->name : '-' }}
                    </div>
                </div>
            </div>
        @endcan

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        {{((!$createMode) ? "Ubah" : "Tambah")}} Formulir
                    </h3>
                </div>
            </div>

            @if (!$createMode)
                @method('put')
            @endif

            <input type="hidden" name="type" value="formulir_form">

            <div class="kt-portlet__body">
                <div class="row">
                    <div class="form-group col-md-6 ">
                        <label>Type Formulir<span class="kt-font-danger">*</span></label>
                        <select name="type_formulir" class="form-control select2 {{ $errors->has('name') ? 'is-invalid' :'' }}" id="pilihTypeForm">
                            <option value="" disabled>Pilih Type Formulir</option>
                                <option value="baru" selected>Baru</option>
                                <option value="import">Import</option>
                        </select>
                    </div>
                </div>
            </div>
        </div>

        <div class="kt-portlet" id="tableBaru" style="display: none">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        Data Formulir
                    </h3>
                </div>
            </div>

            <div class="kt-portlet__body">
                <div class="row">
                    <div class="form-group col-md-6 ">
                        <label>Nama Formulir<span class="kt-font-danger">*</span></label>
                        <input type="text" name="name" id="name_formulir" class="form-control {{ $errors->has('name') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Masukan Nama Formulir" value="{{old('name')}}" required>
                        <div class="text-right mt-1 mr-1" id="validate_name_formulir">Wajib diisi</div>
                    </div>
                    <div class="form-group col-md-6 ">
                        <label>Semester<span class="kt-font-danger">*</span></label>
                        <select name="periode" class="form-control select2 {{ $errors->has('periode') ? 'is-invalid' :'' }}" id="periode_new">
                            <option value="" selected disabled>Pilih Semester</option>
                             @foreach ($dataPeriode as $value)
                                <option value="{{ $value->id }}">{{ $value->semester }}</option>
                            @endforeach
                        </select>
                        <div class="text-right mt-1 mr-1" id="validate_periode_formulir">Wajib diisi</div>
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-6 " style="display: none" id="tgl_mulai_new">
                        <label>Tanggal Mulai<span class="kt-font-danger">*</span></label>
                        <input type="text" name="tgl_mulai" readonly id="val_tgl_mulai_new" class="form-control {{ $errors->has('tgl_mulai') ? 'is-invalid' :'' }}" autocomplete="off" value="{{old('tgl_mulai')}}  ">
                    </div>
                    <div class="form-group col-md-6 " style="display: none" id="tgl_selesai_new">
                        <label>Tanggal Selesai<span class="kt-font-danger">*</span></label>
                        <input type="text" name="tgl_selesai" readonly id="val_tgl_selesai_new" class="form-control {{ $errors->has('tgl_selesai') ? 'is-invalid' :'' }}" autocomplete="off" value="{{old('tgl_selesai')}} ">
                    </div>
                    <div class="form-group col-md-12 mt-4">
                        <div class="kt-section__content">
                            <button class="btn btn-info btn-sm tambahID" id="btnTambahField" type="button">
                                <i class="flaticon2-add-1 font-sizebtn"></i> Tambah Data Input
                            </button>
                            <div class="group"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="kt-portlet__foot">
                <div class="kt-form__actions clearfix">
                    <div class="buttons-group float-right">
                        @if($createMode)
                            <button type="submit" class="btn btn-success saveArray" name="actionsave" value="save"><i class="flaticon2-writing font-sizebtn"></i>Simpan
                            </button>
                            <button type="button" class="btn btn-info saveArray" name="actionsave" value="savenew"><i class="flaticon2-writing font-sizebtn"></i>Simpan & Tambah Baru
                            </button>
                        @else
                            <button type="button" class="btn btn-info "><i class="flaticon2-writing font-sizebtn"></i>Simpan Perubahan
                            </button>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection

@push('scripts')
<script>
    // token for csrf
    let _token   = $('meta[name="csrf-token"]').attr('content');

    //default id formulir
    var id_formulir = null;


    // fungsi delay update data
    function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
            callback.apply(context, args);
            }, ms || 0);
        };
    }

    // ajax setup csrf token
    $.ajaxSetup ({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    // fungsi get data periode select
    $( "#periode_new" ).change(function() {
        var valPeriode = $('#periode_new').val();
        $.ajax({
            type: "GET",
            url: "{{URL('data/getPeriode')}}/"+valPeriode,
            success: function(val) {
                if(val.status=="success"){
                    $('#tgl_mulai_new').show();
                    $('#tgl_selesai_new').show();
                    var d = new Date(val['html'][0]['tgl_mulai']);var day = d.getDate();var month = d.getMonth() + 1;var year = d.getFullYear();if (day < 10) {day = "0" + day;}
                    if (month < 10) {month = "0" + month;}var date = day + "-" + month + "-" + year;
                    var e = new Date(val['html'][0]['tgl_selesai']);var day = e.getDate();var month = e.getMonth() + 1;var year = e.getFullYear();if (day < 10) {day = "0" + day;}
                    if (month < 10) {month = "0" + month;}var date2 = day + "-" + month + "-" + year;
                    $('#val_tgl_mulai_new').val(date);
                    $('#val_tgl_selesai_new').val(date2);
                    hideLoading();
                }else{
                    toastr.error(val.message);
                    $('#tgl_mulai_new').hide();
                    $('#tgl_selesai_new').hide();
                    hideLoading();
                }
            },
            error: function() {
                toastr.error("Terjadi Kesalahan , Silahkan Coba Kembali");
                hideLoading();
            }
        });

    });


    //fungsi pilih type form
    var type = $('#pilihTypeForm').val();
    if (type == "baru") {
        $("#tableBaru").show();
        $("#importData").hide();
        $("#pilihImport").hide();
        $(".saveArray").prop('disabled', true);
    } else {
        $("#tableBaru").hide();
        $("#importData").show();
        $("#pilihImport").show();
    }

    // $('#pilihTypeForm').on('change', function (e) {
    // var type = $("#pilihTypeForm").val();
    //     if (type == "baru") {
    //         $("#tableBaru").show();
    //         $("#importData").hide();
    //         $("#pilihImport").hide();
    //         $(".saveArray").prop('disabled', true);
    //     }else{
    //         $("#tableBaru").hide();
    //         $("#importData").show();
    //         $("#pilihImport").show();
    //     }
    // });


    // all variable for element
    var pilihanGandaGroupOriginal = ' <div class="row"> <div class="col-md-6"> <div class="form-group"> <input type="text" name="pilihan_ganda[]" class="custom-input {{ $errors->has('') ? "is-invalid" : "" }} form_name" autocomplete="off" placeholder="Opsi Pilihan Ganda" value="{{old('')}}"> </div> </div> <div class="col-md-4" style="margin-top: 20px"> <a style="cursor: pointer;" class="tambahOpsiPilihanGanda" id="tambahOpsiPilihanGanda">Tambah Opsi</a> | <a style="cursor: pointer;" class="hapusOpsiPilihanGanda" id="hapusOpsiPilihanGanda">Hapus Opsi</a> </div> </div>';
    var kotakCentangGroupOriginal = ' <div class="row"> <div class="col-md-6"> <div class="form-group"> <input type="text" name="kotak_centang[]" class="custom-input {{ $errors->has('') ? "is-invalid" : "" }} form_name" autocomplete="off" placeholder="Opsi Kotak Centang" value="{{old('')}}"> </div> </div> <div class="col-md-4" style="margin-top: 20px"> <a style="cursor: pointer;" class="tambahOpsiKotakCentang" id="tambahOpsiKotakCentang">Tambah Opsi</a> | <a style="cursor: pointer;" class="hapusOpsiKotakCentang" id="hapusOpsiKotakCentang">Hapus Opsi</a> </div> </div>';
    var selectGroupOriginal = ' <div class="row"> <div class="col-md-6"> <div class="form-group"> <input type="text" name="dropdown[]" class="custom-input {{ $errors->has("") ? "is-invalid" : "" }} form_name" autocomplete="off" placeholder="Opsi Select" value="{{old('')}}"> </div> </div> <div class="col-md-4" style="margin-top: 20px"> <a style="cursor: pointer;" class="tambahOpsiSelect" id="tambahOpsiSelect">Tambah Opsi</a> | <a style="cursor: pointer;" class="hapusOpsiSelect" id="hapusOpsiSelect">Hapus Opsi</a> </div> </div>';


    // fungsi tambah field group
    $("#btnTambahField").click(function (e) {
        e.preventDefault();
        var id = $('.fieldGroupCopy').length;
        var check_nama_formulir = $('#name_formulir').val();
        var check_periode = $('#periode_new').val();

        // check nama formulir kosong
        if((check_nama_formulir == "")){
            $('#validate_name_formulir').addClass('text-danger')
            $('#validate_periode_formulir').removeClass('text-danger')
        }else if(check_periode == null){
            $('#validate_periode_formulir').addClass('text-danger')
            $('#validate_name_formulir').removeClass('text-danger')
        }else{
            $('#validate_periode_formulir').removeClass('text-danger')
            $('#validate_name_formulir').removeClass('text-danger')

            //add element group function and post data type
            function fieldGroupCopy(){
                showLoading();
                $.ajax({
                    url: "{{ route('ajaxRequestPostTypeMFormulir') }}",
                    type: "POST",
                    data: {
                        "id_formulir": "ada",
                    },
                    dataType: 'JSON',
                    success: function(val) {
                        if(val['success'] == true){
                            toastr.success(val.message);
                            hideLoading();
                            // variable element
                            var fieldHTML = '<div class="kt-portlet mt-3 border fieldGroupCopy" id="fieldGroupCopy'+id+'">' + '<div class="kt-portlet__body"><div class="row tableGroup"><div class="table-responsive kt-margin-t-15 tableBody"><table class="table table-bordered" ><thead class="bg-secondary dark"><tr><th>Pertanyaan</th><th>Type Input</th><th>Tools</th><th style="width: 100px">Aksi</th></tr></thead><tbody><tr><td><input type="text" name="label_input[]" id="pertanyaan'+id+'" class="form-control" placeholder="Masukan pertanyaan" ></td><td><select name="type_input[]" id="type'+id+'" class="form-control selectTypeInput"><option value="" selected disabled>Pilih tipe input</option>@foreach($dataType as $value)<option value="{{ $value->id }}">{{ $value->name }}</option>@endforeach</select></td><td style="width: 200px"><button class="btn btn-outline-secondary btn-icon btn-sm btn-tooltip mr-1 tambahDeskripsi" data-toggle="kt-tooltip"  data-placement="top" data-original-title="Tambah Deskripsi" type="button"><i class="flaticon2-list-2"></i></button><button class="btn btn-outline-secondary btn-icon btn-sm btn-tooltip validasiJawaban" data-toggle="kt-tooltip"  data-placement="top" data-original-title="Validasi Jawaban" type="button"><i class="flaticon2-lock"></i></button><label class="container btn-tooltip" data-toggle="kt-tooltip"  data-placement="top" data-original-title="Wajib Diisi">Required<input type="checkbox" name="is_required" id="required'+id+'"><span class="checkmark"></span></label></td><td><button class="btn btn-danger btn-icon btn-sm delete" id="remove" type="button"><i class="flaticon2-trash"></i></button></td></tr></tbody></table></div></div></div>' + '</div>';
                            $('body').find('.group').after(fieldHTML);
                            $('.btn-tooltip').tooltip();
                            $(".saveArray").prop('disabled', false);
                        }else{
                            toastr.error(val.message);
                            hideLoading();
                        }
                    },
                    error: function() {
                        toastr.error("data gagal dibuat");
                        hideLoading();
                    }
                });
            }

            if(!($("body").find('.fieldGroupCopy'))[0]){
                showLoading();
                $.ajax({
                    url: "{{URL('data/formulir-form/create/post-m-formulir')}}",
                    type: "POST",
                    data: {
                        "nama_formulir": check_nama_formulir,
                        "periode": check_periode,
                    },
                    dataType: 'JSON',
                    success: function(val) {
                        if(val['success'] == true){
                            toastr.success(val.message);
                            id_formulir = val.id;
                            hideLoading();
                            fieldGroupCopy();
                        }else{
                            toastr.error(val.message);
                            hideLoading();
                        }
                    },
                    error: function() {
                        toastr.error("data gagal disimpan");
                        hideLoading();
                    }
                });
            }else{
                fieldGroupCopy();
            }
        }
    });


    // update nama formulir
    $("#name_formulir").keyup(delay(function (e) {
        if(id_formulir != null ){
            var check_nama_formulir = $('#name_formulir').val();
            if(check_nama_formulir != ""){
                showLoading();
                $.ajax({
                    url: "{{ route('ajaxRequestUpdateMFormulir') }}",
                    type: "PUT",
                    data: {
                        "id_formulir": id_formulir,
                        "nama_formulir": check_nama_formulir,
                    },
                    dataType: 'JSON',
                    success: function(val) {
                        if(val['success'] == true){
                            toastr.success(val.message);
                            hideLoading();
                        }else{
                            toastr.error(val.message);
                            hideLoading();
                        }
                    },
                    error: function() {
                        toastr.error("data gagal diupdate");
                        hideLoading();
                    }
                });
            }
        }
    }, 500));

    $("body").on("click", ".validasiJawaban", function () {
        var element = $(this).closest('.fieldGroupCopy');
        var get_id =  element[0].id;
        var id = get_id.substr(get_id.length - 1);
        var elementValidasi = element.find('.validasiJawaban');
        $get_type = element.find('.selectTypeInput option:selected').val();
        if ($get_type == '3') {
            if (!(element.find('.validasiJawabanSingkatCopy')[0])){
                var fieldHTML = '<div id="validasi'+id+'" class="mt-2 validasiJawabanSingkatCopy" style="width: 100%">' + '<div class="row"> <div class="col-md-3"> <div class="form-group"> <select name="is_type[]" class="custom-input validasiJawabanSingkatType"> <option value="teks">Teks</option> <option value="angka">Angka</option> </select> </div> </div> <div class="col-md-3 typeTeks" > <div class="form-group"> <select name="is_text[]" class="custom-input"> <option value="bebas">Bebas</option> <option value="email">Email</option> <option value="url">URL</option> </select> </div> </div> <div class="col-md-3 typeAngka" style="display: none"> <div class="form-group"> <input type="number" name="is_numeric[]" class="custom-input {{ $errors->has('') ? "is-invalid" :"" }} form_name" autocomplete="off" placeholder="Panjang angka dalam digit" value="{{old('')}}"> </div> </div> <div class="col-md-4" style="margin-top: 20px"> <a style="cursor: pointer;" class="hapusValidasi">Hapus Validasi</a></div></div>' + '</div>';
                element.find('.tableBody').after(fieldHTML);
                elementValidasi.addClass('btn-primary hapusValidasi');
            } else {
                elementValidasi.removeClass('btn-primary hapusValidasi');
            }
        } else if($get_type == '4'){
            if (!(element.find('.validasiParagrafCopy')[0])){
                var fieldHTML = '<div id="validasi'+id+'" class="mt-2 validasiParagrafCopy" style="width: 100%">' + '<div class="row"> <div class="col-md-3"> <div class="form-group"> <select name="is_type_paragraf[]" class="custom-input validasiParagrafType"> <option value="karakter_maksimal">Jumlah Karakter Maksimal</option> <option value="karakter_minimal">Jumlah Karakter Minimal</option> </select> </div> </div> <div class="col-md-3"> <div class="form-group"> <input type="number" name="is_numeric_paragraf[]" class="custom-input {{ $errors->has('') ? "is-invalid" :"" }} form_name" autocomplete="off" placeholder="Masukan angka" value="{{old('')}}"> </div> </div> <div class="col-md-4" style="margin-top: 20px"> <a style="cursor: pointer;" class="hapusValidasi">Hapus Validasi</a> </div> </div>' + '</div>';
                element.find('.tableBody').after(fieldHTML);
                elementValidasi.addClass('btn-primary hapusValidasi');
            } else {
                elementValidasi.removeClass('btn-primary hapusValidasi');
            }
        } else if($get_type == '2'){
            if (!(element.find('.validasiKotakCentangCopy')[0])){
                var fieldHTML = '<div id="validasi'+id+'" class="mt-2 validasiKotakCentangCopy" style="width: 100%">' + '<div class="row"> <div class="col-md-3"> <div class="form-group"> <select name="is_type_checkbox[]" class="custom-input validasiKotakCentangType"> <option value="paling_sedikit">Pilih paling sedikit</option> <option value="paling_banyak">Pilih paling banyak</option> </select> </div> </div> <div class="col-md-3"> <div class="form-group"> <input type="number" name="is_numeric_checkbox[]" class="custom-input {{ $errors->has('') ? "is-invalid" :"" }} form_name" autocomplete="off" placeholder="Masukan angka" value="{{old('')}}"> </div> </div> <div class="col-md-4" style="margin-top: 20px"> <a style="cursor: pointer;" class="hapusValidasi">Hapus Validasi</a> </div> </div>' + '</div>';
                element.find('.tableBody').after(fieldHTML);
                elementValidasi.addClass('btn-primary hapusValidasi');
            } else {
                elementValidasi.removeClass('btn-primary hapusValidasi');
            }
        } else if($get_type == '6'){
            if (!(element.find('.validasiUploadFileCopy')[0])){
                var fieldHTML = '<div id="validasi'+id+'" class="mt-2 validasiUploadFileCopy" style="width: 100%">' + '<div class="row"> <div class="col-md-3"> <div class="form-group"> <div class="row mt-3"> <div class="col-md-3"> <input type="checkbox" name="is_type_upload'+id+'[]" value="pdf"> <label for="">PDF</label> </div> <div class="col-md-3"> <input type="checkbox" name="is_type_upload'+id+'[]" value="doc"> <label for="">DOC</label> </div> <div class="col-md-3"> <input type="checkbox" name="is_type_upload'+id+'[]" value="docx"> <label for="">DOCX</label> </div> <div class="col-md-3"> <input type="checkbox" name="is_type_upload'+id+'[]" value="xls"> <label for="">XLS</label> </div> <div class="col-md-3"> <input type="checkbox" name="is_type_upload'+id+'[]" value="txt"> <label for="">TXT</label> </div> <div class="col-md-3"> <input type="checkbox" name="is_type_upload'+id+'[]" value="jpg"> <label for="">JPG</label> </div> <div class="col-md-3"> <input type="checkbox" name="is_type_upload'+id+'[]" value="jpeg"> <label for="">JPEG</label> </div> <div class="col-md-3"> <input type="checkbox" name="is_type_upload'+id+'[]" value="png"> <label for="">PNG</label> </div> <div class="col-md-3"> <input type="checkbox" name="is_type_upload'+id+'[]" value="gif"> <label for="">GIF</label> </div> </div> </div> </div> <div class="col-md-3"> <div class="form-group"> <input type="number" name="is_size_file[]" class="custom-input {{ $errors->has('') ? "is-invalid" : "" }} form_name" autocomplete="off" placeholder="Maksimal size dalam MB" value="{{old('')}}"> </div> </div> <div class="col-md-4" style="margin-top: 20px"> <a style="cursor: pointer;" class="hapusValidasi">Hapus Validasi</a> </div> </div>' + '</div>';
                element.find('.tableBody').after(fieldHTML);
                elementValidasi.addClass('btn-primary hapusValidasi');
            } else {
                elementValidasi.removeClass('btn-primary hapusValidasi');
            }
        }
    });

    $("body").on("change", ".validasiJawabanSingkatType", function () {
        var element = $(this).closest('.fieldGroupCopy');
        $get_type = element.find('.validasiJawabanSingkatType option:selected').val();
        if($get_type == 'teks'){
            element.find('.typeTeks').show();
            element.find('.typeAngka').hide();
        }else{
            element.find('.typeTeks').hide();
            element.find('.typeAngka').show();
        }
    });

    $("body").on("click", ".tambahDeskripsi", function () {
        var element = $(this).closest('.fieldGroupCopy');
        var get_id =  element[0].id;
        var id = get_id.substr(get_id.length - 1);
        $get_type = element.find('.selectTypeInput option:selected').val();
        if($get_type != ''){
            if (!(element.find('.descriptionGroupCopy')[0])){
                var fieldHTML = '<div class="mt-5 descriptionGroupCopy" style="width: 100%">' + '<div class="row"> <div class="col-md-6"> <div class="form-group"> <textarea class="custom-textarea" name="deskripsi" id="deskripsi'+id+'" style="width: 100%; max-height: 45px; min-height: 45px"></textarea> </div> </div> <div class="col-md-6" style="margin-top: 30px"> <a style="cursor: pointer;" class="removeDeskripsi">Hapus Deskripsi</a> </div> </div>' + '</div>';
                element.find('.tableGroup').after(fieldHTML);
                var elementDeskripsi = element.find('.tambahDeskripsi');
                if(elementDeskripsi.find(".removeDeskripsi")[0]){
                    element.find(".descriptionGroupCopy").remove();
                    elementDeskripsi.removeClass("btn-primary removeDeskripsi");
                } else {
                    elementDeskripsi.addClass("btn-primary removeDeskripsi");
                }
            }
        }
    });

    $("body").on("change", ".selectTypeInput", function () {
        var element = $(this).closest('.fieldGroupCopy');
        var elementDeskripsi = element.find('.tambahDeskripsi');
        var elementValidasi = element.find('.validasiJawaban');
        $get_type = element.find('.selectTypeInput option:selected').val();
        element.find('.descriptionGroupCopy').remove(); element.find('.validasiJawabanSingkatCopy').remove(); element.find('.validasiParagrafCopy').remove(); element.find('.validasiKotakCentangCopy').remove(); element.find('.validasiUploadFileCopy').remove(); element.find('.validasiUploadGambarCopy').remove(); element.find(".descriptionGroupCopy").remove(); elementDeskripsi.removeClass("btn-primary removeDeskripsi"); elementValidasi.removeClass('btn-primary hapusValidasi');
        if($get_type == '1'){
            //remove previous html
            element.find('.kotakCentangGroupCopy').remove()
            element.find('.selectGroupCopy').remove()
            //end remove

            var fieldHTML = '<div class="mt-2 pilihanGandaGroupCopy" style="width: 100%">' + pilihanGandaGroupOriginal + '</div>';
            element.find('.tableBody').after(fieldHTML);

        }else if($get_type == '2'){
            //remove previous html
            element.find('.pilihanGandaGroupCopy').remove()
            element.find('.selectGroupCopy').remove()
            //end remove

            var fieldHTML = '<div class="mt-2 kotakCentangGroupCopy" style="width: 100%">' + kotakCentangGroupOriginal + '</div>';
            element.find('.tableBody').after(fieldHTML);
        }else if($get_type == '5'){
            //remove previous html
            element.find('.pilihanGandaGroupCopy').remove()
            element.find('.kotakCentangGroupCopy').remove()
            //end remove

            var fieldHTML = '<div class="mt-2 selectGroupCopy" style="width: 100%">' + selectGroupOriginal + '</div>';
            element.find('.tableBody').after(fieldHTML);
        }else{
            element.find('.selectGroupCopy').remove()
            element.find('.pilihanGandaGroupCopy').remove()
            element.find('.kotakCentangGroupCopy').remove()
        }
    });

    $("body").on("click", ".tambahOpsiPilihanGanda", function () {
        var fieldHTML = '<div class="mt-2 pilihanGandaGroupCopy" style="width: 100%">' + pilihanGandaGroupOriginal + '</div>';
        $(this).closest('.fieldGroupCopy').find('.pilihanGandaGroupCopy:last').after(fieldHTML);
    });

    $("body").on("click", ".tambahOpsiKotakCentang", function () {
        var fieldHTML = '<div class="mt-2 kotakCentangGroupCopy" style="width: 100%">' + kotakCentangGroupOriginal + '</div>';
        $(this).closest('.fieldGroupCopy').find('.kotakCentangGroupCopy:last').after(fieldHTML);
    });

    $("body").on("click", ".tambahOpsiSelect", function () {
        var fieldHTML = '<div class="mt-2 selectGroupCopy" style="width: 100%">' + selectGroupOriginal + '</div>';
        $(this).closest('.fieldGroupCopy').find('.selectGroupCopy:last').after(fieldHTML);
    });



    //if page refresh, remove all html

    if (window.performance) {
        console.log("window.halaman dibuka pertamakali");
    }
    if (performance.navigation.type == performance.navigation.TYPE_RELOAD) {
        console.log( "halaman direload" );
    } else {
        console.log( "halaman tidak direload");
    }


    //remove atau hapus element
    $("body").on("click", "#remove", function () {
        var countFieldGroup = $('.fieldGroupCopy').length;
        let currow = $(this).closest('.fieldGroupCopy');
        $(this).parents(".fieldGroupCopy").remove();
        if($("body").find('.fieldGroupCopy')[0]){
            $(".saveArray").prop('disabled', false);
        }else{
            $(".saveArray").prop('disabled', true);
        }
        if(countFieldGroup == 1){
            showLoading();
            $.ajax({
                url: "{{ route('ajaxRequestDeleteMFormulir') }}",
                type: "DELETE",
                data: {
                    "id_formulir": id_formulir,
                },
                dataType: 'JSON',
                success: function(val) {
                    if(val['success'] == true){
                        toastr.success(val.message);
                        id_formulir = null;
                        hideLoading();
                    }else{
                        toastr.error(val.message);
                        hideLoading();
                    }
                },
                error: function() {
                    toastr.error("data gagal dihapus");
                    hideLoading();
                }
            });
        }
    });


    //remove all element
    $("body").on("click", ".removeDeskripsi", function (e) {
        var element = $(this).closest('.fieldGroupCopy');
        var elementDeskripsi = element.find('.tambahDeskripsi');
        element.find(".descriptionGroupCopy").remove();
        elementDeskripsi.removeClass("btn-primary removeDeskripsi");
    });

    $("body").on("click", "#hapusOpsiPilihanGanda", function () {
        let currow = $(this).closest('.fieldGroupCopy');
        $(this).parents(".pilihanGandaGroupCopy").remove();
    });

    $("body").on("click", "#hapusOpsiKotakCentang", function () {
        let currow = $(this).closest('.fieldGroupCopy');
        $(this).parents(".kotakCentangGroupCopy").remove();
    });

    $("body").on("click", "#hapusOpsiSelect", function () {
        let currow = $(this).closest('.fieldGroupCopy');
        $(this).parents(".selectGroupCopy").remove();
    });

    $("body").on("click", ".hapusValidasi", function () {
        var element = $(this).closest('.fieldGroupCopy');
        var elementValidasi = element.find('.validasiJawaban');
        element.find(".validasiJawabanSingkatCopy").remove();
        element.find(".validasiParagrafCopy").remove();
        element.find(".validasiKotakCentangCopy").remove();
        element.find(".validasiUploadFileCopy").remove();
        element.find(".validasiUploadGambarCopy").remove();
        elementValidasi.removeClass('btn-primary hapusValidasi');
    });





    // click fungsi simpan create fix data
    $('.saveArray').click(function (e) {
        if($("body").find('.fieldGroupCopy')[0]){
            var id = $('.fieldGroupCopy').length;
            let type_formulir = $("select[name=type_formulir]").val();
            let nama_formulir = $("input[name=name]").val();
            let id_periode = $("#periode_new").val();
            let tgl_mulai = $("#val_tgl_mulai_new").val();
            let tgl_selesai = $("#val_tgl_selesai_new").val();
            let _token   = $('meta[name="csrf-token"]').attr('content');
            var all_input = [];
            var pertanyaan = [];
            var type = [];
            var deskripsi = [];
            var required = [];
            var validasi = [];

            for (let i = 0; i < id; i++) {
                pertanyaan[i] = $('#fieldGroupCopy'+i+'').find('#pertanyaan'+i+'').val()
            }
            for (let i = 0; i < id; i++) {
                type[i] = $('#fieldGroupCopy'+i+'').find('#type'+i+'').val()
            }
            for (let i = 0; i < id; i++) {
                deskripsi[i] = $('#fieldGroupCopy'+i+'').find('#deskripsi'+i+'').val()
            }
            for (let i = 0; i < id; i++) {
                if ($('#fieldGroupCopy'+i+'').find('#required'+i+'').is(':checked')) {
                    required[i] = $('#fieldGroupCopy'+i+'').find('#required'+i+'').val()
                } else {
                    required[i] = 'off';
                }
            }
            for (let i = 0; i < id; i++) {
                // $('#fieldGroupCopy'+i+'').find('#validasi'+i+' :checked').each(function () {
                //     validasi.push($(this).val());
                // });
                // validasi[i] = $('#fieldGroupCopy'+i+'').find('#validasi'+i+' :input').serializeArray()
                $('#fieldGroupCopy'+i+'').find('#validasi'+i+' :checked').each(function () {
                    validasi.push($(this).val());
                });
                // validasi[i] = validasi[i] += '&validasi='+validasi.join(',');
            }

            if ((type_formulir == "") || (nama_formulir== "") || (id_periode="")) {
                alert("Terdapat field kosong yang harus diisi");
                return false;
            } else {
                // $.ajax({
                //     type: "POST",
                //     url: "/data/formulir-form/save-data",
                //     data: {
                //         _token: _token,
                //         type_formulir: type_formulir,
                //         nama_formulir: nama_formulir,
                //         id_periode: id_periode,
                //         tgl_mulai: tgl_mulai,
                //         tgl_selesai: tgl_selesai,
                //         pertanyaan: pertanyaan,
                //         type: type,
                //         deskripsi: deskripsi,
                //         required: required,
                //         validasi: validasi
                //     },
                //     dataType: "json",
                //     success: function (response) {
                //         console.log (response);
                //     }
                // });
            }
        } else{
            $(".saveArray").prop('disabled', true);
        }
        // for (let i = 0; i < id; i++) {
        //     all_input.push({
        //         all_input : $('#fieldGroupCopy'+i+' :input').serializeArray()
        //     });
        // }
    });
</script>
@endpush
