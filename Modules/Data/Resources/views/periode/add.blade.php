@extends('layouts.template_backend',['title'=>((!$createMode) ? "Ubah" : "Tambah").' Periode'])
@section('sidebar')
    @include('page.page_header',['title'=>'Periode','breadcumb'=>array('Data;#','Periode;'.URL('data/periode'),((!$createMode) ? "Ubah" : "Tambah").' Periode;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endsection
@section('content')
    @include('errors.validasi')

    <form permission="form" method="post"
          action="{{ $createMode ? URL(config('settings.page_backend')."data/periode") : URL("data/periode/".$dataPeriode->id."") }}"
          enctype="multipart/form-data">
        @csrf

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        {{((!$createMode) ? "Ubah" : "Tambah")}} Periode
                    </h3>
                </div>
            </div>

            @if (!$createMode)
              @method('put')
            @endif

            <div class="kt-portlet__body">
                <div class="row">

                    <div class="form-group col-md-3 ">
                        <label>Semester<span class="kt-font-danger">*</span></label>
                        <select name="semester" class="form-control select2 {{ $errors->has('semester') ? 'is-invalid' :'' }}">
                            <option value="" selected disabled>Pilih Semester</option>
                            <option value="1"
                                    @if(old('semester',$dataPeriode->semester)=='1') selected @endif>
                                    1 (Satu)
                            </option>
                            <option value="2"
                                    @if(old('semester',$dataPeriode->semester)=='2') selected @endif>
                                    2 (Dua)
                            </option>
                        </select>
                    </div>

                    <div class="form-group col-md-3 ">
                        <label>Tahun<span class="kt-font-danger">*</span></label>
                        <input type="month" name="tahun" class="form-control {{ $errors->has('tahun') ? 'is-invalid' :'' }}" placeholder="Masukan Tahun" value="{{old('tahun',$dataPeriode->tahun)}}">
                    </div>

                    <div class="form-group col-md-3 ">
                        <label>Tanggal Mulai<span class="kt-font-danger">*</span></label>
                        <input type="date" name="tgl_mulai" class="form-control {{ $errors->has('tgl_mulai') ? 'is-invalid' :'' }}" placeholder="Tanggal Mulai" value="{{ old('tgl_mulai', $dataPeriode->tgl_mulai) }}">
                    </div>

                    <div class="form-group col-md-3 ">
                        <label>Tanggal Selesai<span class="kt-font-danger">*</span></label>
                        <input type="date" name="tgl_selesai" class="form-control {{ $errors->has('tgl_selesai') ? 'is-invalid' :'' }}" placeholder="Tanggal Selesai" value="{{ old('tgl_selesai', $dataPeriode->tgl_selesai) }}">
                    </div>

                </div>
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions clearfix">
                    <div class="buttons-group float-right">
                        @if($createMode)
                            <button type="submit" class="btn btn-success " name="actionsave"
                                    value="save"><i class="flaticon2-writing font-sizebtn"></i>Simpan
                            </button>
                            <button type="submit" class="btn btn-info " name="actionsave"
                                    value="savenew"><i class="flaticon2-writing font-sizebtn"></i>Simpan & Tambah Baru
                            </button>
                        @else
                            <button type="submit" class="btn btn-info "><i
                                    class="flaticon2-writing font-sizebtn"></i>Simpan Perubahan
                            </button>
                        @endif
                    </div>
                </div>
            </div>

        </div>
    </form>
@endsection

@push('scripts')
    <script>

    </script>
@endpush
