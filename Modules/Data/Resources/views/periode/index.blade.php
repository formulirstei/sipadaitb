@extends('layouts.template_backend',['title'=>'Data Periode'])
@section('sidebar')
    @include('page.page_header',['title'=>'Data Periode',(Auth::user()->can('tambah periode') ? 'add_data' : '')=>'Tambah Periode','url_data'=>URL('data/periode/create'),'breadcumb'=>array('Master;#','Periode;#') ,'url_delete'=>URL('data/deleteDataPeriode'), 'reload'=>true , 'delete_multiple'=>true , 'back'=>false])
@endsection
@section('content')
    @include('errors.validasi')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable" id="datatableData"></div>
        </div>
    </div>

    @include('modal.modal', [
      'modal_title' => 'Detail Periode',
      'modal_id'    => 'detail-modal',
      'modal_size'  => 'lg'
    ])

@endsection
@push('scripts')
            <script>
                var urlAjax = "{{URL('data/periode')}}";
                datatableData = $('#datatableData').KTDatatable({
                    data: {
                        type: 'remote',
                        source: {
                            read: {
                                method: 'GET',
                                url: urlAjax,
                            },
                        },
                        pageSize: 10,
                        serverPaging: true,
                        serverFiltering: true,
                        serverSorting: true,
                        saveState :{
                            cookie: false,
                            webstorage: false
                        }
                    },
                    layout: {
                        scroll: false,
                        footer: false,
                        spinner:{
                            message: 'Sedang Memuat Data...'
                        }
                    },
                    sortable: true,
                    pagination: true,
                    search: {
                        input: $('#generalSearch'),
                        delay: 300,
                    },
                    columns: [{
                        field: 'id',
                        title: '#',
                        sortable: false,
                        width: 20,
                        selector: {
                            class: 'kt-checkbox--solid'
                        },
                        textAlign: 'center',
                    }, {
                        field: "semester",
                        title: "Semester",
                    },{
                        field: "tgl_mulai",
                        title: "Tanggal Mulai",
                        template: function(row) {
                            var tglMulai = row.tgl_mulai;
                            var d = new Date(tglMulai);
                            var day = d.getDate();
                            var month = d.getMonth() + 1;
                            var year = d.getFullYear();
                            if (day < 10) {
                                day = "0" + day;
                            }
                            if (month < 10) {
                                month = "0" + month;
                            }
                            var date = day + "-" + month + "-" + year;

                            return date;
                        }
                    },{
                        field: "tgl_selesai",
                        title: "Tanggal Akhir",
                        template: function(row) {
                            var tglSelesai = row.tgl_selesai;
                            var d = new Date(tglSelesai);
                            var day = d.getDate();
                            var month = d.getMonth() + 1;
                            var year = d.getFullYear();
                            if (day < 10) {
                                day = "0" + day;
                            }
                            if (month < 10) {
                                month = "0" + month;
                            }
                            var date = day + "-" + month + "-" + year;

                            return date;
                        }
                    },{
                        field: "tahun",
                        title: "Tahun",
                        template: function(row) {
                            var tahun = row.tahun;
                            var d = new Date(tahun);
                            var year = d.getFullYear();
                            var date = year;

                            return date;
                        }
                    },{
                        field: "Aksi",
                        title: "Aksi",
                        sortable: false,
                        autoHide: false,
                        overflow: 'visible',
                        width: 100,
                        template: function(row) {
                            var actions = '<div class="btn-group" permission="group">';
                            @if (Gate::check('detail periode') or Gate::check('edit periode') or Gate::check('hapus periode'))
                                @if(Gate::check('detail periode'))
                                    actions += '<a data-modal-url="{{URL('data/periode')}}/'+row.id+'" class="btn btn-icon btn-info btn-sm btn-detail white" data-toggle="kt-tooltip"  data-placement="left" title="" data-original-title="Detail Data"><i class="la la-eye"></i></a>';
                                @endif
                                @if (Gate::check('edit periode'))
                                    actions += '<a href="{{URL('data/periode')}}/'+row.id+'/edit" class="btn btn-icon btn-warning btn-sm icon-white" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Edit Data"><i class="la la-pencil"></i></a>';
                                @endif
                                @if (Gate::check('hapus periode'))
                                    actions += '<button onclick="deleteData(this)" data-id="'+row.id+'" data-url="{{URL('data/deleteDataPeriode')}}" type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Hapus Data"><i class="la la-trash-o"></i></button>';
                                @endif
                            @else
                                actions += '<button type="button" class="btn btn-font-sm  btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="" data-original-title="Aksi Tidak Tersedia"><i class="la la-close"></i> Tidak Tersedia</button>';
                            @endif
                                actions += '</div>';

                            return actions;
                        }
                    }]
                });

            </script>
@endpush