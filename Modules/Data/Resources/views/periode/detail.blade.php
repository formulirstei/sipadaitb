<table class="table table-bordered table-striped table-condensed">
    <tbody>

    <tr>
        <td>Semester </td>
        <td>{{ $dataPeriode->semester }}</td>
    </tr>

    <tr>
        <td>Tahun</td>
        <td>{{ date('Y', strtotime($dataPeriode->tahun)) }}</td>
    </tr>

    <tr>
        <td>Tanggal Mulai </td>
        <td>{{ date('d M Y', strtotime($dataPeriode->tgl_mulai)) }}</td>
    </tr>

    <tr>
        <td>Tanggal Selesai </td>
        <td>{{ date('d M Y', strtotime($dataPeriode->tgl_selesai)) }}</td>
    </tr>

    <tr>
        <td class="w-25">Di Buat Oleh</td>
        <td class="w-75">{{(!empty($dataPeriode->get_created_by) ? $dataPeriode->get_created_by->name : '-')}}</td>
    </tr>

    <tr>
        <td class="w-25">Di Ubah Oleh</td>
        <td class="w-75">{{(!empty($dataPeriode->get_updated_by) ? $dataPeriode->get_updated_by->name : '-')}}</td>
    </tr>

    <tr>
        <td class="pad-l-20 w-25">Created at</td>
        <td class="pad-l-20 w-75">{{ $dataPeriode->getCreatedAt('echo') }}</td>
    </tr>
    <tr>
        <td class="pad-l-20 w-25">Updated at</td>
        <td class="pad-l-20 w-75">{{ $dataPeriode->getCreatedAt('echo') }}</td>
    </tr>

    </tbody>
</table>

