<?php

namespace Modules\Data\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use App\Helpers\DatatableCustom;
use App\Services\FormulirServices;
use App\Services\PeriodeServices;
use App\Services\UnitFakultasServices;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Schema;
use Modules\Data\Entities\FormulirDetail;
use Modules\Data\Entities\MFormulir;
use Modules\Data\Entities\UnitFakultas;
use Modules\Data\Http\Requests\FormulirValidasi;

class FormulirController extends Controller
{

    public function __construct(FormulirServices $formulirServices ,
                                MFormulir $mFormulir,
                                PeriodeServices $periodeServices,
                                UnitFakultasServices $unitFakultasServices,
                                FormulirDetail $formulirDetailServices)
    {
        $this->formulirService  = $formulirServices;
        $this->periodeService  = $periodeServices;
        $this->unitFakultasService  = $unitFakultasServices;
        $this->formulirDetailService  = $formulirDetailServices;
        $this->formulir = $mFormulir;
    }


    public function index(Request $request)
    {

        if (!Gate::check('formulir')) {
            return abort(403);
        }

        if (!$request->ajax()) {
            $dataFormulirs = $this->formulirService->byNamaFormulir($request);
            return view('data::formulir.index', compact('dataFormulirs'));
        } else {
            if (Gate::check('tambah formulir is admin')){
                $dataFormulir = $this->formulirService->query($request);
            } else {
                $get_id_unitFakultas = UnitFakultas::where('id', Auth::user()->get_user_detail->m_unitfakultas_id)->first();
                $dataFormulir = $get_id_unitFakultas->m_formulir();
            }
            return DatatableCustom::initDatatable($dataFormulir,$request);
        }
    }

    public function create(Request $request, $is_creator = false)
    {

        // if(!Gate::check('tambah formulir is admin')) {
        //     return abort(403);
        // }

        // if(!Gate::check('tambah formulir is creator')) {
        //     return abort(403);
        // }

        $dataPeriode = $this->periodeService->get($request);
        $dataFormulirs = MFormulir::get();
        $dataFormulir = $this->formulir;

        $createMode=true;

        return view('data::formulir.add',compact('dataPeriode','dataFormulir','dataFormulirs','createMode'));
    }

    public function store(FormulirValidasi $request)
    {

        // if (!Gate::check('tambah formulir is admin') || !Gate::check('tambah formulir is creator')) {
        //     return abort(403);
        // }

        $dataFormulir = $this->formulirService->create($request);

        if (!is_null($dataFormulir)) {
            $message = "Berhasil Menambah Data Formulir";
            if ($request->actionsave == "save") {
                return Redirect::to('data/formulir')->with("message", $message);
            } else {
                return Redirect::to('data/formulir/create')->with("message", $message);
            }
        }

        return Redirect::to('data/formulir')->with("messageerror", "Gagal Menambah Data Formulir");

    }

    public function show(Request $request, $id)
    {
        if (!Gate::check('detail formulir')) {
            return abort(403);
        }

        if (!$request->ajax()) {
            return abort(403);
        }

        $dataFormulir = $this->formulirService->show($request,$id);
        if (is_null($dataFormulir)) {
            return Redirect::to('data/formulir')->with("messageerror", "Data Formulir Tidak Tersedia");
        }

        return view('data::formulir.detail', compact('dataFormulir'));

    }

    public function edit(Request $request, $id)
    {
        if (!Gate::check('edit formulir')) {
            return abort(403);
        }
        $dataPeriode    = $this->periodeService->get($request);
        $dataFormulir   = $this->formulirService->show($request,$id);
        $dataFormulirs  = $this->formulirService->byNamaFormulir($request);

        if (is_null($dataFormulir)) {
            return Redirect::to('data/formulir')->with("messageerror", "Data Formulir Tidak Tersedia");
        }

        $createMode = false;
        return view('data::formulir.add', compact('dataFormulir','dataFormulirs','dataPeriode','createMode'));

    }

    public function update(FormulirValidasi $request, $id)
    {
        if (!Gate::check('edit formulir')) {
            return abort(403);
        }

        $dataFormulir = $this->formulirService->show($request,$id);
        if (is_null($dataFormulir)) {
            return Redirect::to('data/formulir')->with("messageerror", "Data Formulir Tidak Tersedia");
        }

        $updatDataFormulir = $this->formulirService->update($request,$id);

        return Redirect::to('data/formulir')->with("message", "Data Formulir Berhasil Di Perbaharui");

    }

    public function deleteData(Request $request)
    {
        if (!Gate::check('hapus formulir')) {
            return abort(403);
        }

        $idarray = $request->id;
        $deleteGagal = 0;
        $deleteSukses = 0;
        if ($request->ajax()) {
            if (!is_null($idarray)) {
                for ($i = 0; $i < count($idarray); $i++) {
                    $id = $request->id[$i];
                    $dataFormulirDelete = MFormulir::whereId($id)->first();
                    Schema::dropIfExists($dataFormulirDelete->table_name);
                    $this->formulirService->delete($request,$id);
                    $deleteSukses = $deleteSukses + 1;
                }
            }
            if ($deleteGagal > 0) {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => '' . $deleteSukses . ' Data Formulir Berhasil Di Hapus dan ' . $deleteGagal . ' Data Formulir Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => '' . $deleteGagal . ' Data Formulir Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                }
            } else {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Data Formulir Berhasil Di Hapus'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Tidak Ada Data Formulir Yang Di Hapus'
                    ]);
                }
            }

        }
    }

    public function getDataFormulir(Request $request, $table_name = null)
    {

        if (is_null($table_name)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Terjadi Kesalahan Dalam Pengambilan Data , Silakan Coba Kembali'
            ]);
        }


        if (!$request->ajax()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Mohon Maaf Request Error !'
            ]);
        }

        $dataFormulir = $this->formulirService->byNamaTable($request, $table_name);

        if ($dataFormulir->isEmpty()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Data Formulir Tidak Tersedia'
            ]);
        }

        return response()->json([
            'status' => 'success',
            'html' => $dataFormulir
        ]);

    }

    public function getDataPeriode(Request $request, $id = null)
    {

        if (is_null($id)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Terjadi Kesalahan Dalam Pengambilan Data , Silakan Coba Kembali'
            ]);
        }


        if (!$request->ajax()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Mohon Maaf Request Error !'
            ]);
        }

        $dataUnitFakultas = $this->formulirService->getPeriode($request, $id);

        if ($dataUnitFakultas->isEmpty()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Data Periode Tidak Tersedia'
            ]);
        }

        return response()->json([
            'status' => 'success',
            'html' => $dataUnitFakultas
        ]);

    }

    public function mapping(Request $request, $id)
    {
        if (!Gate::check('mapping formulir')) {
            return abort(403);
        }
        // $dataUnitFakultas = $this->unitFakultasService->get($request);
        $dataFormulir = $this->formulirService->show($request,$id);
        $unitFakultas = $dataFormulir->m_unitfakultas()->get()->pluck('id')->toArray();
        if (empty($unitFakultas)) {
            if ($dataFormulir->turunan_formulir != null) {
                $unitFakultas = unserialize($dataFormulir->turunan_formulir);
            }
        } elseif(($dataFormulir->turunan_formulir != null) && ( $unitFakultas != null)){
            $unitFakultas_formulir = unserialize($dataFormulir->turunan_formulir);
            $unitFakultas_formulirable = $dataFormulir->m_unitfakultas()->get()->pluck('id')->toArray();
            $unitFakultas = array_merge($unitFakultas_formulir ,  $unitFakultas_formulirable);
        } else {
            $unitFakultas = $dataFormulir->m_unitfakultas()->get()->pluck('id')->toArray();
        }

        $dataUnitFakultas = UnitFakultas::whereNotIn('id', $unitFakultas)->get();
        $formulirTerpakai = UnitFakultas::whereIn('id', $unitFakultas)->get();
        // dd($dataUnitFakultas);
        return view('data::formulir.mapping', compact('dataUnitFakultas','dataFormulir', 'unitFakultas', 'formulirTerpakai'));
    }

    public function getHeader(Request $request, $id)
    {
        if (!Gate::check('edit formulir')) {
            return abort(403);
        }

        if (!$request->ajax()) {
            $dataFormulir = $this->formulirService->show($request,$id);
            $unitFakultas = $dataFormulir->m_unitfakultas()->get()->pluck('id')->toArray();
            if (empty($unitFakultas)) {
                if ($dataFormulir->turunan_formulir != null) {
                    $unitFakultas = unserialize($dataFormulir->turunan_formulir);
                }
            } elseif(($dataFormulir->turunan_formulir != null) && ( $unitFakultas != null)){
                $unitFakultas_formulir = unserialize($dataFormulir->turunan_formulir);
                $unitFakultas_formulirable = $dataFormulir->m_unitfakultas()->get()->pluck('id')->toArray();
                $unitFakultas = array_merge($unitFakultas_formulir ,  $unitFakultas_formulirable);
            } else {
                $unitFakultas = $dataFormulir->m_unitfakultas()->get()->pluck('id')->toArray();
            }
            $formulirTerpakai = UnitFakultas::whereIn('id', $unitFakultas)->get();

            return view('data::formulir.edit', compact('dataFormulir', 'formulirTerpakai'));
        } else {
            $dataFormulirDetail = $this->formulirService->getHeader($request, $id);
            return DatatableCustom::initDatatable($dataFormulirDetail,$request);
        }

    }

    public function getEditHeader(Request $request, $id)
    {
        if (!Gate::check('edit formulir')) {
            return abort(403);
        }

        $dataFormulir           = $this->formulirService->show($request,$id);
        $dataFormulirDetail     = $this->formulirService->getHeader($request, $id)->get();
        $dataFormulirDetails    = $this->formulirDetailService;

        $createMode=true;

        return view('data::formulir.add_header', compact('createMode','dataFormulir','dataFormulirDetail','dataFormulirDetails'));
    }

    public function createHeader(Request $request, $id)
    {
        if (!Gate::check('edit formulir')) {
            return abort(403);
        }

        $dataFormulir = $this->formulirService->createHeader($request, $id);

        if (!is_null($dataFormulir)) {
            $message = "Berhasil Menambah Data Header Formulir";
            if ($request->actionsave == "save") {
                return Redirect::to('data/formulir/edit-header/'.$id)->with("message", $message);
            } else {
                return Redirect::to('data/formulir-edit-header/'.$id)->with("message", $message);
            }
        }

        return Redirect::to('data/formulir/edit-header/'.$id)->with("messageerror", "Gagal Menambah Data Formulir");
    }

    public function EditHeader(Request $request, $id, $f_detail_id)
    {
        if (!Gate::check('edit formulir')) {
            return abort(403);
        }

        $dataFormulir           = $this->formulirService->show($request,$id);
        $dataFormulirDetail     = $this->formulirService->getHeaderOne($request, $id, $f_detail_id);
        $dataFormulirDetails    = $this->formulirDetailService;

        if (is_null($dataFormulirDetail)) {
            return Redirect::to('data/formulir/edit-header/'.$id)->with("messageerror", "Data Tidak Tersedia");
        }

        return view('data::formulir.edit_header', compact('dataFormulirDetail','dataFormulir'));
    }

    public function updateHeader(Request $request, $id, $f_detail_id)
    {
        if (!Gate::check('edit formulir')) {
            return abort(403);
        }

        $updatDataFormulir = $this->formulirService->updateHeader($request,$id, $f_detail_id);


        return Redirect::to('data/formulir/edit-header/'.$id)->with("message", "Data Header Formulir Berhasil Di Perbaharui");
    }

    public function deleteDataHeader(Request $request, $id, $f_detail_id)
    {
        if (!Gate::check('hapus formulir')) {
            return abort(403);
        }

        $idarray = $request->id;
        $deleteGagal = 0;
        $deleteSukses = 0;
        if ($request->ajax()) {
            if (!is_null($idarray)) {
                for ($i = 0; $i < count($idarray); $i++) {
                    $id = $request->id[$i];
                    $this->formulirService->deleteHeader($request,$id);
                    $deleteSukses = $deleteSukses + 1;
                }
            }
            if ($deleteGagal > 0) {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => '' . $deleteSukses . ' Data Formulir Berhasil Di Hapus dan ' . $deleteGagal . ' Data Formulir Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => '' . $deleteGagal . ' Data Formulir Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                }
            } else {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Data Formulir Berhasil Di Hapus'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Tidak Ada Data Formulir Yang Di Hapus'
                    ]);
                }
            }

        }
    }

    public function createMapping(Request $request, $id)
    {
        if (!Gate::check('mapping formulir')) {
            return abort(403);
        }

        if($request->unit_fakultas==null){
            return Redirect::to('data/formulir')->with("messageerror", 'Unit fakultas Harus Di Pilih Terlebih Dahulu')->withInput($request->all());
        }

        $updateDataFormulir = $this->formulirService->mapping($request,$id);

        if (!is_null($updateDataFormulir)) {
            $message = "Berhasil Menambah Data Formulir";
            return Redirect::to('data/formulir')->with("message", $message);
        }

        return Redirect::to('data/formulir')->with("messageerror", "Gagal Mapping Data Formulir");

    }

    public function createMappingForm(Request $request, $id)
    {
        // if (!Gate::check('mapping formulir')) {
        //     return abort(403);
        // }
        if($request->unit_fakultas==null){
            return Redirect::to('data/formulir')->with("messageerror", 'Unit fakultas Harus Di Pilih Terlebih Dahulu')->withInput($request->all());
        }

        $updateDataFormulir = $this->formulirService->mappingForm($request,$id);

        if (!is_null($updateDataFormulir)) {
            $message = "Berhasil Menambah Data Formulir";
            return Redirect::to('data/formulir')->with("message", $message);
        }

        return Redirect::to('data/formulir')->with("messageerror", "Gagal Mapping Data Formulir");
    }

}
