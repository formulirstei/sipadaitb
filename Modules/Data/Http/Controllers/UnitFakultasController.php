<?php

namespace Modules\Data\Http\Controllers;

use App\Helpers\DatatableCustom;
use App\Services\UnitFakultasServices;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Modules\Data\Entities\UnitFakultas;
use Modules\Data\Http\Requests\UnitFakultasValidasi;

class UnitFakultasController extends Controller
{
    public function __construct(UnitFakultasServices $unitFakultasServices, UnitFakultas $unitFakultas)
    {
        $this->unitFakultasService  = $unitFakultasServices;
        $this->unitFakultas  = $unitFakultas;
    }

    public function index(Request $request)
    {
        if (!Gate::check('unit-fakultas')) {
            return abort(403);
        }

        if (!$request->ajax()) {
            $dataUnitFakultasType = $this->unitFakultasService->byType($request);
            return view('data::unitFakultas.index', compact('dataUnitFakultasType'));
        } else {
            $dataUnitFakultas = $this->unitFakultasService->query($request);
            return DatatableCustom::initDatatable($dataUnitFakultas,$request);
        }
    }

    public function create()
    {
        if (!Gate::check('tambah unit-fakultas')) {
            return abort(403);
        }

        $dataUnitFakultas = $this->unitFakultas;
        $createMode = true;

        return view('data::unitFakultas.add', compact('dataUnitFakultas','createMode'));
    }

    public function store(UnitFakultasValidasi $request)
    {
        if (!Gate::check('tambah unit-fakultas')) {
            return abort(403);
        }

        $dataUnitFakultas = $this->unitFakultasService->create($request);

        if (!is_null($dataUnitFakultas)) {
            $message = "Berhasil Menambah Data Unit & Fakultas";
            if ($request->actionsave == "save") {
                return Redirect::to('data/unit-fakultas')->with("message", $message);
            } else {
                return Redirect::to('data/unit-fakultas/create')->with("message", $message);
            }
        }

        return Redirect::to('data/unit-fakultas')->with("messageerror", "Gagal Menambah Data Unit & Fakultas");
    }

    public function show(Request $request, $id)
    {
        if (!Gate::check('detail unit-fakultas')) {
            return abort(403);
        }

        if (!$request->ajax()) {
            return abort(403);
        }

        $dataUnitFakultas = $this->unitFakultasService->show($request,$id);
        if (is_null($dataUnitFakultas)) {
            return Redirect::to('data/unit-fakultas')->with("messageerror", "Data Unit Fakultas Tidak Tersedia");
        }

        return view('data::unitFakultas.detail', compact('dataUnitFakultas'));
    }

    public function edit(Request $request,$id)
    {
        if (!Gate::check('edit unit-fakultas')) {
            return abort(403);
        }

        $dataUnitFakultas = $this->unitFakultasService->show($request,$id);
        if (is_null($dataUnitFakultas)) {
            return Redirect::to('data/unit-fakultas')->with("messageerror", "Data Unit Fakultas Tidak Tersedia");
        }

        $createMode = false;

        return view('data::unitFakultas.add', compact('dataUnitFakultas','createMode'));
    }

    public function update(UnitFakultasValidasi $request, $id)
    {
        if (!Gate::check('edit unit-fakultas')) {
            return abort(403);
        }

        $dataUnitFakultas = $this->unitFakultasService->show($request,$id);
        if (is_null($dataUnitFakultas)) {
            return Redirect::to('data/unit-fakultas')->with("messageerror", "Data Unit Fakultas Tidak Tersedia");
        }

        $updatDataUnitFakultas = $this->unitFakultasService->update($request,$id);

        return Redirect::to('data/unit-fakultas')->with("message", "Data Unit Fakultas Berhasil Di Perbaharui");
    }

    public function deleteData(Request $request)
    {
        if (!Gate::check('hapus unit-fakultas')) {
            return abort(403);
        }

        $idarray = $request->id;
        $deleteGagal = 0;
        $deleteSukses = 0;
        if ($request->ajax()) {
            if (!is_null($idarray)) {
                for ($i = 0; $i < count($idarray); $i++) {
                    $id = $request->id[$i];
                    $this->unitFakultasService->delete($request,$id);
                    $deleteSukses = $deleteSukses + 1;
                }
            }
            if ($deleteGagal > 0) {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => '' . $deleteSukses . ' Data Unit Fakultas Berhasil Di Hapus dan ' . $deleteGagal . ' Data Unit Fakultas Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => '' . $deleteGagal . ' Data Unit Fakultas Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                }
            } else {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Data Unit Fakultas Berhasil Di Hapus'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Tidak Ada Data Unit Fakultas Yang Di Hapus'
                    ]);
                }
            }

        }
    }

    public function getDataUnitFakultasNama(Request $request, $type = null)
    {

        if (is_null($type)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Terjadi Kesalahan Dalam Pengambilan Data , Silakan Coba Kembali'
            ]);
        }


        if (!$request->ajax()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Mohon Maaf Request Error !'
            ]);
        }

        $dataUnitFakultas = $this->unitFakultasService->byNama($request, $type);

        if ($dataUnitFakultas->isEmpty()) {
            return response()->json([
                'status' => 'error',
                'message' => 'Data Unit Fakultas Tidak Tersedia'
            ]);
        }

        return response()->json([
            'status' => 'success',
            'html' => $dataUnitFakultas
        ]);

    }
}
