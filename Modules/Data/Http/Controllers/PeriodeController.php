<?php

namespace Modules\Data\Http\Controllers;

use App\Helpers\DatatableCustom;
use App\Services\PeriodeServices;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Modules\Data\Entities\Periode;
use Modules\Data\Http\Requests\PeriodeValidasi;

class PeriodeController extends Controller
{
    public function __construct(PeriodeServices $periodeServices, Periode $periode)
    {
        $this->periodeService  = $periodeServices;
        $this->periode  = $periode;
    }

    public function index(Request $request)
    {
        if (!Gate::check('periode')) {
            return abort(403);
        }

         if (!$request->ajax()) {
            return view('data::periode.index');
         } else {
             $dataPeriode = Periode::query();
             return DatatableCustom::initDatatable($dataPeriode,$request);
         }
    }

    public function create()
    {
        if (!Gate::check('tambah periode')) {
            return abort(403);
        }

        $dataPeriode = $this->periode;
        $createMode = true;
        return view('data::periode.add', compact('dataPeriode','createMode'));
    }

    public function store(PeriodeValidasi $request)
    {
        if (!Gate::check('tambah periode')) {
            return abort(403);
        }

        $dataPeriode = $this->periodeService->create($request);

        if (!is_null($dataPeriode)) {
            $message = "Berhasil Menambah Data Periode";
            if ($request->actionsave == "save") {
                return Redirect::to('data/periode')->with("message", $message);
            } else {
                return Redirect::to('data/periode/create')->with("message", $message);
            }
        }

        return Redirect::to('data/periode')->with("messageerror", "Gagal Menambah Data Periode");
    }

    public function show(Request $request, $id)
    {
        if (!Gate::check('detail periode')) {
            return abort(403);
        }

        if (!$request->ajax()) {
            return abort(403);
        }

        $dataPeriode = $this->periodeService->show($request,$id);
        if (is_null($dataPeriode)) {
            return Redirect::to('data/periode')->with("messageerror", "Data Periode Tidak Tersedia");
        }

        return view('data::periode.detail', compact('dataPeriode'));
    }

    public function edit(Request $request,$id)
    {
        if (!Gate::check('edit periode')) {
            return abort(403);
        }

        $dataPeriode = $this->periodeService->show($request,$id);
        if (is_null($dataPeriode)) {
            return Redirect::to('data/periode')->with("messageerror", "Data Periode Tidak Tersedia");
        }

        $createMode = false;

        return view('data::periode.add', compact('dataPeriode','createMode'));
    }

    public function update(PeriodeValidasi $request, $id)
    {
        if (!Gate::check('edit periode')) {
            return abort(403);
        }

        $dataPeriode = $this->periodeService->show($request,$id);
        if (is_null($dataPeriode)) {
            return Redirect::to('data/periode')->with("messageerror", "Data Periode Tidak Tersedia");
        }

        $updatDataPeriode = $this->periodeService->update($request,$id);

        return Redirect::to('data/periode')->with("message", "Data Periode Berhasil Di Perbaharui");
    }

    public function deleteData(Request $request)
    {
        if (!Gate::check('hapus periode')) {
            return abort(403);
        }

        $idarray = $request->id;
        $deleteGagal = 0;
        $deleteSukses = 0;
        if ($request->ajax()) {
            if (!is_null($idarray)) {
                for ($i = 0; $i < count($idarray); $i++) {
                    $id = $request->id[$i];
                    $this->periodeService->delete($request,$id);
                    $deleteSukses = $deleteSukses + 1;
                }
            }
            if ($deleteGagal > 0) {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => '' . $deleteSukses . ' Data Periode Berhasil Di Hapus dan ' . $deleteGagal . ' Data Periode Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => '' . $deleteGagal . ' Data Periode Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                }
            } else {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Data Periode Berhasil Di Hapus'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Tidak Ada Data Periode Yang Di Hapus'
                    ]);
                }
            }

        }
    }
}
