<?php

namespace Modules\Data\Http\Controllers;

use App\Helpers\DatatableCustom;
use App\Services\FormulirServices;
use App\Services\PeriodeServices;
use App\Services\UnitFakultasServices;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Modules\Data\Entities\FormulirDetail;
use Modules\Data\Entities\MFormulir;
use Modules\Data\Entities\MType;
use Modules\Data\Entities\MTypeDetail;
use Modules\Data\Entities\UnitFakultas;
use Modules\Data\Http\Requests\UnitFakultasValidasi;

class FormulirFormController extends Controller
{

    public function __construct(PeriodeServices $periodeServices, FormulirServices $formulirServices)
    {
        $this->periodeService  = $periodeServices;
        $this->formulirService  = $formulirServices;
    }

    public function index(Request $request)
    {

        return view('data::formulir-form.index');

    }

    public function create(Request $request)
    {
        $createMode=true;
        $dataPeriode = $this->periodeService->get($request);
        $dataType = MType::all();
        return view('data::formulir-form.add', compact('createMode','dataPeriode','dataType'));

    }

    public function ajaxRequestPost(Request $request)
    {
       dd($request->all());
    }

    public function ajaxRequestPostMFormulir(Request $request)
    {
       try {
            $alias = '';
            $words = explode(" ", $request->nama_formulir);
            $acronym = "";
            foreach ($words as $w) {
                $acronym .= $w[0];
            }
            $alias = $acronym;
            $periode = $request->periode;
            $nama_formulir = $request->nama_formulir;
            $data = [];
            $data['name'] = $request->nama_formulir;
            $data['table_name'] = str_replace(' ','_', strtolower($nama_formulir));
            $data['alias'] = strtoupper($alias);
            $data['type'] = "formulir_form";
            $data['is_success'] = "0";
            $data['created_by'] = Auth::user()->id;
            $data['updated_by'] = Auth::user()->id;
            $dataFormulir = MFormulir::create($data);
            // $dataFormulir->m_periode()->attach($periode);
            return response()->json(['id' =>$dataFormulir->id, 'success' => true, 'message' => 'Data berhasil disimpan']);
        } catch (\Exception $e) {
            return response()->json(['error' => false, 'message' => 'Data gagal disimpan']);
        }
    }

    public function ajaxRequestDeleteMFormulir(Request $request)
    {
        try {
            $id_formulir = $request->id_formulir;
            $dataFormulir = MFormulir::find($id_formulir);
            $dataFormulir->forceDelete($id_formulir);
            return response()->json(['success' => true, 'message' => 'Data formulir telah dihapus']);
        } catch (\Exception $e) {
            return response()->json(['error' => false, 'message' => 'Data gagal dihapus']);
        }
    }

    public function ajaxRequestUpdateMFormulir(Request $request)
    {
        try {
            $id_formulir = $request->id_formulir;
            $data['name'] = $request->nama_formulir;
            $data['updated_by'] = Auth::user()->id;
            $dataFormulir = MFormulir::whereId($id_formulir)->update($data);
            return response()->json(['success' => true, 'message' => 'Data formulir berhasil diupdate']);
        } catch (\Exception $e) {
            return response()->json(['error' => false, 'message' => 'Data gagal diupdate']);
        }
    }

    public function ajaxRequestPostTypeMFormulir(Request $request)
    {
        try {
            $data = [];
            $data['m_type_id'] = null;
            $data['pertanyaan'] = null;
            $data['deskripsi'] = null;
            $data['status'] = null;
            $data['created_by'] = Auth::user()->id;
            $data['updated_by'] = Auth::user()->id;
            $dataMTypeDetail = MTypeDetail::create($data);
            return response()->json(['success' => true, 'message' => 'Data formulir berhasil disimpan']);
        } catch (\Exception $e) {
            return response()->json(['error' => false, 'message' => 'Data gagal disimpan']);
        }
    }

    public function store(Request $request)
    {
    //   dd("Ada");
    }

    public function show(Request $request, $id)
    {
        $request->all();
    }

    public function edit(Request $request,$id)
    {
        // dd("Ada");
    }

    public function update(UnitFakultasValidasi $request, $id)
    {
        // dd("Ada");
    }

    public function deleteData(Request $request)
    {
        // dd("Ada");
    }

    public function getDataUnitFakultasNama(Request $request, $type = null)
    {
        // dd("Ada");

    }

    public function saveData(Request $request)
    {
        // dd("ada");
    }
}
