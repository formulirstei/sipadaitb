<?php

namespace Modules\Data\Http\Requests;
use Modules\Data\Http\Requests\Request;
class UnitFakultasValidasi extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "type" => "required",
            "name" => "required|max:191",
        ];

    }

    public function messages()
    {

        return [
            "type.required" => "Type Belum Terisi",
            "name.required" => "Nama Belum Terisi"
            ];
    }

}


