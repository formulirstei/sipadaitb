<?php

namespace Modules\Data\Http\Requests;
use Modules\Data\Http\Requests\Request;

class PeriodeValidasi extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "semester" => "required",
            "tahun" => "required",
            "tgl_mulai" => "required",
            "tgl_selesai" => "required",
        ];
    }

    public function messages()
    {
        return [
            "semester.required" => "Semester Belum Terisi",
            "tahun.required" => "Tahun Belum Terisi",
            "tgl_mulai.required" => "Tanggal Mulai Belum Terisi",
            "tgl_selesai.required" => "Tanggal Selesai Belum Terisi"
            ];
    }

}


