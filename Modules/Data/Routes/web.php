<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;
use Modules\Data\Http\Controllers\FormulirFormController;

Route::post('/data/formulir-form/create/post-m-formulir', [FormulirFormController::class, 'ajaxRequestPostMFormulir'])->name('ajaxRequestPostMFormulir');
Route::delete('/data/formulir-form/create/delete-m-formulir', [FormulirFormController::class, 'ajaxRequestDeleteMFormulir'])->name('ajaxRequestDeleteMFormulir');
Route::put('/data/formulir-form/create/update-m-formulir', [FormulirFormController::class, 'ajaxRequestUpdateMFormulir'])->name('ajaxRequestUpdateMFormulir');
Route::post('/data/formulir-form/create/post-type-m-formulir', [FormulirFormController::class, 'ajaxRequestPostTypeMFormulir'])->name('ajaxRequestPostTypeMFormulir');

Route::middleware(['web','auth'])->prefix('data')->group(function () {
    Route::resource('/periode', 'PeriodeController');
    Route::post('/deleteDataPeriode', 'PeriodeController@deleteData');
    Route::resource('/unit-fakultas', 'UnitFakultasController');
    Route::post('/deleteDataUnitFakultas', 'UnitFakultasController@deleteData');
    Route::get('unitFakultasNama/{type?}', 'UnitFakultasController@getDataUnitFakultasNama');
    Route::resource('/formulir', 'FormulirController');
    Route::post('/deleteDataFormulir', 'FormulirController@deleteData');
    Route::get('dataFormulir/{table_name?}', 'FormulirController@getDataFormulir');
    //header formulir
    Route::get('formulir/edit-header/{id}', 'FormulirController@getHeader');
    Route::get('formulir-edit-header/{id?}', 'FormulirController@getEditHeader');
    Route::get('formulir-header-edit/{id?}/{f_detail_id?}', 'FormulirController@EditHeader');
    Route::post('formulir-edit-header/{id?}/create', 'FormulirController@createHeader');
    Route::put('formulir-edit-header/{id?}/{f_detail_id?}/update', 'FormulirController@updateHeader');
    Route::post('formulir-edit-header/{id?}/{f_detail_id?}/delete', 'FormulirController@deleteDataHeader');
    Route::get('getPeriode/{id?}', 'FormulirController@getDataPeriode');
    Route::get('formulir/{id}/mapping', 'FormulirController@mapping');
    Route::post('formulir-mapping/{id}', 'FormulirController@createMapping');
    Route::post('formulir-mapping-form/{id}', 'FormulirController@createMappingForm');
    Route::resource('/formulir-form', 'FormulirFormController');
    Route::post('/formulir-form/save-data', 'FormulirFormController@ajaxRequestPost');

});
