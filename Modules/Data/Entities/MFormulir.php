<?php

namespace Modules\Data\Entities;

use App\Helpers\LayananHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Privilege\Entities\Users;

class MFormulir extends Model
{
    use SoftDeletes;

    protected $table = 'm_formulirs';
    protected $primaryKey = 'id';
    protected $fillable = ['name','table_name','alias','status','type','turunan_formulir','is_success','created_by','updated_by','deleted_by','deleted_at'];

    public function get_isian_formulir()
    {
        return $this->hasMany(IsianFormulirForm::class, 'm_formulirs_id', 'id');
    }

    public function m_periode()
    {
        return $this->morphedByMany(Periode::class, 'm_formulirable');
    }

    public function m_unitfakultas()
    {
        return $this->morphedByMany(UnitFakultas::class, 'm_formulirable');
    }

    public function m_formulir_detail()
    {
        return $this->morphedByMany(FormulirDetail::class, 'm_formulirable');
    }

    public function get_created_by()
    {
        return $this->hasOne(Users::class, 'id', 'created_by')->withTrashed();
    }

    public function get_updated_by()
    {
        return $this->hasOne(Users::class, 'id', 'updated_by')->withTrashed();
    }

    public function get_deleted_by()
    {
        return $this->hasOne(Users::class, 'id', 'deleted_by')->withTrashed();
    }

    public function getCreatedAt($type){
        $tanggal="";
        if ($this->created_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->created_at, $type);
        }
        return $tanggal;
    }


    public function getUpdatedAt($type){
        $tanggal="";
        if ($this->updated_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->updated_at, $type);
        }
        return $tanggal;
    }


    public function getDeletedAt($type){
        $tanggal="";
        if ($this->deleted_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->deleted_at, $type);
        }
        return $tanggal;
    }


}
