<?php

namespace Modules\Data\Entities;

use App\Helpers\LayananHelper;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Privilege\Entities\Users;

class UnitFakultas extends Model
{
    use SoftDeletes;

    protected $table = 'm_unitfakultas';
    protected $primaryKey = 'id';
    protected $fillable = ['shortname', 'name','type', 'created_by','updated_by','deleted_by','deleted_at'];

    public function m_formulir()
    {
        return $this->morphToMany(MFormulir::class, 'm_formulirable');
    }

    public function get_user_detail()
    {
        return $this->hasOne(User::class, 'm_unitfakultas_id', 'id');
    }


    public function get_created_by()
    {
        return $this->hasOne(Users::class, 'id', 'created_by')->withTrashed();
    }

    public function get_updated_by()
    {
        return $this->hasOne(Users::class, 'id', 'updated_by')->withTrashed();
    }

    public function get_deleted_by()
    {
        return $this->hasOne(Users::class, 'id', 'deleted_by')->withTrashed();
    }

    public function getCreatedAt($type){
        $tanggal="";
        if ($this->created_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->created_at, $type);
        }
        return $tanggal;
    }


    public function getUpdatedAt($type){
        $tanggal="";
        if ($this->updated_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->updated_at, $type);
        }
        return $tanggal;
    }


    public function getDeletedAt($type){
        $tanggal="";
        if ($this->deleted_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->deleted_at, $type);
        }
        return $tanggal;
    }


}
