<?php

namespace Modules\Data\Entities;

use App\Helpers\LayananHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Modules\Privilege\Entities\Users;

class IsianFormulirForm extends Model
{
    use SoftDeletes;

    protected $table = 'f_isian_form';
    protected $primaryKey = 'id';
    protected $fillable = ['m_formulirs_id','m_type_detail_id','value','created_by','updated_by','deleted_by'];

    public function get_formulir()
    {
        return $this->belongsTo(MFormulir::class, 'm_formulirs_id', 'id');
    }

    public function get_type_detail()
    {
        return $this->belongsTo(MTypeDetail::class, 'm_type_detail_id', 'id');
    }

    public function get_created_by()
    {
        return $this->hasOne(Users::class, 'id', 'created_by')->withTrashed();
    }

    public function get_updated_by()
    {
        return $this->hasOne(Users::class, 'id', 'updated_by')->withTrashed();
    }

    public function get_deleted_by()
    {
        return $this->hasOne(Users::class, 'id', 'deleted_by')->withTrashed();
    }

    public function getCreatedAt($type){
        $tanggal="";
        if ($this->created_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->created_at, $type);
        }
        return $tanggal;
    }


    public function getUpdatedAt($type){
        $tanggal="";
        if ($this->updated_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->updated_at, $type);
        }
        return $tanggal;
    }


    public function getDeletedAt($type){
        $tanggal="";
        if ($this->deleted_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->deleted_at, $type);
        }
        return $tanggal;
    }


}
