<?php

namespace Modules\Backend\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use App\Helpers\DatatableCustom;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class BackendController extends Controller
{
    public function index()
    {
        $dataUser = User::where('id', Auth::user()->id)->first();
        return view('backend::index', compact('dataUser'));
    }

    public function getChangePrivilege(Request $request)
    {
        $id = $request->id;
        $permission = unserialize(Auth::user()->permission);
        if (empty($permission)) {
            return Redirect::to('backend')->with("messageerror", "Mohon Maaf Privilege Tidak Tersedia , Silahkan Logout dan Login Kembali");
        }

        $collectPermission = collect($permission);

        if (!$collectPermission->contains('id', $id)) {
            return Redirect::to('backend')->with("messageerror", "Mohon Maaf Privilege Tidak Tersedia");
        }

        if (!empty(Auth::user()->roles[0])) {
            if (Auth::user()->roles[0]->id == $id) {
                return Redirect::to('backend')->with("message", "Tidak Ada Perubahan Privilege");
            }
        }

        $cekRoles = Role::whereId($id)->first();
        if (is_null($cekRoles)) {
            return Redirect::to('backend')->with("messageerror", "Privilege Tidak Tersedia");
        }

        Auth::user()->syncRoles($cekRoles->name);
        return Redirect::to('/backend')->with("message", "Privilege Berhasil Di Ubah");

    }

    public function error(Request $request)
    {
        if (Session::has("message")) {
            $message = Session::has("message");
            return view('backend::error.index', compact('message'));
        } else {
            return Redirect::to('/backend');
        }
    }

    public function create()
    {
        return view('backend::create');
    }

    public function store(Request $request)
    {
        //
    }

    public function show($id)
    {
        return view('backend::show');
    }

    public function edit($id)
    {
        return view('backend::edit');
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }
}
