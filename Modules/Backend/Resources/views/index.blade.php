@extends('layouts.template_backend',['title'=>'Dashboard'])
@section('sidebar')
    @include('page.page_header',['title'=>'Dashboard','breadcumb'=>array('Dashboard;#') , 'delete_multiple'=>true , 'back'=>false])
@endsection
@push('styles')

@endpush
@section('content')
    @include('errors.validasi')

        <div class="row">
            <div class="col-md-12">
                <div class="kt-portlet" data-ktportlet="true">
                    <div class="kt-portlet__head bg-info">
                        <div class="kt-portlet__head-label">
                            <h3 class="kt-portlet__head-title text-white">
                                Role :  @foreach ($dataUser->roles as $role) {!! $role->name !!} @endforeach
                            </h3>
                        </div>
                        <div class="kt-portlet__head-toolbar">
                            <div class="kt-portlet__head-group">
                                <a href="#" data-ktportlet-tool="toggle" class="btn btn-sm btn-icon btn-clean btn-icon-md"></a>
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__body">

                        <div class="kt-widget kt-widget--user-profile-3">
                            <div class="kt-widget__top">
                                <div class="kt-widget__media kt-hidden-">
                                    <img src="{{URL('assets/images/avatar.png')}}" alt="image">
                                </div>
                                <div class="kt-widget__pic kt-widget__pic--danger kt-font-danger kt-font-boldest kt-font-light kt-hidden">
                                    JM
                                </div>
                                <div class="kt-widget__content">
                                    <div class="kt-widget__head">
                                        <a href="#" class="kt-widget__username">
                                            {{Auth::user()->name}}
                                        </a>
                                    </div>
                                    <div class="kt-widget__subhead">
                                        <div>
                                            Email : {{Auth::user()->email}}
                                        </div>
                                        <div>
                                            Username : {{Auth::user()->username}}
                                        </div>
                                    </div>
                                    <div class="row mt-4">
                                        <div class="col-md-4">
                                            Unit Atau Fakultas   : {{isset($dataUser->get_user_detail->get_unit_fakultas->name) ? $dataUser->get_user_detail->get_unit_fakultas->name : '-' }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <div class="row">

            <div class="col-lg-3">
                <div class="kt-portlet kt-iconbox">
                    <div class="kt-portlet__body">
                        <a href="{{ URL('kenaikan-pangkat') }}" class="kt-iconbox__body">
                            <div class="kt-iconbox__desc">
                                <h3 class="kt-iconbox__title">
                                    Data
                                </h3>
                                <div class="kt-iconbox__content">
                                    Jumlah User
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="kt-portlet kt-iconbox">
                    <div class="kt-portlet__body">
                        <a href="#" class="kt-iconbox__body">
                            <div class="kt-iconbox__desc">
                                <h3 class="kt-iconbox__title">
                                     Data
                                </h3>
                                <div class="kt-iconbox__content">
                                    Jumlah Fakultas
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="kt-portlet kt-iconbox">
                    <div class="kt-portlet__body">
                        <a href="#" class="kt-iconbox__body">
                            <div class="kt-iconbox__desc">
                                <h3 class="kt-iconbox__title">
                                     Data
                                </h3>
                                <div class="kt-iconbox__content">
                                    Jumlah Unit
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

            <div class="col-lg-3">
                <div class="kt-portlet kt-iconbox">
                    <div class="kt-portlet__body">
                        <a href="#" class="kt-iconbox__body">
                            <div class="kt-iconbox__desc">
                                <h3 class="kt-iconbox__title">
                                     Data
                                </h3>
                                <div class="kt-iconbox__content">
                                     Jumlah
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>

        </div>

@endsection

@push('scripts')

@endpush

