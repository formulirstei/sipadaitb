<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::middleware(['web','auth'])->prefix('backend')->group(function () {
    Route::get('/', 'BackendController@index');
    Route::get('error', 'BackendController@error');
});

Route::middleware(['web','auth'])->prefix('backend')->group(function () {
    Route::get('changePrivilege', 'BackendController@getChangePrivilege');
});
