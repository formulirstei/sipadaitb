<?php
    $link_limit = 12;
?>
<div class="text">
    <h6 style="font-size: 12px; font-weight:normal">
        {!! __('Showing') !!}
        <span>{{ $paginator->firstItem() }}</span>
        {!! __('to') !!}
        <span>{{ $paginator->lastItem() }}</span>
        {!! __('of') !!}
        <span>{{ $paginator->total() }}</span>
        {!! __('results') !!}
    </h6>
</div>
<nav aria-label="Page navigation example">
    <ul class="pagination">
        <li class="page-item {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
            <a class="page-link" style="letter-spacing: 1px; font-weight:normal" href="{{$paginator->url(1)}}">page 1</a>
        </li>
        <li class="page-item {{ ($paginator->currentPage() == 1) ? ' disabled' : '' }}">
            <a class="page-link" href="{{$paginator->previousPageUrl()}}" aria-label="Previous">
                <span aria-hidden="true">&laquo;</span>
            </a>
        </li>
        @for ($i = 1; $i <= $paginator->lastPage(); $i++)
            <?php
                $half_total_links = floor($link_limit / 2);
                $from = $paginator->currentPage() - $half_total_links;
                $to = $paginator->currentPage() + $half_total_links;
                if ($paginator->currentPage() < $half_total_links) {
                    $to += $half_total_links - $paginator->currentPage();
                }
                if ($paginator->lastPage() - $paginator->currentPage() < $half_total_links) {
                    $from -= $half_total_links - ($paginator->lastPage() - $paginator->currentPage()) - 1;
                }
            ?>
            @if ($from < $i && $i < $to)
                <li class="page-item {{ ($paginator->currentPage() == $i) ? ' active' : '' }}">
                    <a class="page-link" href="{{ $paginator->url($i) }}">{{ $i }}</a>
                </li>
            @endif
        @endfor
        <li class="page-item">
            <a class="page-link {{ ($paginator->currentPage() == $paginator->lastPage()) ? ' disabled' : '' }}" href="{{ $paginator->url($paginator->lastPage()) }}" aria-label="Next">
                <span aria-hidden="true">&raquo;</span>
            </a>
        </li>
    </ul>
</nav>
