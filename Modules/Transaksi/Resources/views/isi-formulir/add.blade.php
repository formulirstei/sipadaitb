@extends('layouts.template_backend',['Tambah Isian Formulir'])
@section('sidebar')
    @include('page.page_header',['title'=>'Tambah isian formulir','breadcumb'=>array('Data;'.URL('transaksi/isi-formulir'), 'Tambah data isian formulir;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endsection
@section('content')
    @include('errors.validasi')

    <div class="mt-5">
        <div class="text-center">
            <h4 class="">{{$dataFormulir->name}}</h4>
            <p class="lead"> {{isset($dataFormulir->m_unitfakultas()->first()->type) ? $dataFormulir->m_unitfakultas()->first()->type : 'Master'}}
                <span class="text-primary">
                    {{isset($dataFormulir->m_unitfakultas()->first()->name) ? $dataFormulir->m_unitfakultas()->first()->name : 'Master'}}
                </span>
                . Periode
                <span class="text-primary">
                    Tahun {{date('Y', strtotime($dataFormulir->m_periode()->first()->tahun))}},
                    Semester {{$dataFormulir->m_periode()->first()->semester }}
                </span>
            </p>
        </div>

        <div class="custom-table-outer">
            <form id="formSubmit"
                action="{{URL('transaksi/isi-formulir/'.$dataFormulir->id.'/store') }}"
                method="post">
                @csrf
                @method('PUT')
                <table class="table custom-table-bordered" style="background-color: #fff; border-right: 2px solid #dbdfea;" id="myTable">
                    <thead>
                        <tr>
                            <th class="custom-header-number">No</th>
                            @foreach ($dataFormulir->m_formulir_detail as $header)
                                <th class="custom-header">{{str_replace('_', ' ', $header->name)}}</th>
                            @endforeach
                            <th class="custom-header-tools">Tools</th>
                        </tr>
                    </thead>
                    <tbody class="fieldGroup" id="dom-group">
                        <div>
                            <td class="custom-number count"></td>
                            @foreach ($dataFormulir->m_formulir_detail as $header)
                                <td class="custom-table custom-table">
                                    @if ($header->type == "integer")
                                        <input type="number" autocomplete="off" class="custom-form-control" onkeydown="return event.keyCode !== 69" name="{{$header->name}}[]" placeholder="input {{ str_replace('_', ' ', $header->name) }}" spellcheck="false">
                                    @elseif ($header->type == "string")
                                        <input type="text" autocomplete="off" class="custom-form-control" name="{{$header->name}}[]" placeholder="input {{ str_replace('_', ' ', $header->name) }}" spellcheck="false">
                                    @else
                                        <input type="date" autocomplete="off" class="custom-form-control" name="{{$header->name}}[]" placeholder="input {{ str_replace('_', ' ', $header->name) }}" spellcheck="false">
                                    @endif
                                </td>
                            @endforeach
                        </div>
                    </tbody>
                </table>
            </form>
        </div>
        <div class="btnRemoveCopy" style="display: none">
            <a id="remove" class="btn btn-danger btn-icon btn-sm">
                <i class="la la-trash-o"></i>
            </a>
        </div>

        <div class="mt-3">
            <div class="d-flex">
                <div style="width: 80px; margin-right: 10px">
                    <input type="number" id="loopRow" class="form-control" value="1">
                </div>
                <div>
                    <a class="btn btn-success addMore text-white">
                        <em class="icon ni ni-plus mr-2"></em>
                        Add row
                    </a>
                </div>
            </div>
        </div>
        <div class="card-body text-right">
            <button type="submit" form="formSubmit" class="btn btn-primary">Simpan</button>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        // $("body").on("click", ".validate_number", function (e) {
        //     var elements = document.getElementsByClassName("validate_number");
        //     for (var i = 0; i < elements.length; i++) {
        //         elements[i].addEventListener("keypress", function (evt) {
        //             if (evt.which != 8 && evt.which != 0 && evt.which < 48 || evt.which > 57)
        //             {
        //                 evt.preventDefault();
        //             }
        //         });
        //     }
        // });

        $(document).ready(function () {
        $(".addMore").click(function () {
            var lenght = document.getElementById("loopRow").value;
            for (let i = 1; i <= lenght; i++) {
                var fieldHTML = '<tbody class="fieldGroup" id="dom-group">' + $(".fieldGroup").html() + '</table>';
                $('body').find('.fieldGroup:last').after(fieldHTML);

                var btnRemove = '<td class="tools">' + $(".btnRemoveCopy").html() + '</td>';
                $('body').find('.custom-table:last').after(btnRemove);
            }
        });
        $("body").on("click", "#remove", function () {
            let currow = $(this).closest('.fieldGroup');
            $(this).parents(".fieldGroup").remove();
        });
    });

    </script>
@endpush
