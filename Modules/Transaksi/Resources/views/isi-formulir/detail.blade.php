@extends('layouts.template_backend',['Detail Isian Formulir'])
@section('sidebar')
    @include('page.page_header',['title'=>'Detail isian formulir','breadcumb'=>array('Data;'.URL('transaksi/isi-formulir'), 'Detail data isian formulir;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endsection
@section('content')
    @include('errors.validasi')

    <div class="mt-5">
        <div class="text-center">
            <h4 class="">{{$dataFormulir->name}}</h4>
            <p class="lead"> {{isset($dataFormulir->m_unitfakultas()->first()->type) ? $dataFormulir->m_unitfakultas()->first()->type : 'Master'}}
                <span class="text-primary">
                    {{isset($dataFormulir->m_unitfakultas()->first()->name) ? $dataFormulir->m_unitfakultas()->first()->name : 'Master'}}
                </span>
                . Periode
                <span class="text-primary">
                    Tahun {{date('Y', strtotime($dataFormulir->m_periode()->first()->tahun))}},
                    Semester {{$dataFormulir->m_periode()->first()->semester }}
                </span>
            </p>
        </div>



        <div class="row">
            <div class="col-12 col-md-12 col-lg-6 col-xl-6 mt-1">
                <div class="mb-4">
                    <button href="#" class="btn btn-primary btn-sm mr-1" disabled>
                        <em class="icon ni ni-file"></em>
                        <span>Export</span>
                    </button>
                </div>
            </div>
            <div class="col-12 col-md-12 col-lg-6 col-xl-6 text-right mt-2">
                <div class="mb-4">
                    <a href="{{URL('transaksi/isi-formulir/'.$dataFormulir->id.'/create')}}" class="btn btn-outline-primary btn-dim btn-sm mr-1">
                        <span>Tambah Data</span>
                    </a>
                    <a href="{{URL('transaksi/isi-formulir/'.$dataFormulir->id.'/edit')}}" class="btn btn-outline-success btn-sm mr-1">
                        <span>Edit Data</span>
                    </a>
                    <a href="{{URL('transaksi/isi-formulir/')}}" class="btn btn-outline-warning btn-sm mr-1">
                        <span>Kembali</span>
                    </a>

                </div>
            </div>
        </div>


        <div class="custom-table-outer">
            <table class="table custom-table-bordered" style="background-color: #fff; border-right: 2px solid #dbdfea;" id="myTable">
                <thead>
                    <tr>
                        <th class="custom-header-number">No</th>
                        @foreach ($dataFormulir->m_formulir_detail as $header)
                            <th class="custom-header">{{str_replace('_', ' ', $header->name)}}</th>
                        @endforeach
                    </tr>
                </thead>
                @foreach ($dataIsianFormulirs as $dataIsianFormulir)
                <tbody class="fieldGroup" id="dom-group">
                    <div>
                        <td class="custom-number count"></td>
                        @foreach ($dataFormulir->m_formulir_detail as $header)
                            @php
                                $name = strtolower($header->name);
                            @endphp
                            <td class="custom-table-detail">{{isset($dataIsianFormulir->$name) ? $dataIsianFormulir->$name : '-' }}</td>
                        @endforeach
                    </div>
                </tbody>
                @endforeach
            </table>
        </div>

        <div class="col-span-12 lg:col-span-12 xxl:col-span-12 mt-5 text-center">
            <div class="btn text-center rounded-md py-3"> {{$dataIsianFormulirs->links('transaksi::customPagination.pagination')}}</div>
        </div>


    </div>

@endsection
