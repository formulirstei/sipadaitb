@extends('layouts.template_backend',['Isian Formulir'])
@section('sidebar')
    @include('page.page_header',['title'=>'Isian formulir','breadcumb'=>array('Data;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>false])
@endsection
@section('content')
    @include('errors.validasi')

    <div class="row">
        <form action="{{URL('/transaksi/isi-formulir/search')}}" method="post">
            @csrf
            @method('POST')
            <div class="d-flex col">
                <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                    <input type="text" name="key_search" class="form-control" placeholder="Cari nama formulir disini" value="{{isset($key) ? $key : '' }}" >
                    <span class="kt-input-icon__icon kt-input-icon__icon--right">
                        <span>
                            <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                    <rect x="0" y="0" width="24" height="24"></rect>
                                    <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3"></path>
                                    <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero"></path>
                                </g>
                            </svg>
                        </span>
                    </span>
                </div>
                <div class="ml-3">
                    <button type="submit" class="btn btn-outline-success">Cari</button>
                </div>
            </div>
        </form>
    </div>

    <div class="row mt-5">
        @foreach ($dataFormulirs as $dataFormulir)
            <div class="col-12 col-sm-6 col-md-6 col-lg-4 col-xl-4">
                <div class="kt-portlet">
                    <div class="kt-portlet__head">
                        <div class="kt-portlet__head-label">
                            <div class="badge bg-primary text-wrap text-white mr-3 text-center" style="min-width: 35px;">
                                <h2 class="kt-portlet__head-title text-white">
                                    {{$loop->iteration}}
                                </h2>
                            </div>
                            <div>
                                <h3 class="kt-portlet__head-title">
                                    {{isset($dataFormulir->m_unitfakultas()->first()->name) ? $dataFormulir->m_unitfakultas()->first()->name : 'Master' }}
                                </h3>
                                <div>@if ($dataFormulir->type == 'formulir_table') Formulir Isian Table @else Formulir Isian Form @endif</div>
                            </div>
                        </div>
                    </div>

                    <div class="kt-portlet__body">
                        <div data-toggle="kt-tooltip" data-placement="top" title="Nama Formulir">
                            <h5 class="kt-portlet__body-title"> {{$dataFormulir->name}} </h5>
                        </div>
                        <div>
                            <p>Semester {{$dataFormulir->m_periode()->first()->semester }} - {{date('Y', strtotime($dataFormulir->m_periode()->first()->tahun))}}</p>
                        </div>
                        <div class="flex">
                            <div data-toggle="kt-tooltip" data-placement="top" title="Tanggal Mulai" class="badge mr-2" style="background: #E8EAF6">{{date('d F Y', strtotime($dataFormulir->m_periode()->first()->tgl_mulai))}}</div>
                            <div data-toggle="kt-tooltip" data-placement="top" title="Tanggal Akhir" class="badge" style="background: #FFEBEE">{{date('d F Y', strtotime($dataFormulir->m_periode()->first()->tgl_selesai))}}</div>
                        </div>
                        <div class="flex mt-3">
                            <div data-toggle="kt-tooltip" data-placement="top" title="Type" class="badge mr-2" style="background: #EFEBE9">{{isset($dataFormulir->m_unitfakultas()->first()->type) ? $dataFormulir->m_unitfakultas()->first()->type : 'Master'}}</div>
                            <div data-toggle="kt-tooltip" data-placement="top" title="Untuk" class="badge mr-2" style="background: #E8F5E9">{{isset($dataFormulir->m_unitfakultas()->first()->name) ? $dataFormulir->m_unitfakultas()->first()->name : 'Master'}}</div>
                            <div data-toggle="kt-tooltip" data-placement="top" title="Jumlah Data" class="badge mr-2" style="background: #FFF8E1">
                                @php
                                    $table_name     = $dataFormulir->table_name;
                                    $countData      = Illuminate\Support\Facades\DB::select('select * from '.$table_name.'')
                                @endphp
                                {{count($countData)}} Data
                            </div>
                        </div>
                    </div>
                    <div class="kt-portlet__foot text-center">
                        <div class="btn-group" role="group" aria-label="Button group with nested dropdown">
                            <a @if ($dataFormulir->type == 'formulir_table') href="{{URL('transaksi/isi-formulir/'.$dataFormulir->id.'/create')}}" @else href="{{URL('transaksi/isi-formulir-form/'.$dataFormulir->id.'/create')}}" @endif type="button" class="btn btn-primary btn-sm">Isi Formulir</a>
                            <div class="btn-group" role="group">
                                <button id="btnGroupDrop1" type="button" class="btn btn-primary btn-sm dropdown-toggle" data-toggle="dropdown" aria-expanded="false"></button>
                                <ul class="dropdown-menu" aria-labelledby="btnGroupDrop1">
                                    <li><a class="dropdown-item" @if ($dataFormulir->type == 'formulir_table') href="{{URL('transaksi/isi-formulir/'.$dataFormulir->id.'/edit')}}" @else href="{{URL('transaksi/isi-formulir-form/'.$dataFormulir->id.'/edit')}}" @endif >Edit Formulir</a></li>
                                    <li><a class="dropdown-item" @if ($dataFormulir->type == 'formulir_table') href="{{URL('transaksi/isi-formulir/'.$dataFormulir->id.'')}}" @else href="{{URL('transaksi/isi-formulir-form/'.$dataFormulir->id.'')}}" @endif >Detail Formulir</a></li>
                                    <li><a class="dropdown-item" href="{{URL('transaksi/history-formulir/'.$dataFormulir->id.'')}}">History Formulir</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    <div class="mt-5 text-center">
        <div class="btn text-center rounded-md py-3"> {{$dataFormulirs->links('transaksi::customPagination.pagination')}}</div>
    </div>
@endsection

