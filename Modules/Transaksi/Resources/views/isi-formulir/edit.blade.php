@extends('layouts.template_backend',['Edit Isian Formulir'])
@section('sidebar')
    @include('page.page_header',['title'=>'Edit isian formulir','breadcumb'=>array('Data;'.URL('transaksi/isi-formulir'), 'Edit data isian formulir;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endsection
@section('content')
    @include('errors.validasi')


    <div class="mt-5">
        <div class="text-center">
            <h4 class="">{{$dataFormulir->name}}</h4>
            <p class="lead"> {{isset($dataFormulir->m_unitfakultas()->first()->type) ? $dataFormulir->m_unitfakultas()->first()->type : 'Master'}}
                <span class="text-primary">
                    {{isset($dataFormulir->m_unitfakultas()->first()->name) ? $dataFormulir->m_unitfakultas()->first()->name : 'Master'}}
                </span>
                . Periode
                <span class="text-primary">
                    Tahun {{date('Y', strtotime($dataFormulir->m_periode()->first()->tahun))}},
                    Semester {{$dataFormulir->m_periode()->first()->semester }}
                </span>
            </p>
        </div>
        <div class="custom-table-outer">
            <table class="table custom-table-bordered" style="background-color: #fff; border-right: 2px solid #dbdfea;" id="myTable">
                <thead>
                    <tr>
                        <th class="custom-header-number">No</th>
                        @foreach ($dataFormulir->m_formulir_detail as $header)
                            <th class="custom-header">{{str_replace('_', ' ', $header->name)}}</th>
                        @endforeach
                        <th class="custom-header-tools">Tools</th>
                    </tr>
                </thead>
                @foreach ($dataIsianFormulirs as $dataIsianFormulir)
                <tbody class="fieldGroup" id="dom-group">
                    <div>
                        <td class="custom-number count"></td>
                        <td style="display: none">{{$dataIsianFormulir->id}}</td>
                        @foreach ($dataFormulir->m_formulir_detail as $header)
                            @php
                                $name = strtolower($header->name);
                            @endphp
                            <td class="custom-table-detail">
                                @if ($header->type == "integer")
                                    <input type="number" autocomplete="off" class="custom-form-control" value="{{isset($dataIsianFormulir->$name) ? $dataIsianFormulir->$name : '-' }}" placeholder="input {{ str_replace('_', ' ', $header->name) }}" spellcheck="false" onkeydown="return event.keyCode !== 69" name="{{$header->name}}[]">
                                @elseif ($header->type == "string")
                                    <input type="text" autocomplete="off" class="custom-form-control" value="{{isset($dataIsianFormulir->$name) ? $dataIsianFormulir->$name : '-' }}" name="{{$header->name}}[]" placeholder="input {{ str_replace('_', ' ', $header->name) }}" spellcheck="false">
                                @else
                                    <input type="date" autocomplete="off" class="custom-form-control" value="{{isset($dataIsianFormulir->$name) ? $dataIsianFormulir->$name : '-' }}" name="{{$header->name}}[]" placeholder="input {{ str_replace('_', ' ', $header->name) }}" spellcheck="false">
                                @endif
                            </td>
                        @endforeach
                        <td class="tools">
                            <a id="remove" class="btn btn-danger btn-icon btn-sm temp-data-delete">
                                <i class="la la-trash-o"></i>
                            </a>
                        </td>
                    </div>
                </tbody>
                @endforeach
            </table>
        </div>
        <div class="card-body text-right">
            <button type="submit" form="formSubmit" class="btn btn-primary btn-simpan-temp">Simpan Perubahan</button>
        </div>
    </div>
@endsection
@push('scripts')
<script>
    var temp_data = [];
    var temp_data_edit = [];
    var temp_data_hapus = [];
    function getDataTables () {
        $.get("edit/get_data_tables",
            function (data, textStatus, jqXHR) {
                i=1
                $.each(data, function (index, data_formulir) {
                    $.each(data_formulir, function (index, value) {
                        temp_data[index] = value;
                    });
                });
            }
        );
    }
    function simpanEditDataTables() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.post("edit/simpan_edit_data_tables", {temp_data_edit:temp_data_edit})
        .done(function() {
            if (temp_data_hapus.length > 0) {
                deleteEditDatatables();
            }
            alert( "success" );
        })
        .fail(function() {
            alert( "error" );
        });
    }

    function deleteEditDatatables() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $.post("edit/delete_edit_data_tables", {temp_data_hapus:temp_data_hapus})
        .done(function() {
            alert( "success" );
        })
        .fail(function() {
            alert( "error" );
        });
    }

    $(document).ready(function () {
        getDataTables();
        $('.fieldGroup').on('click keyup keypress keydown', 'input', function (e) {
            $(this).on('change', function (e) {
                var value = $(this).val();
                var name = $(this).attr('name').replace('[]', '');
                var row = $(this).closest('tr');
                var id = row.find('td:eq(1)').text();
                temp_data_edit[id] = temp_data[id];
                temp_data_edit[id][name] = value
            });
        });

        $('.fieldGroup').on('click', '.temp-data-delete',function (e) {
            var row = $(this).closest('tr');
            var id = row.find('td:eq(1)').text();
            temp_data_hapus[id] = temp_data[id];
            console.log(temp_data_hapus[id])
        });

        $("body").on("click", "#remove", function () {
            let currow = $(this).closest('.fieldGroup');
            $(this).parents(".fieldGroup").remove();

        });

        $("body").on("click", "#removeCopy", function () {
            let currow = $(this).closest('.fieldGroupCopy');
            $(this).parents(".fieldGroupCopy").remove();
            $(this).parents(".fieldGroup").remove();
        });

        $('.btn-simpan-temp').on('click',function (e) {
            if (temp_data_edit.length > 0) {
                simpanEditDataTables();
            } else {
                if (temp_data_hapus.length > 0) {
                    deleteEditDatatables();
                }
            }
        });
    });
</script>
@endpush
