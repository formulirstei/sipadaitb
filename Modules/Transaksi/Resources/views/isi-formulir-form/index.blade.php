@extends('layouts.template_backend',['Tambah Isian Formulir'])
@section('sidebar')
    @include('page.page_header',['title'=>'Tambah isian formulir','breadcumb'=>array('Data;'.URL('transaksi/isi-formulir'), 'Tambah data isian formulir;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endsection
@section('content')
    @include('errors.validasi')

    <div class="mt-5">
        <div class="text-center">
            <h4 class="">{{$dataFormulir->name}}</h4>
            <p class="lead"> {{isset($dataFormulir->m_unitfakultas()->first()->type) ? $dataFormulir->m_unitfakultas()->first()->type : 'Master'}}
                <span class="text-primary">
                    {{isset($dataFormulir->m_unitfakultas()->first()->name) ? $dataFormulir->m_unitfakultas()->first()->name : 'Master'}}
                </span>
                . Periode
                <span class="text-primary">
                    Tahun {{date('Y', strtotime($dataFormulir->m_periode()->first()->tahun))}},
                    Semester {{$dataFormulir->m_periode()->first()->semester }}
                </span>
            </p>
        </div>

        <div class="row" style="display: flex; justify-content: center; align-items: center;">
            <div class="col-12 col-sm-12 col-md-9 col-lg-7 col-xl-7">

                <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                    <div class="kt-portlet mt-2">
                        <div class="kt-portlet__head mt-5 pl-5 pr-5">
                            <div style="width: 100%">
                                <h2> <center>{{ $dataFormulir->name }}</center> </h2>
                            </div>
                        </div>
                        <div class="kt-portlet__body mt-2 pl-5 pr-5">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <h6> {{ Auth::user()->name }} -  {{isset(Auth::user()->get_user_detail->get_unit_fakultas->name) ? Auth::user()->get_user_detail->get_unit_fakultas->name : '-' }} </h6>
                                    <p>Segala bentuk data yang anda inputkan akan kami simpan</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                @foreach($isianFormulir as $value)
            <form method="post" id="form" action="{{URL('transaksi/isi-formulir-form/'.$dataFormulir->id.'/store-form') }}">
                @csrf
                @method('PUT')
                <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                    <div class="kt-portlet mt-2">
                        <div class="kt-portlet__body mt-2 pl-5 pr-5">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label style="font-weight: bold">{{ $value->get_type_detail->pertanyaan }}<span class="kt-font-danger"> @if($value->get_type_detail->is_required == 1) * @endif </span></label>
                                    <p class="mt-2 ml-1">@if($value->get_type_detail->deskripsi != null)
                                            {{ $value->get_type_detail->deskripsi }} @endif</p>

                                    <?php $val = unserialize($value->value); ?>

{{--                                    @foreach($val as $valueIsian)--}}
{{--                                        <div class="col-md-6">--}}
{{--                                            <div class="mt-4" style="display: flex">--}}
{{--                                                <input type="{{ $value->get_type_detail->get_type->type }}" id="1" class="mr-1" name="{{ $value->get_type_detail->get_type->type }}[]" value="{{ $valueIsian }}">--}}
{{--                                                <div style="margin-top: -3px; margin-left: 5px;">{{ $valueIsian }}</div>--}}
{{--                                            </div>--}}
{{--                                        </div>--}}
{{--                                    @endforeach--}}

                                    <?php $name = str_replace(' ','_', strtolower($value->get_type_detail->get_type->name)) ?>
                                    @if($value->get_type_detail->get_type->name == "Pilihan Ganda")
                                        @foreach($val as $valueIsian)
                                            <div class="col-md-6">
                                                <div class="mt-4" style="display: flex">
                                                    <input type="radio" id="1" class="mr-1" name="{{ $name }}{{ $value->id }}[]" value="{{ $valueIsian }}">
                                                    <div style="margin-top: -3px; margin-left: 5px;">{{ $valueIsian }}</div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @elseif($value->get_type_detail->get_type->name == "Kotak Centang")
                                        @foreach($val as $valueIsian)
                                            <div class="col-md-6">
                                                <div class="mt-4" style="display: flex">
                                                    <input type="checkbox" class="mr-1" name="{{ $name }}{{ $value->id }}[]" value="{{ $valueIsian }}">
                                                    <div style="margin-top: -3px; margin-left: 5px;">{{ $valueIsian }}</div>
                                                </div>
                                            </div>
                                        @endforeach
                                    @elseif($value->get_type_detail->get_type->name == "Jawaban Singkat")
                                        <input type="text" name="{{ $name }}{{ $value->id }}[]" class="form-control mt-2 {{ $errors->has('') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Jawaban singkat" value="{{old('')}}">
                                    @elseif($value->get_type_detail->get_type->name == "Paragraf")
                                        <textarea style="width: 100%; max-height: 80px; min-height: 80px" type="text" name="{{ $name }}{{ $value->id }}[]" class="form-control mt-2 {{ $errors->has('') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Paragraf" value="{{old('')}}"></textarea>
                                    @elseif($value->get_type_detail->get_type->name == "Dropdown")
                                        <select name="{{ $name }}{{ $value->id }}[]" class="form-control select2 {{ $errors->has('name') ? 'is-invalid' :'' }}">
                                            <option value="" selected disabled>Pilih Semester</option>
                                        </select>
                                    @elseif($value->get_type_detail->get_type->name == "Upload file")
                                        <input type="file" name="{{ $name }}{{ $value->id }}[]" class="form-control mt-2 {{ $errors->has('') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Upload File" value="{{old('')}}">
                                    @elseif($value->get_type_detail->get_type->name == "Tanggal")
                                        <input type="date" name="{{ $name }}{{ $value->id }}[]" class="form-control mt-2 {{ $errors->has('') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Tanggal" value="{{old('')}}">
                                    @else
                                        <input type="time" name="{{ $name }}{{ $value->id }}[]" class="form-control mt-2 {{ $errors->has('') ? 'is-invalid' :'' }} form_name" autocomplete="off" placeholder="Waktu" value="{{old('')}}">
                                    @endif
                                    <p class="text-right mt-5 text-warning">Angka: 50 | Teks: Email, URL</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                @endforeach

                <div class="kt-container  kt-grid__item kt-grid__item--fluid mt-5">
                    <div class="d-flex justify-content-between">
                        <div>
                            <button type="submit" class="btn btn-primary">Simpan</button>
                        </div>
                        <div class="text-right">
                            <input type="reset" value="Kosongkan Formulir" onclick="this.form.reset();" class="btn btn-light"/>
                        </div>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
    $('#form')[0].reset();
</script>
@endpush
