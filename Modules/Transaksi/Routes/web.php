<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


use Illuminate\Support\Facades\Route;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;

Route::middleware(['web','auth'])->prefix('transaksi')->group(function () {
    Route::resource('/isi-formulir', 'TransaksiController');
    Route::post('/isi-formulir/search', 'TransaksiController@index');
    Route::get('/isi-formulir/{id}/create', 'TransaksiController@create');
    Route::put('/isi-formulir/{id}/store', 'TransaksiController@store');
    Route::put('/isi-formulir-form/{id}/store-form', 'TransaksiFormController@storeform');
    Route::get('isi-formulir/{id}/edit/get_data_tables', 'TransaksiController@getDataTable');
    Route::post('isi-formulir/{id}/edit/simpan_edit_data_tables', 'TransaksiController@simpanEditDataTables');
    Route::post('isi-formulir/{id}/edit/delete_edit_data_tables', 'TransaksiController@deleteEditDataTables');

    Route::resource('/isi-formulir-form', 'TransaksiFormController');
    Route::get('/isi-formulir-form/{id}/create', 'TransaksiFormController@create');

    Route::get('/history-formulir/{id}', 'TransaksiFormController@historyFormulir');
});
