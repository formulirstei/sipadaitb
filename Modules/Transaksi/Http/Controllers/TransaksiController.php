<?php

namespace Modules\Transaksi\Http\Controllers;

use App\Services\IsiFormulirServices;
use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Redirect;
use Modules\Data\Entities\MFormulir;
use Modules\Data\Entities\UnitFakultas;

class TransaksiController extends Controller
{

    public function __construct(IsiFormulirServices $IsiFormulirServices)
    {
        $this->IsiFormulirService = $IsiFormulirServices;
    }


    public function index(Request $request)
    {
        $key = $request->key_search;
        if (Gate::check('tambah formulir is admin')){
            // $dataFormulirs = $this->IsiFormulirService->get($request);
            if ($key == null) {
                $dataFormulirs = MFormulir::paginate(9);
            } else {
                $dataFormulirs = MFormulir::where('name' , 'like', '%' . $key . '%')->paginate(9)->withQueryString();
            }
        } else {
            $get_id_unitFakultas = UnitFakultas::where('id', Auth::user()->get_user_detail->m_unitfakultas_id)->first();
            $dataFormulirs =  $get_id_unitFakultas->m_formulir()->where('name' , 'like', '%' . $key . '%')->paginate(9)->withQueryString();
        }

        return view('transaksi::isi-formulir.index', compact('dataFormulirs', 'key'));
    }

    public function create(Request $request, $id)
    {
        $dataFormulir = $this->IsiFormulirService->show($request, $id);
        return view('transaksi::isi-formulir.add', compact('dataFormulir'));
    }

    public function store(Request $request, $id)
    {
        // if (!Gate::check('tambah formulir is admin') || !Gate::check('tambah formulir is creator')) {
        //     return abort(403);
        // }
        $dataFormulir = $this->IsiFormulirService->create($request, $id);
        if (!is_null($dataFormulir)) {
            $message = "Berhasil Menambah Data Formulir";
            return Redirect::to('transaksi/isi-formulir/'.$id.'')->with("message", $message);
        }
        return Redirect::to('transaksi/isi-formulir/'.$id.'/create')->with("messageerror", "Gagal Menambah Data Formulir");
        $dataFormulir = $this->IsiFormulirService->create($request, $id);
    }

    public function show(Request $request, $id)
    {
        // if (!Gate::check('tambah formulir is admin') || !Gate::check('tambah formulir is creator')) {
        //     return abort(403);
        // }
        // if (!$request->ajax()) {
        //     return abort(403);
        // }
        $dataFormulir = $this->IsiFormulirService->show($request,$id);

        $dataIsianFormulirs = $this->IsiFormulirService->detail($request,$id);

        if (is_null($dataIsianFormulirs) && is_null($dataFormulir)) {
            return Redirect::to('/transaksi/isi-formulir')->with("messageerror", "Data Periode Tidak Tersedia");
        }

        return view('transaksi::isi-formulir.detail', compact('dataIsianFormulirs', 'dataFormulir'));
    }

    public function edit(Request $request, $id)
    {
        $dataFormulir = $this->IsiFormulirService->show($request,$id);
        $dataIsianFormulirs = $this->IsiFormulirService->editData($request,$id);
        return view('transaksi::isi-formulir.edit', compact('dataFormulir', 'dataIsianFormulirs'));

    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function getDataTable($id)
    {
        # code...
        $getFormulir = MFormulir::find($id);
        $getTableName = $getFormulir->table_name;
        $getTables = DB::select('select * from '.$getTableName.'');
        $i =0;
        $data =[];
        foreach ($getTables as $value) {
            # code...
            $i=$value->id;
            $data[$i] = $value;
        }
        return response()->json([$data]);
    }

    public function simpanEditDataTables(Request $request, $id)
    {
        # code...
        // dd($request->all());
        $data = 'ada';
        $getFormulir    = MFormulir::where('id', $id)->first();
        foreach ($request->temp_data_edit as $data) {
            # code...
            if (!is_null($data)) {
                # code...
                // dd($data);
                $getTable       = DB::table($getFormulir->table_name)->where('id',$data['id'])->update($data);
            }
        }

        return response()->json([$getTable]);
    }

    public function deleteEditDataTables(Request $request, $id)
    {
        # code...
        // dd($request->all());
        $data = 'ada';
        $getFormulir    = MFormulir::where('id', $id)->first();
        foreach ($request->temp_data_hapus as $data) {
            # code...
            if (!is_null($data)) {
                # code...
                // dd($data);
                $getTable       = DB::table($getFormulir->table_name)->delete($data['id']);
            }
        }

        return response()->json([$getTable]);
    }

}
