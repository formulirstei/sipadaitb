<?php
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::middleware(['web','auth'])->prefix('privilege')->group(function () {
    Route::resource('user', 'UsersController');
    Route::post('deleteDataUser', 'UsersController@deleteData');
    Route::resource('role', 'RolesController');
    Route::post('deleteDataRole', 'RolesController@deleteData');
    Route::resource('permission', 'PermissionsController');
    Route::post('deleteDataPermission', 'PermissionsController@deleteData');

    Route::get('getTypeUnitFakultas/{type?}', 'UsersController@getDataUnitFakultas');
});
