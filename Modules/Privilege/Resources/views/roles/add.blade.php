@extends('layouts.template_backend',['title'=>((!$createMode) ? "Ubah" : "Tambah").' Role'])
@section('sidebar')
    @include('page.page_header',['title'=>'Role','breadcumb'=>array('Privilege;#','Role;'.URL('privilege/role'),((!$createMode) ? "Ubah" : "Tambah").' Role;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endsection
@section('content')
    @include('errors.validasi')

    <form role="form" method="post"
          action="{{ $createMode ? URL(config('settings.page_backend')."privilege/role") : URL("privilege/role/".$dataRoles->id."") }}"
          enctype="multipart/form-data">
        @csrf

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        {{((!$createMode) ? "Ubah" : "Tambah")}} Role
                    </h3>
                </div>
            </div>

            @if (!$createMode)
              @method('put')
            @endif

            <div class="kt-portlet__body">
                <div class="row">
                    <div class="form-group col-md-4 ">
                        <label>Nama Role<span class="kt-font-danger">*</span></label>
                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' :'' }}" placeholder="Masukan Nama Role" value="{{old('name',$dataRoles->name)}}">
                    </div>

                    <div class="form-group col-md-12 ">
                        <label>Permission<span class="kt-font-danger">*</span></label> <br>
                        <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success"><input type="checkbox" class="checked-all-permission"> Semua Permission <span></span> </label>
                        <p></p>
                        <div class="row">
                            @foreach($dataPermissions as $permission)
                                <?php
                                  if($dataRoles->hasPermissionTo($permission->name)){
                                    $status="checked";
                                }else{
                                    $status="";
                                }
                                ?>
                                <div class="col-md-4">
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success checked-row"><input type="checkbox" {{!is_array(old('permission'))?$status:''}} name="permission[]" value="{{$permission->id}}" {{ (is_array(old('permission',$dataPermissions)) && in_array($permission->id, old('permission',$dataPermissions))) ? ' checked' : '' }}> {{$permission->name}} <span></span> </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>

                </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions clearfix">
                    <div class="buttons-group float-right">
                        @if($createMode)
                            <button type="submit" class="btn btn-success " name="actionsave"
                                    value="save"><i class="flaticon2-writing font-sizebtn"></i>Simpan
                            </button>
                            <button type="submit" class="btn btn-info " name="actionsave"
                                    value="savenew"><i class="flaticon2-writing font-sizebtn"></i>Simpan & Tambah Baru
                            </button>
                        @else
                            <button type="submit" class="btn btn-info "><i
                                    class="flaticon2-writing font-sizebtn"></i>Simpan Perubahan
                            </button>
                        @endif
                    </div>
                </div>
            </div>


        </div>
    </form>
@endsection

@push('scripts')
    <script>

        $(".checked-all-permission").click(function(){
            $('.checked-row input').not(this).prop('checked', this.checked);
        });

    </script>
@endpush
