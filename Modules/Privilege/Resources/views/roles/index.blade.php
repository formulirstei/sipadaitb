@extends('layouts.template_backend',['title'=>'Data Role'])
@section('sidebar')
    @include('page.page_header',['title'=>'Data Role',(Auth::user()->can('tambah role') ? 'add_data' : '')=>'Tambah Role','url_data'=>URL(Config::get('settings.page_backend').'privilege/role/create'),'breadcumb'=>array('Privilege;#','Role;#') ,'url_delete'=>URL('privilege/deleteDataRole'), 'reload'=>true , 'delete_multiple'=>true , 'back'=>false])
@endsection
@section('content')
     @include('errors.validasi')

     <div class="kt-portlet kt-portlet--mobile">
         <div class="kt-portlet__body kt-portlet__body--fit">
             <div class="kt-datatable" id="datatableData"></div>
         </div>
     </div>

     @include('modal.modal', [
        'modal_title' => 'Detail Role',
        'modal_id'    => 'detail-modal',
        'modal_size'  => 'lg'
      ])

@endsection

@push('scripts')
    <script>
        var urlAjax = "{{URL('privilege/role')}}";
        datatableData = $('#datatableData').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: urlAjax,
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState :{
                    cookie: false,
                    webstorage: false
                }
            },
            layout: {
                scroll: false,
                footer: false,
                spinner:{
                    message: 'Sedang Memuat Data...'
                }
            },
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
                delay: 300,
            },
            columns: [{
                field: 'id',
                title: '#',
                sortable: false,
                width: 20,
                selector: {
                    class: 'kt-checkbox--solid'
                },
                textAlign: 'center',
            }, {
                field: "name",
                title: "Nama",
            }, {
                field: "guard_name",
                title: "Guard",
            }, {
                field: "Aksi",
                title: "Aksi",
                sortable: false,
                autoHide: false,
                overflow: 'visible',
                width: 100,
                template: function(row) {
                    var actions = '<div class="btn-group" role="group">';
                    @if (Gate::check('detail role') or Gate::check('edit role') or Gate::check('hapus role'))
                        @if(Gate::check('detail role'))
                            actions += '<a data-modal-url="{{URL('privilege/role')}}/'+row.id+'" class="btn btn-icon btn-info btn-sm btn-detail white" data-toggle="kt-tooltip"  data-placement="left" title="" data-original-title="Detail Data"><i class="la la-eye"></i></a>';
                        @endif
                        @if (Gate::check('edit role'))
                            actions += '<a href="{{URL('privilege/role')}}/'+row.id+'/edit" class="btn btn-icon btn-warning btn-sm icon-white" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Edit Data"><i class="la la-pencil"></i></a>';
                        @endif
                        @if (Gate::check('hapus role'))
                            actions += '<button onclick="deleteData(this)" data-id="'+row.id+'" data-url="{{URL('privilege/deleteDataRole')}}" type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Hapus Data"><i class="la la-trash-o"></i></button>';
                        @endif
                    @else
                        actions += '<button type="button" class="btn btn-font-sm  btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="" data-original-title="Aksi Tidak Tersedia"><i class="la la-close"></i> Tidak Tersedia</button>';
                    @endif
                        actions += '</div>';
                        
                    return actions;
                }
            }]
        });

    </script>
@endpush
