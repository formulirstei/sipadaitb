<table class="table table-bordered table-striped table-condensed">
    <tbody>

    <tr>
        <td>Nama </td>
        <td>{{ $dataRoles->name }}</td>
    </tr>

    <tr>
        <td>Guard </td>
        <td>{{ $dataRoles->guard_name }}</td>
    </tr>

    <tr>
        <td>Permission </td>
        <td>
            <div class="row">
            @foreach($dataPermissions as $permission)
                <?php
                if($dataRoles->hasPermissionTo($permission->name)){
                    $status="checked";
                }else{
                    $status="";
                }
                ?>
                <div class="col-md-4">
                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success checked-row"><input type="checkbox" disabled {{$status}}  id="checkedPermission" name="permission[]" value="{{$permission->id}}"> {{$permission->name}} <span></span> </label>
                </div>
            @endforeach
            </div>
        </td>
    </tr>

    </tbody>
</table>
