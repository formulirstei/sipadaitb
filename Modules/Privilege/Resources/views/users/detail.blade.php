<table class="table table-bordered table-striped table-condensed">
    <tbody>

    <tr>
        <td>Nama </td>
        <td>{{ $dataUsers->name }}</td>
    </tr>

    <tr>
        <td>Username </td>
        <td>{{ $dataUsers->username }}</td>
    </tr>

    <tr>
        <td>Email </td>
        <td>{{ $dataUsers->email }}</td>
    </tr>

    <tr>
        <td>Type </td>
        <td>{{isset($dataUsers->get_user_detail->type) ? $dataUsers->get_user_detail->type : '-'}} {{isset($dataUsers->get_user_detail->get_unit_fakultas->name) ? $dataUsers->get_user_detail->get_unit_fakultas->name : '-' }} </td>
    </tr>

    <tr>
        <td>Role </td>
        <td>
            @foreach ($dataUsers->roles as $role)
                {!! $role->name !!}
            @endforeach
        </td>
    </tr>

    <tr>
        <td class="w-25">Di Buat Oleh</td>
        <td class="w-75">{{(!empty($dataUsers->get_created_by) ? $dataUsers->get_created_by->name : '-')}}</td>
    </tr>

    <tr>
        <td class="w-25">Di Ubah Oleh</td>
        <td class="w-75">{{(!empty($dataUsers->get_updated_by) ? $dataUsers->get_updated_by->name : '-')}}</td>
    </tr>

    <tr>
        <td class="pad-l-20 w-25">Created at</td>
        <td class="pad-l-20 w-75">{{ $dataUsers->getCreatedAt('echo') }}</td>
    </tr>
    <tr>
        <td class="pad-l-20 w-25">Updated at</td>
        <td class="pad-l-20 w-75">{{ $dataUsers->getCreatedAt('echo') }}</td>
    </tr>
    </tbody>
</table>
