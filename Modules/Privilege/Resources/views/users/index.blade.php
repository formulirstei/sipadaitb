@extends('layouts.template_backend',['title'=>'Data User'])
@section('sidebar')
    @include('page.page_header',['title'=>'Data User',((Auth::user()->can('tambah user') ? 'add_data' : ''))=>'Tambah User','url_data'=>URL('privilege/user/create'),'breadcumb'=>array('Privilege;#','User;#') ,'url_delete'=>URL('privilege/deleteDataUser'), 'reload'=>true , 'delete_multiple'=>true , 'back'=>false])
@endsection
@section('content')
    @include('errors.validasi')

    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body kt-portlet__body--fit">
            <div class="kt-datatable" id="datatableData"></div>
        </div>
    </div>


    @include('modal.modal', [
       'modal_title' => 'Detail Users',
       'modal_id'    => 'detail-modal',
       'modal_size'  => 'lg'
     ])

@endsection
@push('scripts')
    <script>
        var urlAjax = "{{URL('privilege/user')}}";
        datatableData = $('#datatableData').KTDatatable({
            data: {
                type: 'remote',
                source: {
                    read: {
                        method: 'GET',
                        url: urlAjax,
                    },
                },
                pageSize: 10,
                serverPaging: true,
                serverFiltering: true,
                serverSorting: true,
                saveState :{
                    cookie: false,
                    webstorage: false
                }
            },
            layout: {
                scroll: false,
                footer: false,
                spinner:{
                    message: 'Sedang Memuat Data...'
                }
            },
            sortable: true,
            pagination: true,
            search: {
                input: $('#generalSearch'),
                delay: 300,
            },
            columns: [{
                field: 'id',
                title: '#',
                sortable: false,
                width: 20,
                selector: {
                    class: 'kt-checkbox--solid'
                },
                textAlign: 'center',
            }, {
                field: "name",
                title: "Nama",
            }, {
                field: "username",
                title: "Username",
            },{
                field: "email",
                title: "Email",
            }, {
                field: "type",
                title: "Type",
                template: function(row) {
                    console.log(row.get_user_detail.type)
                    return row.get_user_detail.type;
                }
            },
            // {
            //     field: "profile",
            //     title: "Profile",
            //     width: 70,
            //     template: function(row) {
            //         var img = 'openImage("'+row.image_url+'")';
            //         var actions='<div class="kt-media pointer" onclick=' + img + '><img src="'+row.image_url+'" alt="image"></div>';
            //         return actions;
            //     }
            // }
            // {
            //     field: "role",
            //     title: "Role",
            //     template: function(row) {
            //         console.log(row['roles'][0])
            //         return row.role_html;
            //     }
            // }
            {
                field: "Aksi",
                title: "Aksi",
                sortable: false,
                autoHide: false,
                overflow: 'visible',
                width: 100,
                template: function(row) {
                    var actions = '<div class="btn-group" user="group">';
                        @if (Gate::check('detail user') or Gate::check('edit user') or Gate::check('hapus user'))
                            @if(Gate::check('detail user'))
                                actions += '<a data-modal-url="{{URL('privilege/user')}}/'+row.id+'" class="btn btn-icon btn-info btn-sm btn-detail white" data-toggle="kt-tooltip"  data-placement="left" title="" data-original-title="Detail Data"><i class="la la-eye"></i></a>';
                            @endif
                            @if(Gate::check('edit user'))
                                actions += '<a href="{{URL('privilege/user')}}/'+row.id+'/edit" class="btn btn-icon btn-warning btn-sm icon-white" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Edit Data"><i class="la la-pencil"></i></a>';
                            @endif
                            @if(Gate::check('hapus user'))
                                actions += '<button onclick="deleteData(this)" data-id="'+row.id+'" data-url="{{URL('privilege/deleteDataUser')}}" type="button" class="btn btn-icon btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Hapus Data"><i class="la la-trash-o"></i></button>';
                            @endif
                        @else
                            actions += '<button type="button" class="btn btn-font-sm  btn-danger btn-sm" data-toggle="kt-tooltip" data-placement="top" title="" data-original-title="Aksi Tidak Tersedia"><i class="la la-close"></i> Tidak Tersedia</button>';
                        @endif
                        actions += '</div>';

                    return actions;
                }
            }]
        });

    </script>
@endpush
