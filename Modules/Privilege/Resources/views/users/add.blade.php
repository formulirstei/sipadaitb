@extends('layouts.template_backend',['title'=>((!$createMode) ? "Ubah" : "Tambah").' User'])
@section('sidebar')
    @include('page.page_header',['title'=>'User','breadcumb'=>array('Privilege;#','User;'.URL('privilege/user'),((!$createMode) ? "Ubah" : "Tambah").' User;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
    {{-- @include('page.page_header',['title'=>'User','breadcumb'=>array('Privilege;#','User;'.URL('privilege/user'),((!$createMode) ? "Ubah" : "Tambah").' User;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>true]) --}}
@endsection
@section('content')
    @include('errors.validasi')

    <form permission="form" method="post"
          action="{{ $createMode ? URL("privilege/user") : URL("privilege/user/".$dataUsers->id."") }}"
          enctype="multipart/form-data">
        @csrf

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        {{((!$createMode) ? "Ubah" : "Tambah")}} User
                    </h3>
                </div>
            </div>

            @if (!$createMode)
              @method('put')
            @endif

            <div class="kt-portlet__body">
                <div class="row">
                    <div class="form-group col-md-3 ">
                        <label>Nama Lengkap<span class="kt-font-danger">*</span></label>
                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' :'' }}" placeholder="Masukan Nama Lengkap" value="{{old('name',$dataUsers->name)}}">
                    </div>
                    <div class="form-group col-md-3 ">
                        <label>Username<span class="kt-font-danger">*</span></label>
                        <input type="text" name="username" class="form-control {{ $errors->has('username') ? 'is-invalid' :'' }}" placeholder="Masukan username" value="{{old('username',$dataUsers->username)}}">
                    </div>
                    <div class="form-group col-md-3 ">
                        <label>E-mail<span class="kt-font-danger">*</span></label>
                        <input type="email" name="email" class="form-control {{ $errors->has('email') ? 'is-invalid' :'' }}" placeholder="Masukan email" value="{{old('email',$dataUsers->email)}}">
                    </div>

                    <div class="form-group col-md-3 ">
                        <label>Password<span class="kt-font-danger">*</span></label>
                        <input type="password" name="password" class="form-control {{ $errors->has('password') ? 'is-invalid' :'' }}" placeholder="Masukan Password" value="{{old('password')}}">
                    </div>

                    <div class="form-group col-md-4 ">
                        <label>Type<span class="kt-font-danger">*</span></label>
                        <select name="type" id="type" class="form-control select2 {{ $errors->has('type') ? 'is-invalid' :'' }}">
                            <option value="" selected disabled>Pilih Type</option>
                            <option value="unit" @if(old('type', isset($dataUsers->get_user_detail->type)?$dataUsers->get_user_detail->type : '') == 'unit') selected @endif>Unit</option>
                            <option value="fakultas" @if(old('type', isset($dataUsers->get_user_detail->type)?$dataUsers->get_user_detail->type : '') == 'fakultas') selected @endif>Fakultas</option>
                            <option value="inspektor" @if(old('type', isset($dataUsers->get_user_detail->type)?$dataUsers->get_user_detail->type : '') == 'inspektor') selected @endif>Inspektor</option>
                            <option value="admin" @if(old('type', isset($dataUsers->get_user_detail->type)?$dataUsers->get_user_detail->type : '') == 'admin') selected @endif>Admin</option>
                        </select>
                    </div>


                    <div class="form-group col-md-8 " id="unitFakultas" style="display: none">
                        <label>Unit & Fakultas<span class="kt-font-danger">*</span></label>
                        <select name="unit_fakultas" id="unit_fakultas" class="form-control select2 {{ $errors->has('unit_fakultas') ? 'is-invalid' :'' }}">
                            <option value="" selected disabled>Pilih Type</option>
                        </select>
                    </div>

                    <div class="form-group col-md-8 " id="unitDanFakultas" style="display: none">
                        <label>Unit & Fakultas<span class="kt-font-danger">*</span></label>
                        <select name="unit_dan_fakultas[]" id="unit_dan_fakultas" class="form-control select2" multiple="multiple">
                            <option value="all">Semua Unit & Fakultas</option>
                            @foreach($unitFakultas as $value)
                                <option value="{{ $value->id }}">{{ strtoupper($value->type)  }} - {{ strtoupper($value->name) }}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-12 ">
                        <label>Hak Akses<span class="kt-font-danger">*</span></label> <br>
                        <p></p>
                        <div class="row">
                            @foreach($dataRoles as $role)
                                <?php
                                  if($dataUsers->hasRole($role->name)){
                                    $status="checked";
                                }else{
                                    $status="";
                                }
                                ?>
                                <div class="col-md-4">
                                    <label class="kt-checkbox kt-checkbox--tick kt-checkbox--success checked-row">
                                    <input type="checkbox" {{!is_array(old('role')) ? $status :''}} name="role[]" value="{{$role->id}}" {{ (is_array(old('role',$dataRoles)) && in_array($role->id, old('role',$dataRoles))) ? ' checked' : '' }}> {{$role->name}} <span></span> </label>
                                </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions clearfix">
                    <div class="buttons-group float-right">
                        @if($createMode)
                            <button type="submit" class="btn btn-success " name="actionsave"
                                    value="save"><i class="flaticon2-writing font-sizebtn"></i>Simpan
                            </button>
                            <button type="submit" class="btn btn-info " name="actionsave"
                                    value="savenew"><i class="flaticon2-writing font-sizebtn"></i>Simpan & Tambah Baru
                            </button>
                        @else
                            <button type="submit" class="btn btn-info "><i
                                    class="flaticon2-writing font-sizebtn"></i>Simpan Perubahan
                            </button>
                        @endif
                    </div>
                </div>
            </div>


        </div>
    </form>
@endsection

@push('scripts')
    <script>
        $(document).ready(function() {
        $('select[name="type"]').on('change', function() {
            var typeVal = $(this).val();
            if(typeVal == "unit" || typeVal == "fakultas") {
                $('#unitFakultas').show();
                $('#unitDanFakultas').hide();
                $.ajax({
                    url: "{{URL('privilege/getTypeUnitFakultas')}}/"+typeVal,
                    type: "GET",
                    dataType: "json",
                    success:function(data) {
                        $('select[name="unit_fakultas"]').empty();
                        $.each(data, function(key, value) {
                            $('select[name="unit_fakultas"]').append('<option value="'+ value['id'] +'">'+ value['text'] +'</option>');
                        });
                    }
                });
            }else if(typeVal == "inspektor"){
                $('#unitFakultas').hide();
                $('#unitDanFakultas').hide();
            }else{
                $('#unitFakultas').hide();
                $('#unitDanFakultas').hide();
                $('select[name="unit_fakultas"]').empty();
            }
        });
    });
    </script>
@endpush
