@extends('layouts.template_backend',['title'=>((!$createMode) ? "Ubah" : "Tambah").' Permission'])
@section('sidebar')
    @include('page.page_header',['title'=>'Permission','breadcumb'=>array('Privilege;#','Permission;'.URL('privilege/permission'),((!$createMode) ? "Ubah" : "Tambah").' Permission;#') , 'reload'=>false , 'delete_multiple'=>false , 'back'=>true])
@endsection
@section('content')
    @include('errors.validasi')

    <form permission="form" method="post"
          action="{{ $createMode ? URL(config('settings.page_backend')."privilege/permission") : URL("privilege/permission/".$dataPermissions->id."") }}"
          enctype="multipart/form-data">
        @csrf

        <div class="kt-portlet">
            <div class="kt-portlet__head">
                <div class="kt-portlet__head-label">
                    <h3 class="kt-portlet__head-title">
                        {{((!$createMode) ? "Ubah" : "Tambah")}} Permission
                    </h3>
                </div>
            </div>

            @if (!$createMode)
              @method('put')
            @endif

            <div class="kt-portlet__body">
                <div class="row">
                    <div class="form-group col-md-4 ">
                        <label>Nama Permission<span class="kt-font-danger">*</span></label>
                        <input type="text" name="name" class="form-control {{ $errors->has('name') ? 'is-invalid' :'' }}" placeholder="Masukan Nama Permission" value="{{old('name',$dataPermissions->name)}}">
                    </div>

                </div>

                </div>

            <div class="kt-portlet__foot">
                <div class="kt-form__actions clearfix">
                    <div class="buttons-group float-right">
                        @if($createMode)
                            <button type="submit" class="btn btn-success " name="actionsave"
                                    value="save"><i class="flaticon2-writing font-sizebtn"></i>Simpan
                            </button>
                            <button type="submit" class="btn btn-info " name="actionsave"
                                    value="savenew"><i class="flaticon2-writing font-sizebtn"></i>Simpan & Tambah Baru
                            </button>
                        @else
                            <button type="submit" class="btn btn-info "><i
                                    class="flaticon2-writing font-sizebtn"></i>Simpan Perubahan
                            </button>
                        @endif


                    </div>
                </div>
            </div>


        </div>
    </form>
@endsection

@push('scripts')
    <script>

    </script>
@endpush
