<?php

namespace Modules\Privilege\Entities;

use App\Helpers\LayananHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\Permission\Traits\HasRoles;

class Users extends Model
{
    use SoftDeletes;
    use HasRoles;

    protected $table = 'users';
    protected $primaryKey='id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $fillable = ['name', 'email', 'username','password', 'created_at','updated_at'];

    public function getRoleHtmlAttribute(){
        $permissions = unserialize($this->permission);
        $message ="";
        if(!empty($permissions)){
            foreach($permissions as $value){
                $message.='<span class="kt-badge kt-badge--info kt-badge--inline kt-badge--pill kt-badge--rounded">'.$value['name'].'</span> ';
            }
        }
        return $message;
    }

    public function getCreatedAt($type){
        $tanggal="";
        if ($this->created_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->created_at, $type);
        }
        return $tanggal;
    }


    public function getUpdatedAt($type){
        $tanggal="";
        if ($this->updated_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->updated_at, $type);
        }
        return $tanggal;
    }


    public function getDeletedAt($type){
        $tanggal="";
        if ($this->deleted_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->deleted_at, $type);
        }
        return $tanggal;
    }


}
