<?php

namespace Modules\Privilege\Entities;

use App\Helpers\LayananHelper;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Modules\Data\Entities\UnitFakultas;

class UserDetail extends Model
{

    protected $table = 'user_details';
    protected $primaryKey='id';
    protected $keyType = 'string';
    public $incrementing = false;
    protected $fillable = ['type','user_id', 'm_unitfakultas_id', 'created_at','updated_at','created_by','updated_by','deleted_at'];

    public function get_user(){
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function get_unit_fakultas(){
        return $this->belongsTo(UnitFakultas::class, 'm_unitfakultas_id', 'id');
    }

    public function getCreatedAt($type){
        $tanggal="";
        if ($this->created_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->created_at, $type);
        }
        return $tanggal;
    }


    public function getUpdatedAt($type){
        $tanggal="";
        if ($this->updated_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->updated_at, $type);
        }
        return $tanggal;
    }


    public function getDeletedAt($type){
        $tanggal="";
        if ($this->deleted_at != "") {
            $tanggal = LayananHelper::changeTanggalDate($this->deleted_at, $type);
        }
        return $tanggal;
    }


}
