<?php

namespace Modules\Privilege\Http\Requests;
use Modules\Privilege\Http\Requests\Request;
class RolesValidasi extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "name" => "required|max:191"
        ];

    }

    public function messages()
    {

        return [
            "name.required" => "Nama Role Belum Terisi"
            ];
    }

}


