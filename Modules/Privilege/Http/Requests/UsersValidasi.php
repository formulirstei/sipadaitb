<?php

namespace Modules\Privilege\Http\Requests;
use Modules\Privilege\Http\Requests\Request;
class UsersValidasi extends Request
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            "name" => "required|max:191",
            "username" => "required",
            "password" => "required",
            "email" => "required|email",
        ];

    }

    public function messages()
    {

        return [
            "name.required" => "Nama User Belum Terisi",
            "username.required" => "Username Belum Terisi",
            "password.required" => "Password Belum Terisi",
            "email.required" => "Email Belum Terisi",
            "email.email" => "Format E-mail tidak benar"
            ];
    }

}


