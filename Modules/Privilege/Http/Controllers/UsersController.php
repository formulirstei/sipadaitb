<?php

namespace Modules\Privilege\Http\Controllers;

use App\Helpers\DatatableCustom;
use App\Services\RoleServices;
use App\Services\UnitFakultasServices;
use App\Services\UsersServices;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Modules\Privilege\Http\Requests\UsersValidasi;
use Spatie\Permission\Models\Role;

class UsersController extends Controller
{

    public function __construct(RoleServices $roleServices,
                                UsersServices $usersServices ,
                                User $users,
                                UnitFakultasServices $unitFakultasServices)
    {
        $this->roleService  = $roleServices;
        $this->usersService  = $usersServices;
        $this->unitFakultasService  = $unitFakultasServices;
        $this->users = $users;
    }

    public function index(Request $request)
    {
        if (!Gate::check('user')) {
            return abort(403);
        }

        if (!$request->ajax()) {
            return view('privilege::users.index');
        } else {
            $dataUser =  $this->usersService->get($request);
            return DatatableCustom::initDatatable($dataUser,$request);
        }
    }

    public function create(Request $request)
    {
        if (!Gate::check('tambah permission')) {
            return abort(403);
        }

        $dataUsers = $this->users;
        $dataRoles = $this->roleService->get($request);
        $unitFakultas = $this->unitFakultasService->get($request);
        $createMode=true;
        return view('privilege::users.add',compact('dataUsers','unitFakultas','dataRoles','createMode'));
    }

    public function store(UsersValidasi $request)
    {
        if (!Gate::check('tambah permission')) {
            return abort(403);
        }

        if($request->role == null){
            return Redirect::to('privilege/user/create')->with("messageerror", 'Role Harus Di Pilih Terlebih Dahulu')->withInput($request->all());
        }

        $dataUser = $this->usersService->create($request);

        if (!is_null($dataUser)) {
            $message = "Berhasil Menambah Data User";
            if ($request->actionsave == "save") {
                return Redirect::to('privilege/user')->with("message", $message);
            } else {
                return Redirect::to('privilege/user/create')->with("message", $message);
            }
        }

        return Redirect::to('privilege/user')->with("messageerror", "Gagal Menambah Data User");


    }

    public function show(Request $request, $id)
    {
        if (!Gate::check('detail user')) {
            return abort(403);
        }

        if (!$request->ajax()) {
            return abort(403);
        }

        $dataUsers = $this->usersService->show($request,$id);
        if (is_null($dataUsers)) {
            return Redirect::to('privlege/user')->with("messageerror", "Data Users Tidak Tersedia");
        }

        return view('privilege::users.detail', compact('dataUsers'));

    }

    public function edit(Request $request, $id)
    {
        if (!Gate::check('edit permission')) {
            return abort(403);
        }

        $dataUsers = $this->usersService->show($request,$id);
        $dataRoles = $this->roleService->get($request);
        $unitFakultas = $this->unitFakultasService->get($request);

        if (is_null($dataUsers)) {
            return Redirect::to('privilege/user')->with("messageerror", "Data User Tidak Tersedia");
        }

        $createMode = false;
        return view('privilege::users.add', compact('dataUsers','dataRoles','unitFakultas','createMode'));

    }

    public function update(UsersValidasi $request, $id)
    {
        if (!Gate::check('edit user')) {
            return abort(403);
        }

        if($request->role == null){
            return Redirect::to('privilege/user/'.$id.'/edit')->with("messageerror", 'Role Harus Di Pilih Terlebih Dahulu')->withInput($request->all());
        }

        $dataUsers = $this->usersService->show($request,$id);
        if (is_null($dataUsers)) {
            return Redirect::to('privilege/user')->with("messageerror", "Data User Tidak Tersedia");
        }


        $updatDataUser = $this->usersService->update($request,$id);
        $dataRole = Role::whereIn('id',$request->role)->get();
        $dataUsers->syncRoles($dataRole);

        return Redirect::to('privilege/user')->with("message", "Data User Berhasil Di Perbaharui");
    }

    public function deleteData(Request $request)
    {
        if (!Gate::check('hapus role')) {
            return abort(403);
        }

        $idarray = $request->id;
        $deleteGagal = 0;
        $deleteSukses = 0;
        if ($request->ajax()) {
            if (!is_null($idarray)) {
                for ($i = 0; $i < count($idarray); $i++) {
                    $id = $request->id[$i];
                    $this->usersService->delete($request, $id);
                    $deleteSukses = $deleteSukses + 1;
                }
            }
            if ($deleteGagal > 0) {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => '' . $deleteSukses . ' Data User Berhasil Di Hapus dan ' . $deleteGagal . ' Data User Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => '' . $deleteGagal . ' Data User Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                }
            } else {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Data User Berhasil Di Hapus'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Tidak Ada Data User Yang Di Hapus'
                    ]);
                }
            }

        }
    }

    public function getDataUnitFakultas(Request $request, $type = null)
    {
        if (!Gate::check('tambah user')) {
            return abort(403);
        }

        if (!$request->ajax()) {
            return abort(403);
        }

        $data = $this->unitFakultasService->byNama($request,$type);

        return json_encode($data);

    }
}
