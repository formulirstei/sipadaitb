<?php

namespace Modules\Privilege\Http\Controllers;

use App\Helpers\DatatableCustom;
use App\Services\PermissionServices;
use App\Services\RoleServices;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Modules\Privilege\Http\Requests\RolesValidasi;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Gate;

class RolesController extends Controller
{

    public function __construct(RoleServices $roleServices , PermissionServices $permissionServices , Role $role)
    {
        $this->roleService  = $roleServices;
        $this->permissionService = $permissionServices;
        $this->role = $role;
    }

    public function index(Request $request)
    {
        if (!Gate::check('role')) {
            return abort(403);
        }

        if (!$request->ajax()) {
            return view('privilege::roles.index');
        } else {
            $dataRole = Role::query();
            return DatatableCustom::initDatatable($dataRole,$request);
        }
    }

    public function create(Request $request)
    {
        if (!Gate::check('tambah role')) {
            return abort(403);
        }

        $dataRoles = $this->role;
        $dataPermissions = $this->permissionService->get($request);
        $createMode=true;
        return view('privilege::roles.add',compact('dataPermissions','dataRoles','createMode'));
    }

    public function store(RolesValidasi $request)
    {

        if (!Gate::check('tambah role')) {
            return abort(403);
        }

        if($request->permission == null){
            return Redirect::to('privilege/role/create')->with("messageerror", 'Permission Harus Di Pilih Terlebih Dahulu')->withInput($request->all());
        }

        $dataPermission = Permission::whereIn('id',$request->permission)->get();
        $dataRoles = $this->roleService->create($request);
        $dataRoles->syncPermissions($dataPermission);
        if (!is_null($dataRoles)) {
            $message = "Berhasil Menambah Data Role";
            if ($request->actionsave == "save") {
                return Redirect::to('privilege/role')->with("message", $message);
            } else {
                return Redirect::to('privilege/role/create')->with("message", $message);
            }
        }

        return Redirect::to('privilege/role')->with("message", "Gagal Menambah Data Role")->withInput();


   }

    public function show(Request $request, $id)
    {
        if (!Gate::check('detail role')) {
            return abort(403);
        }

        if (!$request->ajax()) {
            return abort(403);
        }

        $dataRoles = $this->roleService->show($request,$id);
        if (is_null($dataRoles)) {
            return Redirect::to('privilege/role')->with("messageerror", "Data Role Tidak Tersedia");
        }
        $dataPermissions = $this->permissionService->get($request);
        return view('privilege::roles.detail', compact('dataRoles','dataPermissions'));

    }

    public function edit(Request $request, $id)
    {
        if (!Gate::check('edit role')) {
            return abort(403);
        }

        $dataRoles = $this->roleService->show($request,$id);
        if (is_null($dataRoles)) {
            return Redirect::to('privilege/role')->with("messageerror", "Data Role Tidak Tersedia");
        }
        $dataPermissions = $this->permissionService->get($request);
        $createMode=false;
        return view('privilege::roles.add', compact('dataRoles','dataPermissions','createMode'));
    }

    public function update(RolesValidasi $request, $id)
    {
        if (!Gate::check('edit role')) {
            return abort(403);
        }

        if($request->permission==null){
            return Redirect::to('privilege/role/create')->with("messageerror", 'Permission Harus Di Pilih Terlebih Dahulu')->withInput($request->all());
        }

        $dataRoles = $this->roleService->show($request,$id);
        if (is_null($dataRoles)) {
            return Redirect::to('privilege/role')->with("messageerror", "Data Role Tidak Tersedia");
        }

        $updatDataPermission = $this->roleService->update($request,$id);
        $dataPermission = Permission::whereIn('id',$request->permission)->get();
        $dataRoles->syncPermissions($dataPermission);

        return Redirect::to('privilege/role')->with("message", "Data Role Berhasil Di Perbaharui");
    }

    public function deleteData(Request $request)
    {
        if (!Gate::check('hapus role')) {
            return abort(403);
        }
        
        $idarray = $request->id;
        $deleteGagal = 0;
        $deleteSukses = 0;
        if ($request->ajax()) {
            if (!is_null($idarray)) {
                for ($i = 0; $i < count($idarray); $i++) {
                    $id = $request->id[$i];
                    $this->roleService->delete($request, $id);
                    $deleteSukses = $deleteSukses + 1;
                }
            }
            if ($deleteGagal > 0) {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => '' . $deleteSukses . ' Data Role Berhasil Di Hapus dan ' . $deleteGagal . ' Data Role Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => '' . $deleteGagal . ' Data Role Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                }
            } else {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Data Role Berhasil Di Hapus'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Tidak Ada Data Role Yang Di Hapus'
                    ]);
                }
            }

        }
    }
}
