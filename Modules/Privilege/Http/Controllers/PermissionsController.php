<?php

namespace Modules\Privilege\Http\Controllers;

use App\Helpers\DatatableCustom;
use App\Services\PermissionServices;
use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;
use Modules\Privilege\Http\Requests\PermissionsValidasi;
use Spatie\Permission\Models\Permission;
use Illuminate\Support\Facades\Redirect;

class PermissionsController extends Controller
{

    public function __construct(PermissionServices $permissionServices , Permission $permission)
    {
        $this->permissionService  = $permissionServices;
        $this->permission = $permission;
    }


    public function index(Request $request)
    {

       if (!Gate::check('permission')) {
           return abort(403);
       }

        if (!$request->ajax()) {
            return view('privilege::permissions.index');
        } else {
            $dataPermission = Permission::query();
            return DatatableCustom::initDatatable($dataPermission,$request);
        }
    }

    public function create(Request $request)
    {
        if (!Gate::check('tambah permission')) {
            return abort(403);
        }

        $dataPermissions = $this->permission;
        $createMode=true;

        return view('privilege::permissions.add',compact('dataPermissions','createMode'));
    }

    public function store(PermissionsValidasi $request)
    {
        if (!Gate::check('tambah permission')) {
            return abort(403);
        }

        $dataPermission = $this->permissionService->create($request);

        if (!is_null($dataPermission)) {
            $message = "Berhasil Menambah Data Permission";
            if ($request->actionsave == "save") {
                return Redirect::to('privilege/permission')->with("message", $message);
            } else {
                return Redirect::to('privilege/permission/create')->with("message", $message);
            }
        }

        return Redirect::to('privilege/permission')->with("messageerror", "Gagal Menambah Data Permission");


    }

    public function show(Request $request, $id)
    {
        if (!Gate::check('detail permission')) {
            return abort(403);
        }

        if (!$request->ajax()) {
            return abort(403);
        }

        $dataPermission = $this->permissionService->show($request,$id);
        if (is_null($dataPermission)) {
            return Redirect::to('privilege/permission')->with("messageerror", "Data Permission Tidak Tersedia");
        }

        return view('privilege::permissions.detail', compact('dataPermission'));

    }

    public function edit(Request $request, $id)
    {
        if (!Gate::check('edit permission')) {
            return abort(403);
        }

        $dataPermissions = $this->permissionService->show($request,$id);
        if (is_null($dataPermissions)) {
            return Redirect::to('privilege/permission')->with("messageerror", "Data Permission Tidak Tersedia");
        }

        $createMode = false;
        return view('privilege::permissions.add', compact('dataPermissions','createMode'));

    }

    public function update(PermissionsValidasi $request, $id)
    {
        if (!Gate::check('edit permission')) {
            return abort(403);
        }

        $dataPermission = $this->permissionService->show($request,$id);
        if (is_null($dataPermission)) {
            return Redirect::to('privilege/permission')->with("messageerror", "Data Permission Tidak Tersedia");
        }

        $updatDataPermission = $this->permissionService->update($request,$id);

        return Redirect::to('privilege/permission')->with("message", "Data Permission Berhasil Di Perbaharui");

    }

    public function deleteData(Request $request)
    {
        if (!Gate::check('hapus permission')) {
            return abort(403);
        }

        $idarray = $request->id;
        $deleteGagal = 0;
        $deleteSukses = 0;
        if ($request->ajax()) {
            if (!is_null($idarray)) {
                for ($i = 0; $i < count($idarray); $i++) {
                    $id = $request->id[$i];
                    $this->permissionService->delete($request,$id);
                    $deleteSukses = $deleteSukses + 1;
                }
            }
            if ($deleteGagal > 0) {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => '' . $deleteSukses . ' Data Permission Berhasil Di Hapus dan ' . $deleteGagal . ' Data Permission Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => '' . $deleteGagal . ' Data Permission Gagal Di Hapus Dikarenakan Sudah Terpakai'
                    ]);
                }
            } else {
                if ($deleteSukses > 0) {
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Data Permission Berhasil Di Hapus'
                    ]);
                } else {
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Tidak Ada Data Permission Yang Di Hapus'
                    ]);
                }
            }

        }
    }
}
