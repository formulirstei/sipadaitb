@if (count($errors) > 0)
    <div class="alert alert-light alert-elevate fade show" role="alert">
        <div class="alert-icon"><i class="flaticon2-warning kt-font-warning"></i></div>
        <div class="alert-text">
            <code>Terdapat Kesalahan , Mohon Untuk Dilihat Kesalahan Dibawah Ini : </code>
            <p></p>
            @foreach ($errors->all() as $error)
                <span class="la la-close"></span> {{ $error }}<br>
            @endforeach
        </div>
    </div>
@endif

@if(Session::has("message"))
    <div class="alert alert-light alert-elevate fade show" role="alert">
        <div class="alert-icon"><i class="flaticon2-check-mark kt-font-success"></i></div>
        <div class="alert-text">
            <code>Yeeeaayyyy! </code> {{ Session::get("message") }}.
        </div>
    </div>
@endif


@if(Session::has("messageerror"))
    <div class="alert alert-light alert-elevate fade show" role="alert">
        <div class="alert-icon"><i class="flaticon2-information kt-font-danger"></i></div>
        <div class="alert-text">
            <code>Terdapat Kesalahan ! </code> {{ Session::get("messageerror") }}.
        </div>
    </div>
@endif
