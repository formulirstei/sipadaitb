
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
    <div class="kt-container ">
        <div class="kt-subheader__main">
            <h3 class="kt-subheader__title">{{$title}} </h3>
            <span class="kt-subheader__separator kt-subheader__separator--v"></span>

            @if(empty($reload))
                <div class="kt-subheader__breadcrumbs">
                    @if(!empty($breadcumb))
                        <a href="{{URL('')}}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>
                        <span class="kt-subheader__breadcrumbs-separator"></span>
                        @foreach($breadcumb as $key => $val)
                            @php $valueBreadCumb = explode(";",$val);
                        $key++;
                            @endphp
                            <a href="{{URL($valueBreadCumb[1])}}" class="kt-subheader__breadcrumbs-link">   {{$valueBreadCumb[0]}} </a>
                            @if($key!=count($breadcumb))
                                <span class="kt-subheader__breadcrumbs-separator"></span>
                            @endif
                        @endforeach
                    @endif
                </div>
            @endif

            @if(!empty($reload))
                <div class="kt-subheader__group" id="kt_subheader_search">
											<span class="kt-subheader__desc" id="kt_subheader_total">
												0 Total </span>
                    <div class="kt-margin-l-20">
                        <div class="kt-input-icon kt-input-icon--right kt-subheader__search">
                            <input type="text" class="form-control" placeholder="Search..." id="generalSearch">
                            <span class="kt-input-icon__icon kt-input-icon__icon--right">
                                <span>
                                    <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                            <rect x="0" y="0" width="24" height="24" />
                                            <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                                            <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
                                        </g>
                                    </svg>

                                    <!--<i class="flaticon2-search-1"></i>-->
                                </span>
                            </span>
                        </div>
                    </div>
                </div>
            @endif
            <div class="kt-subheader__group kt-hidden" id="kt_subheader_group_actions">
                <div class="kt-subheader__desc"><span id="kt_subheader_group_selected_rows"></span> Selected:</div>
                <div class="btn-toolbar kt-margin-l-20">
                    @if(!empty($approval))
                        <div class="dropdown">
                            <button type="button" class="btn btn-label-brand btn-bold btn-sm dropdown-toggle" data-toggle="dropdown">
                                Update Status
                            </button>
                            <div class="dropdown-menu">
                                <a class="dropdown-item" data-url="{{!empty($url_approve)?$url_approve:''}}" id="kt_subheader_group_actions_konfirmasi">Setujui</a>
                                <a class="dropdown-item" data-url="{{!empty($url_rejected)?$url_rejected:''}}" id="kt_subheader_group_actions_rejected">Tolak</a>
                            </div>
                        </div>
                    @endif
                    @if(!empty($url_delete))
                        <button class="btn btn-label-danger btn-bold btn-sm btn-icon-h" data-url="{{!empty($url_delete)?$url_delete:''}}" id="kt_subheader_group_actions_delete_all">
                            Hapus Semua
                        </button>
                    @endif
                </div>
            </div>
        </div>
        <div class="kt-subheader__toolbar">
            @if(!empty($back) && $back)
                <button type="button" id="backButton" class="btn btn-outline-info btn-elevate btn-circle btn-icon" data-toggle="kt-tooltip" title="" data-placement="left" data-original-title="Kembali"><i class="flaticon2-back"></i></button>
            @endif

            @if(!empty($reload) && $reload)
                <button type="button" id="perbaharuiData" class="btn btn-outline-info btn-elevate btn-circle btn-icon" data-toggle="kt-tooltip" title="" data-placement="left" data-original-title="Perbaharui Data"><i class="flaticon2-refresh-1"></i></button>
            @endif

            @if(!empty($add_data))
                @if($url_data=="modal")
                    <button type="button" class="btn btn-label-info btn-bold btn-icon-h kt-margin-l-10" data-toggle="modal" data-target="#pilihFormulir"><i
                        class="flaticon2-writing" ></i>{{$add_data}}
                    </button>
                @else
                    <a href="{{$url_data}}" class="btn btn-label-info btn-bold btn-icon-h kt-margin-l-10" data-toggle="kt-tooltip" title="" data-placement="left" data-original-title="{{$add_data}}">
                        <i class="flaticon2-writing"></i> {{$add_data}}
                    </a>
                @endif

            @endif
        </div>
    </div>
</div>
