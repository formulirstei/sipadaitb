@php
    $segment_modul = Request::segment(0);
    $segment_page = Request::segment(1);
    $segment_page3 = Request::segment(2);
@endphp
<button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
<div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">

    <!-- begin:: Aside Menu -->
    <div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
        <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1">
            <ul class="kt-menu__nav ">

                <li class="kt-menu__item @if($segment_page=="backend") kt-menu__item--active @endif" aria-haspopup="true"><a href="{{URL('/backend')}}" class="kt-menu__link "><i class="kt-menu__link-icon fa fa-home"></i><span class="kt-menu__link-text">Dashboard</span></a></li>

                @canany(['isian formulir'])
                    <li class="kt-menu__section ">
                        <h4 class="kt-menu__section-text">Isi Formulir</h4>
                        <i class="kt-menu__section-icon flaticon-more-v2"></i>
                    </li>
                    @can('isian formulir')
                        <li class="kt-menu__item @if($segment_page3=="isi-formulir") kt-menu__item--active @endif" aria-haspopup="true"><a href="{{ URL('transaksi/isi-formulir') }}" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="fa fa-book"></i></span><span class="kt-menu__link-text">Isi Formulir</span></a></li>
                    @endcan
                @endcan

                @canany(['periode', 'unit-fakultas' , 'formulir'])
                    <li class="kt-menu__section ">
                        <h4 class="kt-menu__section-text">Data Master</h4>
                        <i class="kt-menu__section-icon flaticon-more-v2"></i>
                    </li>
                    @can('formulir')
                        <li class="kt-menu__item @if($segment_page3=="formulir") kt-menu__item--active @endif" aria-haspopup="true"><a href="{{ URL('data/formulir') }}" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="flaticon2-writing"></i></span><span class="kt-menu__link-text">Formulir</span></a></li>
                    @endcan
                    @can('periode')
                        <li class="kt-menu__item @if($segment_page3=="periode") kt-menu__item--active @endif" aria-haspopup="true"><a href="{{ URL('data/periode') }}" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="fa fa-calendar"></i></span><span class="kt-menu__link-text">Periode</span></a></li>
                    @endcan
                    @can('unit-fakultas')
                        <li class="kt-menu__item @if($segment_page3=="unit-fakultas") kt-menu__item--active @endif" aria-haspopup="true"><a href="{{ URL('data/unit-fakultas') }}" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="fa fa-university"></i></span><span class="kt-menu__link-text">Unit & Fakultas</span></a></li>
                    @endcan
                @endcan

                @canany(['user', 'role' , 'permission'])
                    <li class="kt-menu__section ">
                        <h4 class="kt-menu__section-text">Privilege</h4>
                        <i class="kt-menu__section-icon flaticon-more-v2"></i>
                    </li>
                    @can('user')
                        <li class="kt-menu__item @if($segment_page3=="user") kt-menu__item--active @endif" aria-haspopup="true"><a href="{{ URL('privilege/user') }}" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="flaticon-users-1"></i></span><span class="kt-menu__link-text">Users</span></a></li>
                    @endcan
                    @can('role')
                        <li class="kt-menu__item @if($segment_page3=="role") kt-menu__item--active @endif" aria-haspopup="true"><a href="{{ URL('privilege/role') }}" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="flaticon2-protected"></i></span><span class="kt-menu__link-text">Role</span></a></li>
                    @endcan
                    @can('permission')
                        <li class="kt-menu__item @if($segment_page3=="permission") kt-menu__item--active @endif" aria-haspopup="true"><a href="{{ URL('privilege/permission') }}" class="kt-menu__link "><span class="kt-menu__link-icon"><i class="flaticon2-writing"></i></span><span class="kt-menu__link-text">Permission</span></a></li>
                    @endcan
                @endcan

            </ul>
        </div>
    </div>
    <!-- end:: Aside Menu -->
</div>
