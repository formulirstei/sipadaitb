@php
    $segment_modul = Request::segment(0);
    $segment_page = Request::segment(1);
    $segment_page3 = Request::segment(2);
    $permission = unserialize(Auth::user()->permission);
@endphp

<div id="kt_header" class="kt-header  kt-header--fixed " data-ktheader-minimize="on">
    <div class="kt-container  kt-container--fluid ">
        <!-- begin: Header Menu -->
        <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
        <div class="kt-header-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_header_menu_wrapper">
            <button class="kt-aside-toggler kt-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
            <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
                <ul class="kt-menu__nav ">
                    <li class="kt-menu__item  @if($segment_page=="") kt-menu__item--active @endif "><a href="{{URL('/backend')}}" class="kt-menu__link"><span class="kt-menu__link-text">Dashobard</span></a></li>
                    <li class="kt-menu__item  kt-menu__item--active"><a href="#" class="kt-menu__link"><span class="kt-menu__link-text" style="color:#dc3545;">Penggunaan Aplikasi</span></a></li>
                </ul>
            </div>
        </div>
        <!-- end: Header Menu -->

        <!-- begin:: Brand -->
        <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
            <a class="kt-header__brand-logo" href="{{URL('/')}}">
                <img alt="Logo" src="{{URL('assets/images/sidaitb.png')}}" width="120" />
            </a>
        </div>
        <div class="kt-header__topbar kt-grid__item">
                    {{-- @can('ubah privilege') --}}
                        @if(!empty($permission))
                            <div class="kt-header__topbar-item dropdown">
                                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                                    <span class="kt-header__topbar-icon">
                                    <i class="flaticon2-shield kt-font-success"></i>
                                    </span>
                                </div>
                                <div
                                    class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround">
                                    <h5 class="dropdown-header kt-margin-t-10">Ubah Privilege</h5>
                                    <ul class="kt-nav padding-top-5">

                                        @foreach($permission as $dataPermission)
                                            @php
                                                $class = "";
                                                if(Auth::user()->roles[0]->id == $dataPermission['id']){
                                                $class = 'kt-nav__item--active';
                                                }
                                            @endphp

                                            <li class="kt-nav__item {{$class}}">
                                                <a href="{{URL('backend/changePrivilege?id='.$dataPermission['id'].'')}}"
                                                class="kt-nav__link">
                                                    <span class="kt-nav__link-text">{{$dataPermission['name']}}</span>
                                                </a>
                                            </li>
                                        @endforeach

                                    </ul>
                                </div>
                            </div>
                        @endif
                    {{-- @endcan --}}

                    @can('notifikasi')
                        <div class="kt-header__topbar-item dropdown kt-mr-5">
                            <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px" aria-expanded="false">
                                <span class="kt-header__topbar-icon"><i class="flaticon2-bell-alarm-symbol kt-font-info"></i></span>
                                <span class="kt-badge kt-badge--danger"></span>
                            </div>
                            <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl" style="">
                                <div class="kt-head kt-head--skin-light kt-head--fit-x kt-head--fit-b">
                                    <h3 class="kt-head__title">
                                        Notifikasi
                                        &nbsp;
                                        <span class="btn btn-label-brand btn-sm btn-bold btn-font-md"> new</span>
                                    </h3>
                                    <ul class="nav nav-tabs nav-tabs-line nav-tabs-bold nav-tabs-line-3x nav-tabs-line-brand  kt-notification-item-padding-x" role="tablist">
                                        <li class="nav-item">
                                            <a class="nav-link show active" data-toggle="tab" href="#topbar_notifications_notifications" role="tab" aria-selected="true">Notifikasi</a>
                                        </li>
                                    </ul>
                                </div>
                                <!--end: Head -->

                                <div class="tab-content">
                                    <div class="tab-pane show active" id="topbar_notifications_notifications" role="tabpanel">
                                        <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll ps" data-scroll="true" data-height="300" data-mobile-height="200">


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endcan


                @can('setting aplikasi')
                    <div class="kt-header__topbar-item dropdown " data-toggle="kt-tooltip" data-placement="left" title="" data-original-title="Konfigurasi Aplikasi">
                        <div class="kt-header__topbar-wrapper">
                            <a href="{{URL('setting')}}" class="kt-header__topbar-icon"><i class="flaticon2-gear"></i></a>
                        </div>
                    </div>
                @endcan

        <!--begin: User bar -->
            <div class="kt-header__topbar-item kt-header__topbar-item--user">
                <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                    <span class="kt-header__topbar-welcome kt-visible-desktop">Hi,</span>
                    <span class="kt-header__topbar-username kt-visible-desktop">{{Auth::user()->name}}</span>
                    {{-- <img alt="Pic" src="{{URL('upload/profile/'.Auth::user()->profile.'')}}" /> --}}

                    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                    <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
                </div>
                <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">

                    <!--begin: Head -->
                    <div class="kt-user-card kt-user-card--skin-light kt-notification-item-padding-x">
                        <div class="kt-user-card__avatar">
                            {{-- <img class="kt-hidden-" alt="Pic" src="{{URL('upload/profile/'.Auth::user()->profile.'')}}" width="50"/> --}}
                            <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
                        </div>
                        <div class="kt-user-card__name">
                            {{Auth::user()->name}}
                            <div style="font-size:13px !important; color: #74788d;">{{isset(Auth::user()->get_user_detail->get_unit_fakultas->name) ? Auth::user()->get_user_detail->get_unit_fakultas->name : '-' }}</div>
                            <div style="font-size:13px !important; color: #74788d;">
                                Role :
                                @foreach (Auth::user()->roles as $role)
                                    {!! $role->name !!}
                                @endforeach
                            </div>
                        </div>
                    </div>

                    <!--end: Head -->

                    <!--begin: Navigation -->
                    <div class="kt-notification">
                        <div class="kt-notification__custom kt-space-between">
                            <a href="{{URL('logout')}}"  class="btn btn-info btn-label-info btn-sm">Sign Out</a>
                        </div>
                    </div>

                    <!--end: Navigation -->
                </div>
            </div>

            <!--end: User bar -->


        </div>

        <!-- end:: Header Topbar -->
    </div>
</div>

<!-- end:: Header -->
