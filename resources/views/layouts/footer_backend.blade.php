<div class="kt-footer kt-grid__item" id="kt_footer">
    <div class="kt-container ">
        <div class="kt-footer__wrapper">
            <div class="kt-footer__copyright">
                Copyright © 2021 PT. Dataquest Laverage Indonesia , All rights reserved.
            </div>
        </div>
    </div>
</div>
