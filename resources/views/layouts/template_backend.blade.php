<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="shortcut icon" type="image/x-icon" href="{{URL('assets/images/sidaitb.png')}}">
    <meta name="author" content="KIKI">
    <title>@if(!empty($title)) {{$title}} @else Beranda @endif | SIPADA </title>
    <link rel="stylesheet"
          href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <link rel="stylesheet" type="text/css"
          href="{{URL('assets/theme/plugins/general/perfect-scrollbar/css/perfect-scrollbar.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL('assets/theme/plugins/general/tether/dist/css/tether.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{URL('assets/theme/plugins/general/bootstrap-select/dist/css/bootstrap-select.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL('assets/theme/plugins/general/select2/dist/css/select2.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL('assets/theme/plugins/general/animate.css/animate.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL('assets/theme/plugins/general/toastr/build/toastr.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{URL('assets/theme/plugins/general/sweetalert2/dist/sweetalert2.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL('assets/theme/plugins/general/socicon/css/socicon.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{URL('assets/theme/plugins/general/plugins/line-awesome/css/line-awesome.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL('assets/theme/plugins/general/plugins/flaticon/flaticon.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{URL('assets/theme/plugins/general/plugins/flaticon2/flaticon.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{URL('assets/theme/plugins/general/@fortawesome/fontawesome-free/css/all.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{URL('assets/theme/plugins/custom/plugins/jquery-ui/jquery-ui.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{URL('assets/theme/plugins/custom/datatables.net-bs4/css/dataTables.bootstrap4.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{URL('assets/theme/plugins/custom/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" type="text/css"
          href="{{URL('assets/theme/plugins/general/summernote/dist/summernote.css')}}">
    @stack('styles')
    <link rel="stylesheet" type="text/css" href="{{URL('assets/theme/css/style.bundle.css')}}">
    <link rel="stylesheet" type="text/css" href="{{URL('assets/theme/css/style.css?v2')}}">
    <link rel="stylesheet" type="text/css" href="{{URL('assets/theme/css/customInput.css')}}">
</head>

<body
    class="kt-page--loading-enabled kt-page--loading kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-header-mobile--fixed kt-subheader--enabled kt-subheader--transparent kt-aside--enabled kt-aside--left kt-aside--fixed kt-page--loading">


<!-- begin:: Header Mobile -->
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
    <div class="kt-header-mobile__logo">
        <a href="{{URL('')}}">
            <img alt="Logo" src="{{URL('assets/images/sidaitb.png')}}" width="50"/>
        </a>
    </div>
    <div class="kt-header-mobile__toolbar">
        <button class="kt-header-mobile__toolbar-toggler kt-header-mobile__toolbar-toggler--left"
                id="kt_aside_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
        <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i
                class="flaticon-more-1"></i></button>
    </div>
</div>

<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
        @include('layouts.nav_backend')
        @include('layouts.menu_backend')
            <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
                <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                @yield('sidebar')
                    <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                        @yield('content')
                    </div>
                </div>
            </div>
            @include('layouts.footer_backend')
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<div id="modalOpenImage" class="modal fade text-left" tabindex="-1" role="dialog" aria-labelledby="myModalLabel1"
     aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="myModalLabel1">Detail Gambar</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="imageReplace"></div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-info" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


@can('tambah data pendaftar')
    @if(!is_null(\Auth::user()->school->options))
        <div id="modalTambahPendaftaran" class="modal fade text-left" tabindex="-1" role="dialog"
             aria-labelledby="myModalLabel1"
             aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title" id="myModalLabel1">Pilihan Jalur :</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="list-group">
                            @foreach(\Auth::user()->school->options as $option)
                                <a href="{{URL('pendaftar/create?choice='.$option->id)}}"
                                   class="list-group-item list-group-item-action">
                                    <span class="text-black-50">{{ $option->name }}</span>
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn grey btn-outline-info" data-dismiss="modal">Tutup</button>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endcan

@include('modal.modal_info')
@include('modal.modal_pdf_berkas')
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#1E9FF2",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#1E9FF2",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>



<script src="{{URL('assets/theme/plugins/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/popper.js/dist/umd/popper.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/bootstrap/dist/js/bootstrap.min.js')}}"type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/js-cookie/src/js.cookie.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/moment/min/moment.min.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/tooltip.js/dist/umd/tooltip.min.js')}}"type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/perfect-scrollbar/dist/perfect-scrollbar.js')}}"type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/sticky-js/dist/sticky.min.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/jquery-form/dist/jquery.form.min.js')}}"type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/block-ui/jquery.blockUI.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/bootstrap-select/dist/js/bootstrap-select.js')}}"type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/select2/dist/js/select2.full.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/toastr/build/toastr.min.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/sweetalert2/dist/sweetalert2.min.js')}}"type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/js/global/integration/plugins/sweetalert2.init.js')}}"type="text/javascript"></script>
<script src="{{URL('assets/theme/js/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/custom/plugins/jquery-ui/jquery-ui.min.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/custom/datatables.net/js/jquery.dataTables.js')}}"type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/custom/datatables.net-bs4/js/dataTables.bootstrap4.js')}}"type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/custom/js/global/integration/plugins/datatables.init.js')}}"type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/js/global/integration/plugins/sweetalert2.init.js')}}"type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/summernote/dist/summernote.js')}}"type="text/javascript"></script>
<script src="{{URL('assets/theme/js/mobile.js')}}"type="text/javascript"></script>




@stack('scripts')
<script>var token = '{{csrf_token()}}';</script>
<script src="{{URL('assets/theme/js/custom.js?v8')}}" type="text/javascript"></script>
<script>

    // $(window).on('load',function(){
    //       $('#modalInfo').modal('show');
    //   });


    (function ($) {
        window.loading_modal = $('.modal-body').html();
    })(jQuery);

    function openImage(val) {
        $('#imageReplace').html('<img src="' + val + '" class="img-fluid" style="width:100%">');
        $('#modalOpenImage').modal("show");
    }


    function showLoading() {
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'info',
            message: 'Processing...'
        });
    }

    function hideLoading() {
        KTApp.unblockPage();
    }


    function showLoadingModal() {
        KTApp.block('.modal-content', {
            overlayColor: '#000000',
            type: 'v2',
            state: 'info',
            message: 'Processing...'
        });
    }

    function hideLoadingModal() {
        KTApp.unblock('.modal-content');
    }


    function showModal(link) {
        var url = "";
        if (typeof link === 'string' || link instanceof String) {
            url = link;
        } else {
            url = $(link).attr("data-url");
        }

        showLoading();
        $('#modalApplication #content-modal').empty();
        $('#modalApplication #content-modal').load(url,
            function (responseText, textStatus, req) {
                if (isJSON(responseText)) {
                    var value = jQuery.parseJSON(responseText);
                    if (value.status == "error") {
                        toastr.error(value.message);
                    }
                } else {
                    $('#modalApplication').modal('show');
                }
                hideLoading();
            });
    }

    $(document).on('submit', function (e) {
        showLoading();
        toastr.info('Sedang Memproses');
    });

    var $detail_modal = $('#detail-modal');
    $detail_modal_body = $detail_modal.find('.modal-body');
    $(document).on('click', '.btn-detail', function () {
        $detail_modal.modal('toggle');
        var html_loading = '<h4 class="text-center"><i class="kt-spinner kt-spinner--sm kt-spinner--info" style="margin-right:30px;"></i> Sedang Memuat ...</h4>';
        $detail_modal_body.html(html_loading);
        $.ajax({
            url: $(this).attr('data-modal-url'),
            success: function (data) {
                if (typeof data.status !== 'undefined'){
                    swal.fire("Informasi", data.message, "error");
                    $detail_modal.modal('toggle');
                }else{
                    $detail_modal_body.html(data);
                }
            }
        });
    });


    $(document).on('show.bs.modal', '.modal', function (event) {
        var zIndex = 1040 + (10 * $('.modal:visible').length);
        $(this).css('z-index', zIndex);
        setTimeout(function () {
            $('.modal-backdrop').not('.modal-stack').css('z-index', zIndex - 1).addClass('modal-stack');
        }, 0);


    });

    $('input[type=number]').on('mousewheel', function () {
        var el = $(this);
        el.blur();
        setTimeout(function () {
            el.focus();
        }, 10);
    });


    function lihatBerkas(val) {
        var id = $(val).data('id');
        showLoading();
        $.ajax({
            url: "{{URL('lihatBerkas?id=')}}" + id,
            type: "GET",
            success: function (data) {
                if (data.status === "error") {
                    swal.fire("Informasi", data.message, "error");
                } else if (data.status === "success") {
                    $('#resultLihatBerkas').html(data.result);
                    // $('#modalLihatBerkas').modal({backdrop: 'static', keyboard: false});
                    $('#modalLihatBerkas').modal();
                }
                hideLoading();
            },
            error: function (response) {
                swal.fire("Informasi", "Maaf Request Error , Silahkan Coba Kembali", "error");
                hideLoading();
            }
        });
    }

    function openBerkas(val) {
        var url = $(val).data('url');
        if(jQuery.browser.mobile)
        {
            window.open(url, '_blank');
        }else{
            var height = $(window).height()-220;
            $('#pdfBerkas').html('<embed src="' + url + '" width="100%" height="' + height + '"/>');
            $('#modalPdfBerkas').modal();
        }
    }

</script>
@include('javascript.js')
</body>
</html>
