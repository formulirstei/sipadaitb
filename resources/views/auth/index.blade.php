
<!DOCTYPE html>
<html class="loading" lang="en" data-textdirection="ltr">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <link rel="shortcut icon" type="image/x-icon" href="{{URL('assets/images/logo.ico')}}">
    <meta name="author" content="KIKI">
    <title>Login Aplikasi Sipada</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <link href="{{URL('assets/theme/css/pages/login/login-1.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL('assets/theme/css/style.bundle.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL('assets/theme/css/skins/header/base/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL('assets/theme/css/skins/header/menu/light.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL('assets/theme/css/skins/brand/dark.css')}}" rel="stylesheet" type="text/css" />
    <link href="{{URL('assets/theme/css/skins/aside/dark.css')}}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" type="text/css" href="{{URL('assets/theme/plugins/general/toastr/build/toastr.css')}}">
</head>

<body  class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading"  style="background-color:#1E9FF2;">

<!-- begin:: Page -->
<div class="kt-grid kt-grid--ver kt-grid--root">
    <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v1" id="kt_login">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--desktop kt-grid--ver-desktop kt-grid--hor-tablet-and-mobile">
            <!--begin::Aside-->
            <div class="kt-grid__item kt-grid__item--order-tablet-and-mobile-2 kt-grid kt-grid--hor kt-login__aside" style="background-image: url({{URL('assets/images/logo_login.png')}}); background-size: 78%;">
                <div class="kt-grid__item">
                    {{-- <a href="#" class="kt-login__logo">
                        <img src="{{URL('assets/images/sidaitb.png')}}" width="180" style="float: right">
                    </a> --}}
                </div>
            </div>
            <!--begin::Aside-->

            <!--begin::Content-->
            <div class="kt-grid__item kt-grid__item--fluid  kt-grid__item--order-tablet-and-mobile-1  kt-login__wrapper" style="background-color:#1E9FF2;">
                <!--begin::Head-->
                <div class="kt-login__head">
                    <a href="#" class="kt-login__logo">
                        <img src="{{URL('assets/images/sidaitb.png')}}" width="200">
                    </a>
                </div>
                <!--end::Head-->

                <!--begin::Body-->
                <div class="kt-login__body">
                    <!--begin::Signin-->
                    <div class="kt-login__form">
                        <div class="kt-login__title">
                            <h3 class="text-white">Login Aplikasi Sipada ITB</h3>
                        </div>

                        <!--begin::Form-->
                        <form class="kt-form" method="POST" id="formLogin">
                            <input type="hidden" name="_token" value="{{csrf_token()}}">
                            <div class="form-group">
                                <input class="form-control" type="text" placeholder="Username / Email" name="username" autocomplete="off" id="username">
                            </div>
                            <div class="form-group">
                                <input class="form-control" type="password" placeholder="Password" name="password"  autocomplete="off" id="password">
                            </div>
                            <!--begin::Action-->
                            <div class="kt-login__actions">
                                <a href="#" class="kt-link kt-login__link-forgot text-white">

                                </a>
                                <button type="button" class="btn btn-light text-info" id="btnLogin">Login Aplikasi</button>
                            </div>
                            <!--end::Action-->
                        </form>
                        <!--end::Form-->

                        <!--end::Options-->
                    </div>
                    <!--end::Signin-->
                </div>
                <!--end::Body-->
            </div>
            <!--end::Content-->
        </div>
    </div>
</div>

</body>



<script src="{{URL('assets/theme/plugins/general/jquery/dist/jquery.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/js/scripts.bundle.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/js/pages/custom/login/login-1.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/sticky-js/dist/sticky.min.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/block-ui/jquery.blockUI.js')}}" type="text/javascript"></script>
<script src="{{URL('assets/theme/plugins/general/toastr/build/toastr.min.js')}}" type="text/javascript"></script>

<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#1E9FF2",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#1E9FF2",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };

    $("#btnLogin").click(function(){
        showLoading();

        var username = $("#username").val();
        var password = $("#password").val();

        $.ajax({
            type: "POST",
            url: "{{URL('login')}}",
            data: {
                "username": username,
                "password": password,
                "_token": "{{ csrf_token() }}",

            },
            success: function(val) {
                if(val['success'] == true){
                    toastr.success(val.message);
                    hideLoading();
                    var url = "{{ URL('/backend') }}";
                    $(location).attr('href',url);
                }else{
                    toastr.error(val.message);
                    hideLoading();
                }
            },
            error: function() {
                toastr.error("Terjadi Kesalahan , Silahkan Coba Kembali");
                hideLoading();
            }
        });
    });


    function showLoading() {
        KTApp.blockPage({
            overlayColor: '#000000',
            type: 'v2',
            state: 'info',
            message: 'Processing...'
        });
    }

    function hideLoading() {
        KTApp.unblockPage();
    }



</script>



</html>
