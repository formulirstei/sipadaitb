<div class="modal fade text-left" id="modalLihatBerkas" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-info white">
                <h4 class="modal-title white"><i class="la la-archive"></i> Lihat Berkas Rawan Melanjutkan Pendidikan</h4>
                <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                        <div id="resultLihatBerkas"></div>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
            </div>
        </div>
    </div>
</div>

