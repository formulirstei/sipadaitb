<div class="modal fade text-left"  id="{{ $modal_id }}" tabindex="-1">
    <div class="modal-dialog modal-{{$modal_size}}">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h4 class="modal-title white" id="{{ $modal_id }}-label"><i class="la la-ellipsis-h"></i>  {{ $modal_title }}</h4>
                <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h4 class="text-center"><i class="kt-spinner kt-spinner--sm kt-spinner--info" style="margin-right:30px;"></i> Sedang Memuat ...</h4>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-info" data-dismiss="modal">Tutup</button>
                @if(isset($save_mode))
                    <button type="button" class="btn btn-info d-none" id="save-{{strtolower($save_title)}}">
                        <i class="la la-save"></i>
                        Simpan {{$save_title}}
                    </button>
                @endif
                @if(isset($print_mode))
                    <a href="" id="print_out" class="btn btn-warning" target="_blank">
                        Print
                    </a>
                @endif
            </div>
        </div>
    </div>
</div>

