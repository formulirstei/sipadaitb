<style>
    .modal-content-progress {
        position: relative;
        display: -moz-box;
        display: flex;
        -webkit-flex-direction: column;
        -ms-flex-direction: column;
        flex-direction: column;
        width: 100%;
        pointer-events: auto;
        background-color: #FFF;
        background-clip: padding-box;
        border: 1px solid rgba(0, 0, 0, .2);
        border-radius: .35rem;
        outline: 0;
    }
</style>
<div class="modal fade text-left" id="modalProgress" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content-progress">
            <div class="modal-header bg-info white">
                <h4 class="modal-title white"><i class="la la-check-circle"></i> Progress Upload Berkas Pendaftaran Asesor</h4>
                <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <div class="progress">
                        <div class="progress-bar progress-bar-striped progress-bar-animated bg-info" role="progressbar"  id="progressBerkas" style="width:0%">0%</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

