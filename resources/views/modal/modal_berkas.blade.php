<div class="modal fade text-left" id="modalBerkas" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-info white">
                <h4 class="modal-title white"><i class="la la-archive"></i> <span id="titleBerkas"></span> Berkas Pendaftaran Asesor</h4>
                <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="container">
                <form id="formUploadBerkas" method="post" enctype="multipart/form-data">
                    <input type="hidden" name="id" id="idSiswaBerkas">
                    <input type="hidden" name="semester" id="semesterBerkas">
                    <input type="hidden" id="tokenBerkas" name="_token" value="{{csrf_token()}}">
                    <div class="container-modal">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group mb-1">
                                <label>Nama Siswa</label>
                                <input type="text" class="form-control" placeholder="Nama Siswa"
                                       id="namaSiswa" readonly>
                            </div>
                        </div>

                        <div class="col-md-12">
                           <div class="row">
                               <div class="col-md-6 form-group mb-1">
                                   <label>File SKTM.</label>
                                   <input name="file_sktm" type="file" class="form-control"  accept="application/pdf">
                               </div>

                               <div class="col-md-6 form-group mb-1">
                                   <label>Kartu/Dokumen Pendukung ( Jika Ada ).</label>
                                   <input name="file_kartu" type="file" class="form-control"  accept="application/pdf">
                               </div>


                           </div>
                        </div>

                    </div>

                        <div class="alert bg-danger alert-dismissible mb-2" role="alert">
                            * Berkas Harus Berformat PDF <br>
                            * File Maksimum 5MB , lebih dari 5MB silahkan Compress terlebih dahulu <br>
                            Compress PDF : <a href="https://smallpdf.com/compress-pdf" target="_blank" style="color:white;   text-decoration: underline;">Buka Link Compress PDF</a>
                        </div>

                    </div>
                </form>
                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-outline-info" id="btnTambahBerkas"><span id="titleBtnBerkas"></span> Berkas</button>
            </div>
        </div>
    </div>
</div>

