<div class="modal fade text-left"  id="modalInfo" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-info white">
                <h4 class="modal-title white modal-title-prestasi"><i class="la la-info"></i> Persiapan Pemutakhiran Data Mandiri ASN</h4>
                <button type="button" class="close white" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="kt-section">
                    <div class="kt-section__desc">
                        Yth. Bapak/Ibu Kepala Satuan Pendidikan serta Operator Satuan Pendidikan Formal dan Non Formal di lingkungan Disdik Kota Bandung    <p></p>

                        Disampaikan dengan hormat, dalam rangka Persiapan Pemutakhiran Data Mandiri ASN melalui aplikasi MySAPK pada bulan Juli - Agustus 2021 maka perlu disampaikan beberapa hal berikut ini :
                        <p></p>
                        1. sehubungan dengan BKPSDM Kota Bandung tidak dapat mengetahui sudah sejauh mana dan berapa persen rekan2 ASN Kota Bandung yg sudah mengisi data email dan no hp AKTIF pada website: kanreg3.id/puasn
                        <br>
                        2. Maka perlu pengisian data email AKTIF dan no hp AKTIF akan dilaksanakan menggunakan aplikasi ini SIDAK agar dapat terdokumentasi menjadi Data Base Disdik;
                        <br>
                        3. Untuk itu kami sampaikan agar operator masing-masing Satuan Pendidikan dapat segera <b>mengupdate data email dan No HP ASN yang masih AKTIF</b> (JANGAN MENGAMBIL DARI SIMPEG dan DAPODIK) di laman sidak.disdik.bandung.go.id <b>paling lambat Jumat, 23 April 2021 Pukul 11.00 WIB</b>
                        <br>
                        4. Tutorial pengisian terlampir.
                        <p></p>
                        Demikian hal ini disampaikan, atas kerjasamanya kami ucapkan terima kasih. <p></p>

                        Kasubbag Umum dan Kepegawaian <br>
                        CC : Kadisdik, Sekdisdik, Para Kepala Bidang.
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn grey btn-outline-secondary" data-dismiss="modal">Tutup</button>
                <a href="{{URL('data/pegawai')}}"><button type="button" class="btn grey btn-info">Update No Handphone ASN</button></a>
            </div>
        </div>
    </div>
</div>

