<script>
    $(document).on('click', '.btn-logout', function() {
        swal({
            title: "Konfirmasi",
            text: "Apakah Anda Yakin Ingin Keluar ? ",
            icon: "warning",
            showLoaderOnConfirm: true,
            closeOnConfirm: true,
            buttons: {
                cancel: {
                    text: "Batal",
                    value: null,
                    visible: true,
                    className: "",
                    closeModal: true,
                },
                confirm: {
                    text: "Ya",
                    value: true,
                    visible: true,
                    className: "btn-info",
                    closeModal: false
                }
            }
        }).then(isConfirm => {
            if (isConfirm) {
                toastr.info("Proses Logout");
                window.document.location.href = "{{ URL('logout') }}";
            }
        });
    });

    var remoteSelect2 = function(parameters) {
        var selector 	 = parameters['selector'],
            type          = parameters['type'],
            filter       = parameters['filter'],
            selected       = parameters['selected'],
            url = "{{URL(config('settings.page_backend').'remote?type=')}}"+type,
            domSelector  = parameters['domSelector'] || null,
            where     = parameters['where'] || null,
            multiple     = parameters['multiple'] || false;

        var closeOnSelect = !multiple;
        var $element = (domSelector != null) ? domSelector : $(selector);

        var placeholder="";
        if(type==="sekolah"){
            placeholder="Pilih Sekolah";
        }else if(type==="regions"){
            placeholder="Pilih Wilayah";
        }else{
            placeholder="Pilih Data";
        }
        $(selector).select2({
            width: '100%',
            allowClear: true,
            placeholder: {
                id: '-1',
                text: placeholder
            },
            ajax: {
                url      :url,
                dataType : 'json',
                delay    : 350,
                data     : function (params) {
                    var newParam = {};
                    newParam['q'] = (params.term == undefined) ? '' : params.term;
                    newParam['page'] = (params.page == undefined) ? '' : params.page;
                    // if(where) {
                    //     var whereData = where.split(",");
                    //     for (var i = 0; i < whereData.length; i++) {
                    //         if(whereData[i]==="sekolahId"){
                    //             newParam['sekolahId'] = $('.setting-sekolah').val();
                    //         }else if(whereData[i]==="tahunAjaranId"){
                    //             newParam['tahunAjaranId'] = $('.setting-tahun_ajaran').val();
                    //         }
                    //     }
                    // }

                    return $.param(newParam);
                },
                processResults: function (data, params) {
                    params.page = params.page || 1;
                     $.each(data.data, function(){
                         this.id = this.id;
                         if(filter==="region"){
                             this.text = this['province'] + " - " + this['city'] + " - " + this['district'] + " - " + this['subdistrict'];
                         }else{
                             this.text = this[filter];
                         }
                     });
                    return {
                        results: data.data,
                        pagination: {
                            more: params.page < data.pagination.total_page
                        }
                    };

                },
                cache: true
            },
            escapeMarkup       : function (markup) { return markup; },
            minimumInputLength : 0,
            templateResult     :  formatSelect2[type],
            templateSelection  : _formatSelect2,
            closeOnSelect      : closeOnSelect
        });

    };
    var formatSelect2 = {};

    var _formatSelect2 = function (item) {
        return item.text;
    };

</script>
